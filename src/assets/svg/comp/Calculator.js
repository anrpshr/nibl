import React from 'react';

const Calculator = () => (
  <svg className="svg-icon _calculator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.86 25.92">
    <g id="Layer_2" data-name="Layer 2">
      <g id="Layer_1-2" data-name="Layer 1">
        <path id="Path_59" data-name="Path 59" d="M20.25,0H1.34A1.34,1.34,0,0,0,0,1.34V26.22a1.34,1.34,0,0,0,1.34,1.34H20.25a1.34,1.34,0,0,0,1.34-1.34V1.34A1.34,1.34,0,0,0,20.25,0Zm.42,26.22a.42.42,0,0,1-.42.42H1.34a.42.42,0,0,1-.42-.42V1.34A.42.42,0,0,1,1.34.92H20.25a.42.42,0,0,1,.42.42Z"/>
        <path id="Path_60" data-name="Path 60" d="M2.3,25.26H7.35V20.21h-5Zm.92-4.13H6.43v3.21H3.22Z"/>
        <path id="Path_61" data-name="Path 61" d="M8.27,25.26h5.05V20.21H8.27Zm.92-4.13h3.22v3.21H9.19Z"/>
        <path id="Path_62" data-name="Path 62" d="M14.24,25.26h5v-11h-5Zm.92-10.1h3.21v9.18H15.16Z"/>
        <path id="Path_63" data-name="Path 63" d="M2.3,19.29H7.35v-5h-5Zm.92-4.13H6.43v3.22H3.22Z"/>
        <path id="Path_64" data-name="Path 64" d="M8.27,19.29h5.05v-5H8.27Zm.92-4.13h3.22v3.22H9.19Z"/>
        <path id="Path_65" data-name="Path 65" d="M2.3,13.32H7.35V8.27h-5Zm.92-4.13H6.43v3.22H3.22Z"/>
        <path id="Path_66" data-name="Path 66" d="M8.27,13.32h5.05V8.27H8.27Zm.92-4.13h3.22v3.22H9.19Z"/>
        <path id="Path_67" data-name="Path 67" d="M14.24,13.32h5V8.27h-5Zm.92-4.13h3.21v3.22H15.16Z"/>
        <path id="Path_68" data-name="Path 68" d="M2.3,6.89h17V2.3H2.3Zm.92-3.67H18.37V6H3.22Z"/>
      </g>
    </g>
  </svg>
);

export default Calculator;
