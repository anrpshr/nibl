import axios from 'axios';
import { generatePath } from 'react-router';

import store from '../redux';
import API from '../constants/api.constants';

const state = store.getState();
const { locale } = state;

const api = `${API.apiV2}${API.endPoints.PRODUCT}`;

export const getProduct = id => new Promise((resolve, reject) => {
  axios({
    url: generatePath(generatePath(api, { id })),
    method: 'GET',
    headers: { 'X-Custom-Locale': locale.lang }
  })
    .then((response) => {
      if (response.data.status) {
        resolve(response.data.result);
      } else reject(response.data.error);
    })
    .catch((e) => {
      reject(e);
    });
});
