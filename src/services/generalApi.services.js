import axios from 'axios';

import store from '../redux';
import { API } from '../constants';

const state = store.getState();
const { locale } = state;

export const get = (api, success, fail = () => {}, params) => {
  axios({
    url: `${API.apiUrl}${api}`,
    method: 'GET',
    params,
    headers: { 'X-Custom-Locale': locale.lang }
  })
    .then((response) => {
      if (response.data.status) {
        success(response.data.result);
      } else throw new Error(response.data.error);
    })
    .catch((e) => {
      fail(e);
    });
};

export const post = (api, success, fail, data = {}, params = null) => {
  axios({
    url: `${API.apiUrl}${api}`,
    method: 'POST',
    data,
    params
  })
    .then((response) => {
      if (response.data.status === 'success') {
        success(response.data.message);
      } else {
        throw new Error(response.data.message);
      }
    })
    .catch((e) => {
      fail(e.message);
    });
};

export const getV2 = (api, params = null) => new Promise((resolve, reject) => {
  axios({
    url: `${API.apiV2}${api}`,
    method: 'GET',
    params,
    headers: { 'X-Custom-Locale': locale.lang }
  })
    .then((response) => {
      if (response.data.status) {
        resolve(response.data.result);
      } else throw new Error(response.data.error);
    })
    .catch((e) => {
      reject(e);
    });
});
