import store from '../redux';

export const getRoute = (id) => {
  const { navigation } = store.getState();
  const flattenedNav = [];
  [...navigation.primary, ...navigation.secondary].forEach((route) => {
    flattenedNav.push(route);

    if (route.routes && route.routes.length > 0) {
      route.routes.forEach((cRoute) => {
        flattenedNav.push(cRoute);

        if (cRoute.routes && cRoute.routes.length > 0) {
          cRoute.routes.forEach((gRoute) => {
            flattenedNav.push(gRoute);
          });
        }
      });
    }
  });

  const route = flattenedNav.find(r => r.menuId === id);

  return route && route.route;
};
