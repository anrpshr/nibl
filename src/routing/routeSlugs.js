import slugs from 'slugs';
import { sortBy } from 'lodash';

const makeSlug = name => slugs(name);

const primaryMenuOrder = [1, 4, 2, 5];

const makeSlugs = (routes) => {
  const primary = [...routes.primary];
  const secondary = [...routes.secondary];

  const sortedPrimary = sortBy(primary, (e => primaryMenuOrder.indexOf(e.menuId)));

  sortedPrimary.forEach((route) => {
    const routeSlug = makeSlug(route.name);
    const routeRoute = `/${routeSlug}`;
    route.route = routeRoute;
    route.slug = routeSlug;

    if (route.routes && route.routes.length > 0) {
      route.routes.forEach((childRoute) => {
        const childRouteSlug = makeSlug(childRoute.name);
        const childRouteRoute = `${routeRoute}/${childRouteSlug}`;
        childRoute.slug = childRouteSlug;
        childRoute.route = childRouteRoute;

        if (childRoute.routes && childRoute.routes.length > 0) {
          childRoute.routes.forEach((gChild) => {
            const gChildSlug = makeSlug(gChild.name);
            const gChildRoute = `${childRouteRoute}/${gChildSlug}`;
            gChild.route = gChildRoute;
            gChild.slug = gChildSlug;
          });
        }
      });
    }
  });

  secondary.forEach((route) => {
    const routeSlug = makeSlug(route.name);
    const routeRoute = `/${routeSlug}`;
    route.route = routeRoute;
    route.slug = routeSlug;

    if (route.routes && route.routes.length > 0) {
      route.routes.forEach((childRoute) => {
        const childRouteSlug = makeSlug(childRoute.name);
        const childRouteRoute = `${routeRoute}/${childRouteSlug}`;
        childRoute.slug = childRouteSlug;
        childRoute.slug = childRouteSlug;

        if (childRoute.routes && childRoute.routes.length > 0) {
          childRoute.routes.forEach((gChild) => {
            const gChildSlug = makeSlug(gChild.name);
            const gChildRoute = `${childRouteRoute}/${gChildSlug}`;
            gChild.route = gChildRoute;
            gChild.slug = gChildSlug;
          });
        }
      });
    }
  });

  return {
    primary: sortedPrimary,
    secondary
  };
};

export default makeSlugs;
