export const routes = {
  primary: [
    {
      name: 'Personal',
      slug: 'personal',
      id: '12345',
      template: 'blockListing',
      routes: [
        {
          name: 'Deposits',
          slug: 'deposits',
          id: '12345',
          template: 'blockListing',
          routes: [
            {
              name: 'Keta Keti Bachat Khata',
              slug: 'keta-keti-bachat-khata',
              id: '4',
              routes: null,
              template: 'overview',
              sections: [
                {
                  name: 'Product Overview',
                  slug: 'overview'
                },
                {
                  name: 'Features',
                  slug: 'features'
                },
                {
                  slug: 'lost-stolen-cards',
                  name: 'Lost/Stolen Cards'
                },
                {
                  name: 'Tools and Downloads',
                  slug: 'tools-downloads'
                },
                {
                  name: 'Product Video',
                  slug: 'product-video'
                },
                {
                  name: 'FAQ',
                  slug: 'faq'
                }
              ]
            },
            {
              name: 'Lotus Account',
              slug: 'lotus-account',
              id: '1',
              routes: null,
              template: 'overview'
            },
            {
              name: 'E-zee Saving Account',
              slug: 'ezee-account',
              id: '2',
              routes: null,
              template: 'overview'
            },
            {
              name: 'Afnai Bachat Khata',
              slug: 'afnai-bachat-khata',
              id: '3',
              routes: null,
              template: 'overview'
            },
            {
              name: 'Ezee Student Account',
              slug: 'students-savings-account',
              id: '5',
              routes: null,
              template: 'overview'
            },
            {
              name: 'NIBL Saving Bonanza',
              slug: 'savings-account',
              id: '6',
              routes: null,
              template: 'overview'
            },
            {
              name: 'Surakchya Bachat Khata',
              slug: 'surakchya-khata',
              id: '7',
              template: 'overview'
            },
            {
              name: 'Prithivi Remittance Saving Account',
              slug: 'prithivi-remittance-saving-account',
              id: '8',
              template: 'overview'
            }
          ]
        },
        {
          name: 'Fixed Deposit',
          slug: 'fixed-deposit',
          id: '123456',
          routes: null,
          template: null
        },
        {
          name: 'Loans',
          slug: 'loans',
          id: '12345',
          routes: [
            {
              name: 'NIBL Home Loan',
              slug: 'home-loan',
              id: '8',
              template: 'overview'
            },
            {
              name: 'NIBL Education Loan',
              slug: 'education-loan',
              id: '9',
              template: 'overview'
            },
            {
              name: 'NIBL Auto Loan',
              slug: 'auto-loan',
              id: '11',
              template: 'overview'
            },
            {
              name: 'NIBL Personal Loan',
              slug: 'nibl-personal-loan',
              id: '14',
              template: 'overview'
            },
            {
              name: 'NIBL Gold/Silver Loan',
              slug: 'nibl-gold-silver-loan',
              id: '15',
              template: 'overview'
            },
            {
              name: 'NIBL Home And Solar Loan',
              slug: 'solar-loan',
              id: '12',
              template: 'overview'
            },
            {
              name: 'NIBL Cab Loan',
              slug: 'emi-scheme',
              id: '13',
              template: 'overview'
            },
            {
              name: 'NIBL EMI Scheme',
              slug: 'emi-scheme',
              id: '13',
              template: 'overview'
            }
          ],
          template: null
        },
        {
          name: 'Cards',
          slug: 'cards',
          id: '5',
          routes: null,
          template: 'overview'
        },
        {
          name: 'Rates & Fees',
          slug: 'rates-fees',
          id: '12345',
          routes: [
            {
              name: 'Downloads',
              slug: 'downloads',
              id: '12345',
              routes: null,
              template: 'downloads'
            },
            {
              name: 'Daily Rates',
              slug: 'daily-rates',
              id: '12345',
              routes: null,
              template: 'dailyRates'
            },
            {
              name: 'STC Charges',
              slug: 'stc-charges',
              id: '12345',
              routes: null,
              template: 'interestRates'
            }
          ],
          template: null
        },
        {
          name: 'Services',
          slug: 'services',
          id: '12345',
          routes: [
            {
              name: 'ATM',
              slug: 'atm',
              id: '12345',
              routes: null,
              template: 'location'
            },
            {
              name: 'E-Banking',
              slug: 'e-banking',
              id: '12345',
              routes: null,
              template: null
            },
            {
              name: 'Bill Payments',
              slug: 'bill-payments',
              id: '12345',
              routes: null,
              template: 'payments'
            },
            {
              name: 'Mobile Banking',
              slug: 'mobile-banking',
              id: '12345',
              routes: null,
              template: 'mobileBanking'
            },
            {
              name: 'Locker',
              slug: 'locker',
              id: '12345',
              routes: null,
              template: 'locker'
            },
            {
              name: 'Branchless Banking',
              slug: 'branchless-banking',
              id: '12345',
              routes: null,
              template: 'branchlessBanking'
            },
            {
              name: 'IPS',
              slug: 'ips',
              id: '12345',
              routes: null,
              template: null
            },
            {
              name: 'C-ASBA',
              slug: 'c-asba',
              id: '12345',
              routes: null,
              template: null
            }
          ],
          template: null
        },
        {
          name: 'Remittance',
          slug: 'remittance',
          id: '12345',
          routes: [
            {
              name: 'Prithivi Remitance',
              slug: 'prithivi-remitance',
              id: '12345',
              routes: null,
              template: 'prithiviRemitance'
            },
            {
              name: 'Domestic Remitance',
              slug: 'domestic-remitance',
              id: '12345',
              routes: null,
              template: 'domesticRemitance'
            },
            {
              name: 'International Partners',
              slug: 'international-partners',
              id: '12345',
              routes: null,
              template: 'internationalNetworks'
            },
            {
              name: 'Find An Agent',
              slug: 'find-an-agent',
              id: '12345',
              routes: null,
              template: 'contactForm'
            }
          ],
          template: null
        },
        {
          name: 'Tools & Calculators',
          slug: 'tools-calculators',
          id: '12345',
          routes: null,
          template: 'calculatorOverBg'
        },
        {
          name: 'Faq',
          slug: 'faq',
          id: '12345',
          routes: null,
          template: 'supportFaq'
        }
        // {
        //   name: 'Privilege Banking',
        //   slug: 'privilege-banking',
        //   id: '12345',
        //   routes: [
        //     {
        //       name: 'ATM',
        //       slug: 'atm',
        //       id: '12345',
        //       routes: null,
        //       template: 'location'
        //     },
        //     {
        //       name: 'Contact Form',
        //       slug: 'contact-form',
        //       id: '12345',
        //       routes: null,
        //       template: 'contactForm'
        //     }
        //   ],
        //   template: 'services'
        // },
        // {
        //   name: 'Bank Assurance',
        //   slug: 'bank-assurance',
        //   id: '12345',
        //   routes: null,
        //   template: 'overview'
        // }
      ]
    },
    {
      name: 'Corporate',
      slug: 'corporate',
      id: '12345',
      routes: [
        {
          name: 'Business Account',
          slug: 'business-account',
          id: '12345',
          routes: null,
          template: 'overview'
        },
        {
          name: 'Business Loan',
          slug: 'business-loan',
          id: '12345',
          routes: null,
          template: 'overview'
        },
        {
          name: 'Trade Services',
          slug: 'trade-services',
          id: '12345',
          routes: null,
          template: 'fullWidthContentCard'
        },
        {
          name: 'Treasury',
          slug: 'treasury',
          id: '12345',
          routes: [
            {
              name: 'Correspondent Bank',
              slug: 'correspondent-bank',
              routes: null,
              template: 'correspondentBank'
            },
            {
              name: 'Forex',
              slug: 'forex',
              routes: null,
              template: null
            },
            {
              name: 'Bills/Bond',
              slug: 'bills-bond',
              routes: null,
              template: null
            },
            {
              name: 'Inter Bank Placements',
              slug: 'inter-bank-placements',
              routes: null,
              template: null
            },
            {
              name: 'Contract',
              slug: 'contracts',
              routes: null,
              template: null
            },
            {
              name: 'Research',
              slug: 'research',
              routes: null,
              template: null
            }
          ],
          template: null
        },
        {
          name: 'Rates & Fees',
          slug: 'rates-fees',
          id: '12345',
          routes: null,
          template: 'dailyRates'
        },
        {
          name: 'Faq',
          slug: 'faq',
          id: '12345',
          routes: null,
          template: 'supportFaq'
        }
        // {
        //   name: 'Investment',
        //   slug: 'investment',
        //   id: '12345',
        //   routes: null,
        //   template: 'fullWidthContentCard'
        // },
        // {
        //   name: 'Calculators',
        //   slug: 'calculators',
        //   id: '12345',
        //   routes: null,
        //   template: 'calculatorOverBg'
        // }
      ],
      template: 'corporate'
    },
    {
      name: 'SME',
      slug: 'sme',
      routes: [
        {
          name: 'Solar Loan',
          slug: 'solar-loan',
          id: '1233423',
          routes: null,
          template: null
        },
        {
          name: 'Agiculture Loan',
          slug: 'agriculture-loan',
          id: '1233423',
          routes: null,
          template: null
        }
      ],
      template: null
    },
    {
      name: 'Online Services',
      slug: 'online-services',
      id: '1233423',
      routes: [
        {
          name: 'Online Form',
          slug: 'online-form',
          id: '1233423',
          routes: null,
          template: null
        },
        {
          name: 'Online Payments',
          slug: 'bill-payments',
          id: '12345',
          routes: null,
          template: 'payments'
        }
      ],
      template: null
    }
  ],
  secondary: [
    {
      name: 'About',
      slug: 'about',
      id: '12345',
      routes: [
        {
          name: 'Overview',
          slug: 'overview',
          id: '12345',
          routes: null,
          template: 'about'
        },
        {
          name: 'Team',
          slug: 'team',
          id: '12345',
          routes: [
            {
              name: 'Board of Directors',
              slug: 'board-of-directors',
              id: '12345',
              routes: null,
              template: 'team'
            },
            {
              name: 'Management Team',
              slug: 'management-team',
              id: '12345',
              routes: null,
              template: 'managementTeam'
            }
          ],
          template: 'team'
        },
        {
          name: 'Risk Management',
          slug: 'risk-management',
          id: '12345',
          routes: null,
          template: 'fullWidthContentCard'
        },
        {
          name: 'Compliance',
          slug: 'compliance',
          id: '12345',
          routes: null,
          template: 'compliance'
        },
        {
          name: 'Investor Relations',
          slug: 'investor-relations',
          id: '123456',
          template: 'reports',
          routes: [
            {
              name: 'Reports',
              slug: 'reports',
              id: '12345',
              routes: null,
              template: 'reports'
            },
            {
              name: 'Financial Downloads',
              slug: 'financial-downloads',
              id: '12345',
              routes: null,
              template: 'financialDownloads'
            },
            {
              name: 'Strategy',
              slug: 'strategy',
              id: '12345',
              routes: null,
              template: 'strategy'
            },
            {
              name: 'Stock Charts',
              slug: 'stocks',
              id: '12345',
              routes: null,
              template: 'stocks'
            }
          ]
        },
        {
          name: 'CSR',
          slug: 'csr',
          id: '1233456',
          routes: null,
          template: 'csr'
        },
        {
          name: 'News',
          slug: 'news',
          id: '12345',
          routes: null,
          template: 'newsAndContent'
        },
        {
          name: 'Certification',
          slug: 'certification',
          id: '12345',
          routes: null,
          template: 'certification'
        },
        {
          name: 'Correspondent Bank',
          slug: 'correspondent-bank',
          id: '12345',
          routes: null,
          template: 'correspondentBank'
        },
        {
          name: 'Disclosure',
          slug: 'basel-disclosure',
          id: '12345',
          routes: null,
          template: 'baselDisclosure'
        },
        {
          name: 'Downloads',
          slug: 'downloads',
          id: '12345',
          routes: null,
          template: 'downloads'
        }
      ],
      template: 'about'
    },
    {
      name: 'Support',
      slug: 'support',
      id: '12345',
      routes: [
        {
          name: 'Tutorials',
          slug: 'tutorials',
          id: '12345',
          routes: null,
          template: 'tutorials'
        },
        {
          name: 'News',
          slug: 'news',
          id: '12345',
          routes: null,
          template: 'newsAndContent'
        },
        {
          name: 'Faq',
          slug: 'faq',
          id: '12345',
          routes: null,
          template: 'supportFaq'
        },
        {
          name: 'Lost/ Stolen Cards',
          slug: 'lost-stolen-cards',
          id: '12345',
          routes: null,
          template: 'lostStolenCards'
        },
        {
          name: 'Contact Form',
          slug: 'contact-form',
          id: '12345',
          routes: null,
          template: 'contactForm'
        }
      ],
      template: 'tutorials'
    },
    // {
    //   name: 'News and Content',
    //   slug: 'news-content',
    //   id: '12345',
    //   routes: null,
    //   template: 'newsAndContent'
    // },
    // {
    //   name: 'Shareholder',
    //   slug: 'shareholder',
    //   id: '12345',
    //   routes: null,
    //   template: 'shareholder'
    // },
    {
      name: 'Contact',
      slug: 'contact',
      id: '12345',
      routes: [
        {
          name: 'General Enquiry',
          slug: 'general-enquiry',
          id: '12345',
          routes: null,
          template: 'generalEnquiry'
        },
        {
          name: 'Corporate Office',
          slug: 'corporate-office',
          id: '12345',
          routes: null,
          template: 'corporateOffice'
        },
        {
          name: 'Location',
          slug: 'location',
          id: '12345',
          routes: [
            {
              name: 'Branches',
              slug: 'branches',
              id: '12345',
              routes: null,
              template: 'location'
            },
            {
              name: 'ATM',
              slug: 'atm',
              id: '12345',
              routes: null,
              template: 'location'
            },
            {
              name: 'Lockers',
              slug: 'lockers',
              id: '12345',
              routes: null,
              template: 'location'
            },
            {
              name: 'Branchless Banking',
              slug: 'branchless-banking',
              id: '12345',
              routes: null,
              template: 'location'
            }
          ],
          template: null
        },
        {
          name: 'Complaints',
          slug: 'complaints',
          id: '123312',
          routes: null,
          template: null
        },
        {
          name: 'Grievance',
          slug: 'grievance',
          id: '123312',
          routes: null,
          template: null
        },
        {
          name: 'Contact Us',
          slug: 'contact-us',
          id: '12345',
          routes: null,
          template: 'contactForm'
        },
        {
          name: 'Career',
          slug: 'career',
          id: '12345',
          routes: null,
          template: 'contactForm'
        },
        {
          name: 'Information Office',
          slug: 'information-office',
          id: '12345',
          routes: null,
          template: null
        }
      ],
      template: 'generalEnquiry'
    }
  ]
};
