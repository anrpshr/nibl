import
import Overview from '../components/__templates/Overview';

const getComponentFromTemplate = (templateSlug) => {
  switch (templateSlug) {
    case 'overview':
      return <Overview/>;
    default:
      return false;
  }
};

export default getComponentFromTemplate;
