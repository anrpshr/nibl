import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import { TEMPLATES } from '../constants';
import sitemap from './siteMap';
import Home from '../components/__pages/Home';
import DetailPage from '../components/__templates/DetailPage';
// import NotFound from '../components/__pages/NotFound';

const GenerateRoutes = ({ routes }) => {
  const allRoutes = routes.map((route, i) => {
    const currentSlug = route.slug;
    const childRoutes = route.routes && route.routes.length > 0 && route.routes.map((childRoute, ci) => {
      const currentChildSlug = childRoute.slug;
      const grandChildRoutes = childRoute.routes && childRoute.routes.length > 0 && childRoute.routes.map((grandChildRoute, gi) => {
        const Component = TEMPLATES[grandChildRoute.pageTemplate];
        return (
          <Route key={`route-${grandChildRoute.slug}-${gi}`} exact path={`/${currentSlug}/${currentChildSlug}/${grandChildRoute.slug}`} render={props => <Component pageId={grandChildRoute.menuId} {...props} {...grandChildRoute}/>}/>
        );
      });
      const Component = TEMPLATES[childRoute.pageTemplate];
      return (
        <React.Fragment key={`route-${childRoute.slug}-${ci}`}>
          <Route exact path={`/${currentSlug}/${childRoute.slug}`} render={props => <Component pageId={childRoute.menuId} {...props} {...childRoute}/>}/>
          { grandChildRoutes }
        </React.Fragment>
      );
    });
    const Component = TEMPLATES[route.pageTemplate];
    return (
      <React.Fragment key={`route-${route.slug}-${i}`}>
        <Route exact path={`/${route.slug}`} render={props => <Component pageId={route.menuId} {...props} {...route}/>}/>
        { childRoutes }
      </React.Fragment>
    );
  });

  return (
    <Switch>
      <Route exact path={sitemap.home} component={Home}/>
      <Route exact path={sitemap.newsContent} component={DetailPage}/>
      {
        allRoutes && allRoutes.length > 0 && (
          <>
            { allRoutes }
            {/* <Redirect to={sitemap.notFound} component={NotFound}/> */}
          </>
        )
      }
    </Switch>
  );
};

GenerateRoutes.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default GenerateRoutes;
