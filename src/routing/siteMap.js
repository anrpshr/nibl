export default {
  home: '/',
  notFound: '/notfound',
  newsContent: '/news-content/:id',
  career: {
    index: '/career',
    application: '/career/application'
  },
  csr: {
    index: '/csr',
    csrDetail: '/csr/csr-detail'
  },
  security: {
    index: '/security',
    securityInside: '/security/security-inside'
  },
  treasury: {
    index: '/treasury'
  },
  informationOffice: '/informationoffice',
  niblCapital: '/niblCapital',
  bankingDays: '/365daysbanking',
  ebanking: '/ebanking'
};
