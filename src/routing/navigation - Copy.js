import store from '../redux';
import setNavigation from '../redux/actions/navigation.actions';

const getNavigation = () => {
  const routes = {
    primary: [
      {
        name: 'Personal',
        slug: 'personal',
        id: '12345',
        template: 'blockListing',
        routes: [
          {
            name: 'Accounts',
            slug: 'accounts',
            id: '12345',
            template: 'blockListing',
            routes: [
              {
                name: 'Keta Keti Bachat Khata',
                slug: 'keta-keti-bachat-khata',
                id: '4',
                routes: [],
                template: 'overview',
                sections: [
                  {
                    name: 'Product Overview',
                    slug: 'overview'
                  },
                  {
                    name: 'Features',
                    slug: 'features'
                  },
                  {
                    slug: 'lost-stolen-cards',
                    name: 'Lost/Stolen Cards'
                  },
                  {
                    name: 'Tools and Downloads',
                    slug: 'tools-downloads'
                  },
                  {
                    name: 'Product Video',
                    slug: 'product-video'
                  },
                  {
                    name: 'FAQ',
                    slug: 'faq'
                  }
                ]
              },
              {
                name: 'Lotus Account',
                slug: 'lotus-account',
                id: '1',
                routes: [],
                template: 'overview'
              },
              {
                name: 'E-zee Saving Account',
                slug: 'ezee-account',
                id: '2',
                routes: [],
                template: 'overview'
              },
              {
                name: 'Afnai Bachat Khata',
                slug: 'afnai-bachat-khata',
                id: '3',
                routes: [],
                template: 'overview'
              },
              {
                name: 'Ezee Student Account',
                slug: 'students-savings-account',
                id: '5',
                routes: [],
                template: 'overview'
              },
              {
                name: 'NIBL Saving Bonanza',
                slug: 'savings-account',
                id: '6',
                routes: [],
                template: 'overview'
              },
              {
                name: 'Surakchya Bachat Khata',
                slug: 'surakchya-khata',
                id: '7',
                template: 'overview'
              }
            ]
          },
          {
            name: 'Loans',
            slug: 'loans',
            id: '12345',
            routes: [
              {
                name: 'NIBL Home Loan',
                slug: 'home-loan',
                id: '8',
                template: 'overview'
              },
              {
                name: 'NIBL Education Loan',
                slug: 'education-loan',
                id: '9',
                template: 'overview'
              },
              {
                name: 'NIBL Auto Loan',
                slug: 'auto-loan',
                id: '11',
                template: 'overview'
              },
              {
                name: 'NIBL Personal Loan',
                slug: 'nibl-personal-loan',
                id: '14',
                template: 'overview'
              }
              // {
              //   name: 'NIBL Home And Solar Loan',
              //   slug: 'solar-loan',
              //   id: '12',
              //   template: 'overview'
              // },
              // {
              //   name: 'NIBL EMI Scheme',
              //   slug: 'emi-scheme',
              //   id: '13',
              //   template: 'overview'
              // }
            ],
            template: 'blank'
          },
          {
            name: 'Credit Cards',
            slug: 'credit-cards',
            id: '5',
            routes: [],
            template: 'overview'
          },
          {
            name: 'Privilege Banking',
            slug: 'privilege-banking',
            id: '12345',
            routes: [
              {
                name: 'ATM',
                slug: 'atm',
                id: '12345',
                routes: null,
                template: 'location'
              },
              {
                name: 'Contact Form',
                slug: 'contact-form',
                id: '12345',
                routes: null,
                template: 'contactForm'
              }
            ],
            template: 'services'
          },
          {
            name: 'Services',
            slug: 'services',
            id: '12345',
            routes: [

              {
                name: 'ATM',
                slug: 'atm',
                id: '12345',
                routes: [],
                template: 'location'
              },
              {
                name: 'Payments',
                slug: 'payments',
                id: '12345',
                routes: [],
                template: 'payments'
              },
              {
                name: 'Mobile Banking',
                slug: 'mobile-banking',
                id: '12345',
                routes: [],
                template: 'mobileBanking'
              },
              {
                name: 'Locker',
                slug: 'locker',
                id: '12345',
                routes: [],
                template: 'locker'
              },
              {
                name: 'Branchless Banking',
                slug: 'branchless-banking',
                id: '12345',
                routes: [],
                template: 'branchlessBanking'
              }
            ],
            template: 'null'
          },
          {
            name: 'Rates & Fees',
            slug: 'rates-fees',
            id: '12345',
            routes: [
              {
                name: 'Daily Rates',
                slug: 'daily-rates',
                id: '12345',
                routes: [],
                template: 'dailyRates'
              },
              {
                name: 'Interest Rates',
                slug: 'interest-rates',
                id: '12345',
                routes: [],
                template: 'interestRates'
              },
              {
                name: 'Downloads',
                slug: 'downloads',
                id: '12345',
                routes: [],
                template: 'downloads'
              }
            ],
            template: null
          },
          {
            name: 'Tools & Calculators',
            slug: 'tools-calculators',
            id: '12345',
            routes: [],
            template: 'calculatorOverBg'
          },
          {
            name: 'Bank Assurance',
            slug: 'bank-assurance',
            id: '12345',
            routes: [],
            template: 'overview'
          }
        ]
      },
      {
        name: 'Corporate',
        slug: 'corporate',
        id: '12345',
        routes: [
          {
            name: 'Business Account',
            slug: 'business-account',
            id: '12345',
            routes: [],
            template: 'overview'
          },
          {
            name: 'Business Loan',
            slug: 'business-loan',
            id: '12345',
            routes: [],
            template: 'overview'
          },
          {
            name: 'Business Services',
            slug: 'business-services',
            id: '12345',
            routes: [],
            template: 'fullWidthContentCard'
          },
          {
            name: 'Insurance and Risk Management',
            slug: 'insurance-and-risk-management',
            id: '12345',
            routes: [],
            template: 'fullWidthContentCard'
          },
          {
            name: 'Investment',
            slug: 'investment',
            id: '12345',
            routes: [],
            template: 'fullWidthContentCard'
          },
          {
            name: 'Rates',
            slug: 'rates',
            id: '12345',
            routes: [],
            template: 'dailyRates'
          },
          {
            name: 'Calculators',
            slug: 'calculators',
            id: '12345',
            routes: [],
            template: 'calculatorOverBg'
          }
        ],
        template: 'corporate'
      }
    ],
    secondary: [
      {
        name: 'About Us',
        slug: 'about-us',
        id: '12345',
        routes: [
          {
            name: 'About NIBL',
            slug: 'about-nibl',
            id: '12345',
            routes: [],
            template: 'about'
          },
          {
            name: 'Team',
            slug: 'team',
            id: '12345',
            routes: [
              {
                name: 'Board of Directors',
                slug: 'board-of-directors',
                id: '12345',
                routes: [],
                template: 'team'
              },
              {
                name: 'Management Team',
                slug: 'management-team',
                id: '12345',
                routes: [],
                template: 'managementTeam'
              }
            ],
            template: 'team'
          },
          {
            name: 'Compliance',
            slug: 'compliance',
            id: '12345',
            routes: [],
            template: 'compliance'
          },
          {
            name: 'Certification',
            slug: 'certification',
            id: '12345',
            routes: [],
            template: 'certification'
          },
          {
            name: 'Correspondent Bank',
            slug: 'correspondent-bank',
            id: '12345',
            routes: [],
            template: 'correspondentBank'
          },
          {
            name: 'Disclosure',
            slug: 'basel-disclosure',
            id: '12345',
            routes: [],
            template: 'baselDisclosure'
          },
          {
            name: 'Downloads',
            slug: 'downloads',
            id: '12345',
            routes: [],
            template: 'downloads'
          }
        ],
        template: 'about'
      },
      {
        name: 'Investor Relations',
        slug: 'investor-relations',
        id: '123456',
        template: 'reports',
        routes: [
          {
            name: 'Reports',
            slug: 'reports',
            id: '12345',
            routes: null,
            template: 'reports'
          },
          {
            name: 'Financial Downloads',
            slug: 'financial-downloads',
            id: '12345',
            routes: null,
            template: 'financialDownloads'
          },
          {
            name: 'Strategy',
            slug: 'strategy',
            id: '12345',
            routes: null,
            template: 'strategy'
          },
          {
            name: 'Stock Charts',
            slug: 'stocks',
            id: '12345',
            routes: null,
            template: 'stocks'
          }
        ]
      },
      {
        name: 'Support',
        slug: 'support',
        id: '12345',
        routes: [
          {
            name: 'Tutorials',
            slug: 'tutorials',
            id: '12345',
            routes: [],
            template: 'tutorials'
          },
          {
            name: 'News',
            slug: 'news',
            id: '12345',
            routes: [],
            template: 'newsAndContent'
          },
          {
            name: 'Faq',
            slug: 'faq',
            id: '12345',
            routes: [],
            template: 'supportFaq'
          },
          {
            name: 'Lost/ Stolen Cards',
            slug: 'lost-stolen-cards',
            id: '12345',
            routes: [],
            template: 'lostStolenCards'
          },
          {
            name: 'Contact Form',
            slug: 'contact-form',
            id: '12345',
            routes: [],
            template: 'contactForm'
          }
        ],
        template: 'tutorials'
      },
      {
        name: 'Remittance',
        slug: 'remittance',
        id: '12345',
        routes: [
          {
            name: 'Prithivi Remitance',
            slug: 'prithivi-remitance',
            id: '12345',
            routes: [],
            template: 'prithiviRemitance'
          },
          {
            name: 'Domestic Remitance',
            slug: 'domestic-remitance',
            id: '12345',
            routes: [],
            template: 'domesticRemitance'
          },
          {
            name: 'International Networks',
            slug: 'international-networks',
            id: '12345',
            routes: [],
            template: 'internationalNetworks'
          },
          {
            name: 'Find Our Agent',
            slug: 'find-our-agent',
            id: '12345',
            routes: [],
            template: 'contactForm'
          }
        ],
        template: 'prithiviRemitance'
      },
      // {
      //   name: 'News and Content',
      //   slug: 'news-content',
      //   id: '12345',
      //   routes: [],
      //   template: 'newsAndContent'
      // },
      // {
      //   name: 'Shareholder',
      //   slug: 'shareholder',
      //   id: '12345',
      //   routes: [],
      //   template: 'shareholder'
      // },
      {
        name: 'Contact',
        slug: 'contact',
        id: '12345',
        routes: [
          {
            name: 'General Enquiry',
            slug: 'general-enquiry',
            id: '12345',
            routes: [],
            template: 'generalEnquiry'
          },
          {
            name: 'Corporate Office',
            slug: 'corporate-office',
            id: '12345',
            routes: [],
            template: 'corporateOffice'
          },
          {
            name: 'Location',
            slug: 'location',
            id: '12345',
            routes: [
              {
                name: 'Branches',
                slug: 'branches',
                id: '12345',
                routes: [],
                template: 'location'
              },
              {
                name: 'ATM',
                slug: 'atm',
                id: '12345',
                routes: [],
                template: 'location'
              },
              {
                name: 'Lockers',
                slug: 'lockers',
                id: '12345',
                routes: [],
                template: 'location'
              },
              {
                name: 'Branchless Banking',
                slug: 'branchless-banking',
                id: '12345',
                routes: [],
                template: 'location'
              }
            ],
            template: null
          },
          {
            name: 'Contact Form',
            slug: 'contact-form',
            id: '12345',
            routes: [],
            template: 'contactForm'
          }
        ],
        template: 'generalEnquiry'
      }
    ]
  };
  store.dispatch(setNavigation(routes));
};

export default getNavigation;
