import React, { Component } from 'react';
import { IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import BreakPoint, { BreakpointProvider, setDefaultBreakpoints } from 'react-socks';
import { Spin } from 'antd';

import store from './redux';
import { GLOBALS, API } from './constants';
import setLocale from './redux/actions/locale.actions';
import setNavigation from './redux/actions/navigation.actions';
import setCurrentPage from './redux/actions/currentpage.actions';
import { setSplashOpen } from './redux/actions/common.actions';
import { isSupportedLanguage, getLanguageMessageList } from './locales/locale.messages';

import Header from './components/__elements/Header';
import HeaderMobile from './components/__elements/HeaderMobile';
import Footer from './components/__elements/Footer';

import Career from './components/__pages/Career';
import Application from './components/__pages/Career/Application';
import Csr from './components/__pages/CSR';
import CsrDetail from './components/__pages/CSR/CsrDetail';
import Security from './components/__pages/Security';
import SecurityDetailPages from './components/__pages/Security/SecurityDetailPages';
import Treasury from './components/__pages/Treasury';
import InformationOffice from './components/__pages/InformationOffice';
import NiblCapital from './components/__pages/NiblCapital';
import BankingDays from './components/__pages/365DaysBanking';
import FixedRightIcons from './components/FixedRightIcon';
import SplashSlider from './components/SplashSlider';
import ScrollToTop from './components/ScrollToTop';
import Ebanking from './components/__pages/Ebanking';
import { getV2 } from './services/generalApi.services';
import makeSlugs from './routing/routeSlugs';

import GenerateRoutes from './routing/GenerateRoutes';
import siteMap from './routing/siteMap';

const mapStateToProps = state => (
  {
    lang: state.locale.lang,
    snack: state.snack,
    navigation: state.navigation,
    common: state.common
  }
);

setDefaultBreakpoints([
  { xs: 376 },
  { s: 426 },
  { m: 769 },
  { l: 1250 },
  { xl: 1500 }
]);

class App extends Component {
  constructor() {
    super();

    const navigatorLanguage = (
      navigator.language
      || navigator.browserLanguage
      || GLOBALS.DEFAULT_SITE_LANGUAGE
    )
      .substring(0, 2);

    let lang = '';
    if (localStorage.getItem(GLOBALS.LANG_KEY)) {
      lang = localStorage.getItem(GLOBALS.LANG_KEY);
    } else {
      lang = isSupportedLanguage(navigatorLanguage)
        ? navigatorLanguage
        : GLOBALS.DEFAULT_SITE_LANGUAGE;
    }

    store.dispatch(setLocale(lang));
  }

  componentDidMount() {
    // // status check
    // get(
    //   API.endPoints.ABOUT_TEST,
    //   response => console.log(response),
    //   error => console.log(error),
    // );

    // GET NAVIGATION
    getV2(API.endPoints.MENU)
      .then((routes) => {
        const path = window.location.pathname;
        const newRoutes = makeSlugs(routes);
        if (path === '/') {
          store.dispatch(setCurrentPage(newRoutes.primary[0].slug));
        } else {
          const pageSlug = path.split('/')[1];
          store.dispatch(setCurrentPage(pageSlug));
        }
        store.dispatch(setNavigation(newRoutes));
      })
      .catch();

    // splash token
    const splashToken = localStorage.getItem('NIBL_SPLASH');
    if (!splashToken) {
      localStorage.setItem('NIBL_SPLASH', 1);
      store.dispatch(setSplashOpen(0));
    }
  }

  componentDidUpdate(prevProps) {
    const { common } = this.props; // eslint-disable-line

    if (common.modalOpen !== prevProps.common.modalOpen) { // eslint-disable-line
      this.toggleModalOpenClass();
    }
  }

  toggleModalOpenClass = () => {
    const { common } = this.props; // eslint-disable-line

    const body = document.querySelector('body');
    if (common.modalOpen) {
      body.classList.add('modal-open');
    } else {
      body.classList.remove('modal-open');
    }
  }

  render() {
    // const { lang, messages } = this.state;
    const { lang, snack, navigation, common } = this.props; // eslint-disable-line

    return (
      <>
        {
          navigation
            ? (
              <IntlProvider locale={lang} messages={getLanguageMessageList(lang)}>
                <BrowserRouter>
                  <>
                    <ScrollToTop/>
                    <BreakpointProvider>
                      <div id="wrapper">
                        <Snackbar
                          anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'center'
                          }}
                          open={snack.message !== ''}
                          autoHideDuration={snack.duration}
                          onClose={this.closeSnack}
                          message={<span id="message-id">{snack.message}</span>}
                          action={[
                            <IconButton
                              key="close"
                              aria-label="Close"
                              color="inherit"
                              onClick={this.closeSnack}
                            >
                              <CloseIcon/>
                            </IconButton>
                          ]}
                        />
                        {
                          common.splashOpen && <SplashSlider/>
                        }
                        <BreakPoint m down>
                          <HeaderMobile/>
                        </BreakPoint>
                        <BreakPoint l up>
                          <Header/>
                        </BreakPoint>

                        <GenerateRoutes routes={[...navigation.primary, ...navigation.secondary]}/>
                        
                        <Switch>
                          <Route exact path={siteMap.career.index} component={Career}/>
                          <Route exact path={siteMap.career.application} component={Application}/>
                          <Route exact path={siteMap.csr.index} component={Csr}/>
                          <Route exact path={siteMap.csr.csrDetail} component={CsrDetail}/>
                          <Route exact path={siteMap.security.index} component={Security}/>
                          <Route exact path={siteMap.security.securityInside} component={SecurityDetailPages}/>
                          <Route exact path={siteMap.treasury.index} component={Treasury}/>
                          <Route exact path={siteMap.informationOffice} component={InformationOffice}/>
                          <Route exact path={siteMap.niblCapital} component={NiblCapital}/>
                          <Route exact path={siteMap.bankingDays} component={BankingDays}/>
                          <Route exact path={siteMap.ebanking} component={Ebanking}/>
                        </Switch>
                        
                        <FixedRightIcons/>

                        <Footer />
                      </div>
                    </BreakpointProvider>
                  </>
                </BrowserRouter>
              </IntlProvider>
            )
            : (
              <div className="site-loading">
                <Spin/>
              </div>
            )
        }
      </>
    );
  }
}

export default connect(mapStateToProps, null)(App);
