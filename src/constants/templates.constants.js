import Overview from '../components/__templates/Overview';
import About from '../components/__templates/About';
import Payments from '../components/__templates/Payments';
import Location from '../components/__templates/Location';
import BranchLocation from '../components/__templates/BranchLocation';
import MobileBanking from '../components/__templates/MobileBanking';
import Locker from '../components/__templates/Lockers';
import BranchlessBanking from '../components/__templates/BranchlessBanking';
import Downloads from '../components/__templates/Downloads';
import DailyRates from '../components/__templates/DailyRates';
import InterestRates from '../components/__templates/InterestRates';
import Services from '../components/__templates/Services';
import Team from '../components/__templates/BoardOfDirectors';
import ManagementTeam from '../components/__templates/ManagementTeam';
import Compliance from '../components/__templates/Compliance';
import Certification from '../components/__templates/Certification';
import BaselDisclosure from '../components/__templates/BaselDisclosure';
import CorrespondentBank from '../components/__templates/CorrespondentBank';
import PrithiviRemitance from '../components/__templates/PrithiviRemit';
import DomesticRemitance from '../components/__templates/DomesticRemit';
import InternationalNetworks from '../components/__templates/InternationalNetwork';
import AgentContactForm from '../components/__templates/ContactForm';
import Shareholder from '../components/__templates/Shareholder';
import Corporate from '../components/__templates/CorporateLanding';
import FullWidthContentCard from '../components/__templates/FullWidthContentCard';
import CalculatorOverBg from '../components/__templates/CalculatorOverBg';
import NewsAndContent from '../components/__templates/NewsAndContent';
import Tutorials from '../components/__templates/Tutorials';
import SupportFaq from '../components/__templates/SupportFaq';
import LostStolenCards from '../components/__templates/LostStolenCards';
import ContactForm from '../components/__templates/SupportContact';
import GeneralEnquiry from '../components/__templates/GeneralEnquiry';
import CorporateOffice from '../components/__templates/CorporateOffice';
import Reports from '../components/__templates/Reports';
import FinancialDownloads from '../components/__templates/FinancialDownloads';
import Strategy from '../components/__templates/Strategy';
import Stock from '../components/__templates/Stock';
import BlockListing from '../components/__templates/BlockListing';
import CSR from '../components/__pages/CSR';
import Career from '../components/__pages/Career';
import Treasury from '../components/__pages/Treasury';
import InformationOffice from '../components/__pages/InformationOffice';
import TeamImages from '../components/__templates/TeamImages';

export default {
  about: About,
  overview: Overview,
  payments: Payments,
  services: Services,
  location: Location,
  mobileBanking: MobileBanking,
  locker: Locker,
  branchLocation: BranchLocation,
  branchlessBanking: BranchlessBanking,
  downloads: Downloads,
  dailyRates: DailyRates,
  interestRates: InterestRates,
  team: Team,
  teamImages: TeamImages,
  managementTeam: ManagementTeam,
  compliance: Compliance,
  certification: Certification,
  baselDisclosure: BaselDisclosure,
  correspondentBank: CorrespondentBank,
  prithiviRemitance: PrithiviRemitance,
  domesticRemitance: DomesticRemitance,
  internationalNetworks: InternationalNetworks,
  agentContactForm: AgentContactForm,
  shareholder: Shareholder,
  corporate: Corporate,
  fullWidthContentCard: FullWidthContentCard,
  calculatorOverBg: CalculatorOverBg,
  newsAndContent: NewsAndContent,
  tutorials: Tutorials,
  supportFaq: SupportFaq,
  lostStolenCards: LostStolenCards,
  contactForm: ContactForm,
  generalEnquiry: GeneralEnquiry,
  corporateOffice: CorporateOffice,
  reports: Reports,
  financialDownloads: FinancialDownloads,
  strategy: Strategy,
  stocks: Stock,
  blockListing: BlockListing,
  csr: CSR,
  career: Career,
  treasury: Treasury,
  informationOffice: InformationOffice
};
