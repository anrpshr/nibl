import USDFlag from '../assets/svg/flags/USD.svg';
import AUDFlag from '../assets/svg/flags/AUD.svg';
import EURFlag from '../assets/svg/flags/EUR.svg';
import GBPFlag from '../assets/svg/flags/GBP.svg';
import SGDFlag from '../assets/svg/flags/SGD.svg';

export default {
  loginURL: 'http://pagodalabs.com.np/nibllogin',
  LANG_KEY: '__nibl__lang__',
  SCROLL_ANCHOR_THRESHOLD: 700,
  SCROLL_ANCHOR_DEBOUNCE: 1000,
  FOREX_CURRENCIES: {
    USD: USDFlag,
    AUD: AUDFlag,
    EUR: EURFlag,
    GBP: GBPFlag,
    SGD: SGDFlag
  }
};
