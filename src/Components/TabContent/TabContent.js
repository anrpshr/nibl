import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';

import Styles from './styles.module.scss';

const TabContent = ({ children }) => {
  const tabPanes = children.map((child, i) => (
    <CSSTransition
      in={child.props.isActive}
      timeout={300}
      classNames="tab-pane"
      key={`css-trans-${i}`}
      unmountOnExit
    >
      <div className={Styles.tabPane}>
        {child}
      </div>
    </CSSTransition>
  ));
  return (
    <div className={Styles.wrapper}>
      {tabPanes}
    </div>
  );
};

TabContent.propTypes = {
  children: PropTypes.node.isRequired
};

export default TabContent;
