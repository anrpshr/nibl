import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';

import Styles from './generalBlocks.module.scss';

// Components
import ContentCard from '../ContentCard';

const GeneralBlocks = ({ blocks }) => {
  const blockList = blocks.map(block => block.description && (
    <Col span={8}>
      <div className={Styles.block}>
        <ContentCard>
          <h4>{block.title}</h4>
          <p>
            {block.description}
          </p>
          {
            block.link && <a href={block.link}>Learn More</a>
          }
        </ContentCard>
      </div>
    </Col>
  ));

  return (
    <Row type="flex" gutter={36}>
      {blockList}
    </Row>
  );
};

GeneralBlocks.propTypes = {
  blocks: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default GeneralBlocks;
