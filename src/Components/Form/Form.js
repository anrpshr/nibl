import React from 'react';
import PropTypes from 'prop-types';
import Styles from './form.module.scss';

// Components
import BigTitleBlock from '../Layouts/Block/BigTitleBlock';

const Form = ({ title, description, background, children, secondaryTitle, formTitle }) => (
  <div className={Styles.wrapper} style={{ background: `url(${background})`, backgroundSize: 'cover' }}>
    <div className="container">
      <div className={Styles.flexSection}>
        <div className={Styles.description}>
          <h1>{title}</h1>
          <div className={Styles.text}>
            <p>{description}</p>
          </div>
        </div>
        <div className={Styles.form}>
          <h3>{secondaryTitle}</h3>
          <form>
            <BigTitleBlock title={formTitle}>
              {children}
            </BigTitleBlock>
          </form>
        </div>
      </div>
    </div>
  </div>
);

Form.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  background: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  secondaryTitle: PropTypes.string.isRequired,
  formTitle: PropTypes.string.isRequired
};

export default Form;
