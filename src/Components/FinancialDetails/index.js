import React from 'react';
import PropTypes from 'prop-types';
import Styles from './styles.module.scss';

// Components
import ContentHeader from '../ContentHeader';
import ExpansionPanel from '../ExpansionPanel';

// Images
import downloadIcon from '../../assets/img/download.png';

const FinancialDetails = ({ details, title }) => {
  const detailsRow = details.map(item => (
    {
      primary:
      (
        <div className={`${Styles.primaryPanel}`}>
          <div>
            <p>
              {item.primary}
            </p>
          </div>
          <a href={`${item.downloadLink}`}><img src={downloadIcon} alt=""/></a>
        </div>
      ),
      secondary: <div>{item.secondary}</div>
    }
  ));

  return (
    <div>
      <div className="heading">
        <h1>{title}</h1>
      </div>
      <ContentHeader grey>
        <div className={Styles.contentHeader}>
          <span className={Styles.sn}>S.N</span>
          <span className={Styles.details}>Details</span>
        </div>
      </ContentHeader>
      <ExpansionPanel
        isOrdered
        items={detailsRow}
      />
    </div>
  );
};

FinancialDetails.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object).isRequired,
  title: PropTypes.string.isRequired
};

export default FinancialDetails;
