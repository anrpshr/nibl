import React, { Component } from 'react';
import { AiOutlineSearch, TiLocationArrowOutline } from 'react-icons/all';
import { string } from 'prop-types';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

import store from '../../redux';
import { showGlobalSnack } from '../../redux/actions/snack.actions';
import ExpansionPanel from '../ExpansionPanel';
import './Locator.scss';

class Locator extends Component {
  state = {
    search: '',
    markers: [],
    activeMarker: null,
    filteredLocations: []
  };

  mapRef = React.createRef();

  bounds = null;

  libraries = ['geometry'];

  populateMarkers = () => {
    const { locations } = this.props;
    const { activeMarker } = this.state;

    this.bounds = new window.google.maps.LatLngBounds();

    let filteredList;

    if (activeMarker !== null) {
      filteredList = locations.filter((i, index) => activeMarker === index);
    } else {
      filteredList = locations.filter(e => e.isValid);
    }

    const markerList = filteredList.map((location, index) => {
      const loc = new window.google.maps.LatLng(location.lat, location.lng);
      this.bounds.extend(loc);

      return (
        <Marker
          key={`marker-${index}`}
          onClick={() => { this.openPanel(index); }}
          position={{
            lat: location.lat,
            lng: location.lng
          }}
        />
      );
    });

    this.setState({ markers: markerList }, () => {
      this.mapRef.fitBounds(this.bounds);
    });
  }

  setLocation = (locationIndex) => {
    this.setState({ activeMarker: locationIndex }, () => {
      this.populateMarkers();
    });
  }

  setViewAll = () => {
    this.setState({ activeMarker: null }, () => {
      this.populateMarkers();
    });
  }

  filterLocations = (search) => {
    const { locations } = this.props;
    const filteredLocations = locations.filter(location => location.address.toLowerCase().includes(search));
    this.setState({ filteredLocations, search });
  }

  getNearestLocation = () => {
    const { locations } = this.props;
    const { filteredLocations } = this.state;

    const locationsResult = filteredLocations.length > 0 ? filteredLocations : locations;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        const distances = locationsResult.map((location) => {
          const from = new window.google.maps.LatLng(pos.lat, pos.lng);
          const to = new window.google.maps.LatLng(location.lat, location.lng);
          return window.google.maps.geometry.spherical.computeDistanceBetween(from, to);
        });

        // floor distances
        const flooredDistances = distances.map(distance => Math.floor(distance));
        const minDistance = Math.min(...flooredDistances);

        flooredDistances.forEach((distance, index) => {
          if (distance === minDistance) {
            this.setLocation(index);
          }
        });
      });
    } else {
      store.dispatch(showGlobalSnack('normal', 'Sorry, your browser does not support location. Please update your browser'));
    }
  }

  render() {
    const { title, locations } = this.props;
    const { markers, activeMarker, filteredLocations, search } = this.state;

    const locationsResult = filteredLocations.length > 0 ? filteredLocations : locations;

    const locationList = locationsResult.map((location, index) => (
      {
        primary: (
          <div
            role="button"
            tabIndex="-1"
            onClick={() => {
              if (location.isValid) {
                this.setLocation(index);
              }
            }}
            key={`location-title-${index}`}
          >
            {location.name}
          </div>
        ),
        secondary:
        (
          <div className="secondary" key={`location-body-${index}`}>
            <div>
              { location.address && <span>{location.address}</span> }
              { location.phoneNumber && <span>Tel: {location.phoneNumber}</span> }
              { location.faxNumber && <span>Fax: {location.faxNumber}</span> }
            </div>
          </div>
        )
      }
    ));

    return (
      <div id="locator-map">
        <div className="map-sidebar">
          <div className="search-map-wrap">
            <input
              type="text"
              placeholder="Search a location"
              className="search-map"
              value={search}
              onChange={e => this.filterLocations(e.target.value)}
            />
            <AiOutlineSearch/>
          </div>
          <div
            className="title"
            role="button"
            tabIndex="-1"
            onClick={this.getNearestLocation}
          >
            {title}
          </div>
          <div className="locations">
            <ExpansionPanel
              isOrdered={false}
              compact
              items={locationList}
            />
            {
              activeMarker !== null && (
                <button onClick={this.setViewAll} className="view-all-btn">
                  View All
                </button>
              )
            }
          </div>
        </div>
        <div className="map-holder">
          <LoadScript
            id="script-loader"
            googleMapsApiKey="AIzaSyDMxUSJ48hvYmWupZGlbNyOcsFIjS_kVHE"
            libraries={this.libraries}
          >
            <GoogleMap
              id="example-map"
              mapContainerStyle={{ width: '100%', height: '100%' }}
              defaultZoom={17}
              center={{
                lat: locations[0].lat,
                lng: locations[0].lng
              }}
              onLoad={(map) => {
                this.mapRef = map;
                map.setOptions({
                  maxZoom: 16,
                  minZoom: 8
                });
                this.bounds = new window.google.maps.LatLngBounds();
                this.populateMarkers();
              }}
            >
              {markers}
            </GoogleMap>
          </LoadScript>
        </div>
      </div>
    );
  }
}

Locator.propTypes = {
  title: string.isRequired
};

export default Locator;
