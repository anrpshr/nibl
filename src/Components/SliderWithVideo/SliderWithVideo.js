import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Styles from './sliderWithVideo.module.scss';

// Components
import SlickSlider from '../SlickSlider';
import VideoModalNoText from '../VideoModalNoText';

// Images
import vidImg from '../../assets/img/vidImg.jpg';
import playBtn from '../../assets/img/play.png';

// Video
import vid from '../../assets/vid/video.mp4';

const NextArrow = ({ className, style, onClick }) => (
  <button
    className={className}
    style={{ ...style, display: 'block', background: 'none' }}
    onClick={onClick}
  >
    <ChevronRight />
  </button>
);

const PrevArrow = ({ className, style, onClick }) => (
  <button
    className={className}
    style={{ ...style, display: 'block', background: 'none' }}
    onClick={onClick}
  >
    <ChevronLeft />
  </button>
);

const settings = {
  dots: false,
  infinite: true,
  fade: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  nextArrow: <NextArrow />,
  prevArrow: <PrevArrow />
};

class SliderWithVideo extends Component {
  state = {
    slider: []
  };

  componentDidMount() {
    this.setState({
      slider: [
        {
          content: {
            url: vidImg,
            height: 778
          }
        },
        {
          content: {
            url: vidImg,
            height: 778
          }
        },
        {
          content: {
            url: vidImg,
            height: 778
          }
        },
        {
          content: {
            url: vidImg,
            height: 778
          }
        }
      ]
    });
  }

  render() {
    const { slider } = this.state;
    const singleSlide = slider.map(slide => (
      <div className={Styles.slide}>
        {/* <img src={slide.content.url} alt=""/>
        <div>
          <img src={playBtn} alt=""/>
        </div> */}
        <VideoModalNoText videoId="9xwazD5SyVg" videoThumb={slide.content.url}/>
      </div>
    ));
    return (
      <div className={Styles.sliderWrapper}>
        <SlickSlider settings={settings}>
          {singleSlide}
        </SlickSlider>
      </div>
    );
  }
}

NextArrow.propTypes = {
  className: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  onClick: PropTypes.func
};

NextArrow.defaultProps = {
  className: '',
  style: {},
  onClick: null
};

PrevArrow.propTypes = {
  className: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  onClick: PropTypes.func
};

PrevArrow.defaultProps = {
  className: '',
  style: {},
  onClick: null
};

export default SliderWithVideo;
