import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styles from './rnd.module.scss';

// Components
import ContentCard from '../../../ContentCard';
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';

import RndTbl from './RndTbl';

class ResearchAndDevelopment extends Component {
  state = {
    items: [
      {
        name: 'Research Project',
        key: 'research-project'
      },
      {
        name: 'Forex Reports',
        key: 'forex-reports'
      },
      {
        name: 'Inflation Reports',
        key: 'inflation-reports'
      }
    ],
    activeItemKey: 'research-project'
  }

  render() {
    const { items, activeItemKey } = this.state;
    const { title, content } = this.props;
    return (
      <div className={Styles.rnd}>
        <div className="container">
          <div className="heading">
            <h1>Research</h1>
          </div>
          <ContentCard>
            <p>
              {content}
              The department updates the exchange rates daily and
              as per the decisions of FEDAN. The exchange rates are
              based on the movements of INR against USD.
            </p>
            <p>
              The department maintains the foreign currency position
              as per the needs of the bank. Mostly, the currencies are
              squared off at day end to minimize the risks related to
              exchange rate fluctuations. Mostly spot transactions are
              carried out for sales/ purchase of the foreign currencies.
            </p>
          </ContentCard>
        </div>

        <div className={Styles.sectionWrapper}>
          <TabNav
            onClick={(key) => { this.setState({ activeItemKey: key }); }}
            activeItemKey={activeItemKey}
            items={items}
          />
          <div className="container">
            <div className={Styles.tabContentWrapper}>
              <TabContent>
                <RndTbl title="Download Research Projects" isActive={activeItemKey === 'research-project'}/>
                <RndTbl title="R &amp; D Forex Reports" isActive={activeItemKey === 'forex-reports'}/>
                <RndTbl title="R &amp; D Inflation Reports" isActive={activeItemKey === 'inflation-reports'}/>
              </TabContent>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ResearchAndDevelopment.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default ResearchAndDevelopment;
