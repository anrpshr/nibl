import React, { Component } from 'react';
import Styles from './treasuryInsidePage.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import ForeignExchange from './ForeignExchange';
import ResearchAndDevelopment from './ResearchAndDevelopment';
import PageWithBasicContent from './PageWithBasicContent';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';

class Treasury extends Component {
  state = {
    items: [
      {
        name: 'Foreign Exchange',
        key: 'foreign-exchange'
      },
      {
        name: 'Fund Management',
        key: 'fund-management'
      },
      {
        name: 'Bills/ Bonds',
        key: 'bills-bonds'
      },
      {
        name: 'Inter-Bank Placements',
        key: 'inter-bank-placements'
      },
      {
        name: 'Research',
        key: 'research'
      },
      {
        name: 'Forward Contracts',
        key: 'forward-contracts'
      },
      {
        name: 'NRB Reports',
        key: 'nrb-reports'
      }
    ],
    activeItemKey: 'foreign-exchange'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
          title="Foreign Exchange"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
          </div>
          <TabContent>
            <ForeignExchange title="Foreign Exchange" isActive={activeItemKey === 'foreign-exchange'}/>
            <PageWithBasicContent title="Fund Management" isActive={activeItemKey === 'fund-management'}/>
            <PageWithBasicContent title="Bills/ Bonds" isActive={activeItemKey === 'bills-bonds'}/>
            <PageWithBasicContent title="Inter-Bank Placements" isActive={activeItemKey === 'inter-bank-placements'}/>
            <ResearchAndDevelopment isActive={activeItemKey === 'research'}/>
            <PageWithBasicContent title="Forward Contracts" isActive={activeItemKey === 'forward-contracts'}/>
            <PageWithBasicContent title="NRB Reports" isActive={activeItemKey === 'nrb-reports'}/>
          </TabContent>
        </div>
      </div>
    );
  }
}

export default Treasury;
