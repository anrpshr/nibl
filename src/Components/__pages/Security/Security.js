import React, { Component } from 'react';
import Styles from './security.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import Content from './Content';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';


class Security extends Component {
  state = {
    items: [
      {
        name: 'Phising',
        key: 'phising'
      },
      {
        name: 'Safe Banking',
        key: 'safe-banking'
      },
      {
        name: 'Email Fraud',
        key: 'email-fraud'
      },
      {
        name: 'ATM Skimmers',
        key: 'atm-skimmers'
      },
      {
        name: 'Online Safety',
        key: 'online-safety'
      },
      {
        name: 'Download',
        key: 'download'
      }
    ],
    activeItemKey: 'phising'
  };

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <>
        <BannerBlock background={banner}/>
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                stretch
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            <TabContent>
              <Content isActive={activeItemKey === 'phising'}/>
              <Content isActive={activeItemKey === 'safe-banking'}/>
              <Content isActive={activeItemKey === 'email-fraud'}/>
              <Content isActive={activeItemKey === 'atm-skimmers'}/>
              <Content isActive={activeItemKey === 'online-safety'}/>
              <Content isActive={activeItemKey === 'download'}/>
            </TabContent>
          </div>
        </div>
      </>
    );
  }
}

export default Security;
