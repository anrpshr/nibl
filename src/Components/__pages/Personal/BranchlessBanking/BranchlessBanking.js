import React, { Component } from 'react';
import LocationOn from '@material-ui/icons/LocationOn';
import Styles from './branchlessBanking.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import BranchlessBankingContent from './BranchlessBankingContent';
import Strip from '../../../Strip';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import banner from '../../../../assets/img/mobtop-bg.jpg';

class BranchlessBanking extends Component {
  state = {
    items: [
      {
        name: 'Branchless Banking',
        key: 'branchless-banking'
      },
      {
        name: 'How does it work',
        key: 'how-does-it-work'
      }
    ],
    activeItemKey: 'branchless-banking'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title="Banchless Banking"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
          background={banner}
        />
        <div className="greyWrapper">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <TabContent>
              <BranchlessBankingContent isActive={activeItemKey === 'branchless-banking'}/>
              <BranchlessBankingContent isActive={activeItemKey === 'how-does-it-works'}/>
            </TabContent>
          </div>
        </div>
        <div className={Styles.strip}>
          <Strip btnTitle="Download">
            <span>Download the Branchless Banking Application form.</span>
          </Strip>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default BranchlessBanking;
