import React, { Component } from 'react';
import Styles from './ketaketi.module.scss';

// Components
import BannerBlock from '../../../../BannerBlock';
import TabNav from '../../../../TabNav';
import LeftImageText from '../../../../LeftImageText';
import ButtonRedBorder from '../../../../ButtonRedBorder';
import FeaturesAndBenefits from '../../../../FeaturesAndBenefits';
import Strip from '../../../../Strip';
import SecondaryBanner from '../../../../SecondaryBanner';
import GeneralBlocks from '../../../../generalBlocks';
import SliderWithVideo from '../../../../SliderWithVideo';
import ExpansionPanel from '../../../../ExpansionPanel';

// Images
import banner from '../../../../../assets/img/product-bg.jpg';
import dummyImg from '../../../../../assets/img/nibl-dummy.jpg';
import secondaryBanner from '../../../../../assets/img/rectangle.jpg';
import atmImg from '../../../../../assets/img/atm.jpg';
import BigTitleBlockSet from '../../../../Layouts/Block/BigTitleBlockSet';

class KetaKeti extends Component {
  state = {
    items: [
      {
        name: 'Product Overview',
        key: 'product-overview'
      },
      {
        name: 'Features',
        key: 'features'
      },
      {
        name: 'Lost/ Stolen Cards',
        key: 'Lost-stolen-cards'
      },
      {
        name: 'Tools and Downloads',
        key: 'tools-and-downloads'
      },
      {
        name: 'Product-video',
        key: 'product-video'
      },
      {
        name: 'FAQ',
        key: 'faq'
      }
    ],
    activeItemKey: 'product-overview'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
          title="Keta Keti Bachat Khata"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
          link="1"
          linkTitle="Apply Now"
        />
        <div className={Styles.greyInner}>
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <div className={Styles.heading}>
              <h1>Product Overview</h1>
            </div>
            <div className={Styles.contentWrapper}>
              <LeftImageText
                title="Product Details"
                excerpt="Interest Rate Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
                image={dummyImg}
              >
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                  ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
                  dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                  ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat
                  massa quis enim.
                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                  ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
                  dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
                  nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                </p>
                <ButtonRedBorder title="Apply Now"/>
              </LeftImageText>
              <FeaturesAndBenefits firstTableTitle="Features" secondTableTitle="Benefits"/>
            </div>
          </div>
        </div>
        <div className={Styles.searchSectionWrapper}>
          <Strip btnTitle="subcribe">
            <input type="text" placeholder="Enter your email address"/>
          </Strip>
        </div>
        <SecondaryBanner
          background={secondaryBanner}
          image={atmImg}
          title="Lost and Stolen Cards"
          description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor"
        />
        <div className="container">
          <GeneralBlocks/>
          <SliderWithVideo/>
          <div className={Styles.faqWrapper}>
            <h1>FAQ</h1>
            <ExpansionPanel
              isOrdered
              items={[
                {
                  primary: <div>Which browsers support NIBL eBanking?</div>,
                  secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
                },
                {
                  primary: <div>Which browsers support NIBL eBanking?</div>,
                  secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
                },
                {
                  primary: <div>Which browsers support NIBL eBanking?</div>,
                  secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
                },
                {
                  primary: <div>Which browsers support NIBL eBanking?</div>,
                  secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
                },
                {
                  primary: <div>Which browsers support NIBL eBanking?</div>,
                  secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
                }
              ]}
            />
          </div>
        </div>
        <div className={Styles.applyNow}>
          <Strip btnTitle="Apply Now">
            <span>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            </span>
          </Strip>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default KetaKeti;
