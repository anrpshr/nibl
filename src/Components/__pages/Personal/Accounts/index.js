import React from 'react';
import { Route, Switch } from 'react-router-dom';

import sitemap from '../../../../routing/siteMap';

// Components
import KetaKeti from './KetaKeti';

const Accounts = () => (
  <Switch>
    <Route exact path={sitemap.personal.accounts.ketaKeti} component={KetaKeti}/>
  </Switch>
);

export default Accounts;
