import React, { Component } from 'react';
import Styles from './services.module.scss';

// Components
import Card from '../../../Layouts/Cards';
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';
import BannerBlock from '../../../BannerBlock';

// Images
import banner from '../../../../assets/img/payment-bg.jpg';
import dummy from '../../../../assets/img/dummy.png';
import atmIcon from '../../../../assets/img/card.png';
import billIcon from '../../../../assets/img/bill.png';
import branchlessIcon from '../../../../assets/img/branchless.png';
import dummyGirl from '../../../../assets/img/dummygirl.png';

class PersonalServices extends Component {
  state = {
    posts: [],
    loading: true
  };

  componentDidMount() {
    this.setState({
      loading: false,
      posts: [
        {
          isFeatured: false,
          title: 'Payments',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'payments',
          id: 1,
          image: dummy,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Atm',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'atm',
          id: 2,
          image: null,
          icon: atmIcon,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Locker',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'locker',
          id: 3,
          image: null,
          icon: billIcon,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Branchless Banking',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'branchless-banking',
          id: 4,
          image: null,
          icon: branchlessIcon,
          backgroundColor: 'blue'
        },
        {
          isFeatured: true,
          title: 'Mobile (Sms) Banking',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'mobile-sms-service',
          id: 5,
          image: {
            url: dummyGirl,
            width: 393,
            height: 636
          },
          icon: null,
          backgroundColor: null
        }
      ]
    });
  }

  render() {
    const { posts, loading } = this.state;
    const featuredBlock = posts.filter(post => post.isFeatured);
    const nonFeaturedBlocks = posts.filter(post => !post.isFeatured);

    // Mapping nonFeaturedBlocks
    const nonFeaturedBlockList = nonFeaturedBlocks.map((post) => {
      if (post.image) {
        return (
          <ImageBlockWithOverlay title={post.title} image={post.image}>
            <p>
              {post.excerpt}
              <a href="1">more</a>
            </p>
          </ImageBlockWithOverlay>
        );
      } return (
        <Card
          color={post.backgroundColor}
          title={post.title}
          image={post.icon}
          content={post.excerpt}
        />
      );
    });

    // Mapping featuredBlock
    const featuredBlockList = featuredBlock.map(post => (
      <ImageBlockWithOverlay title={post.title} image={post.image.url}>
        <p>
          {post.excerpt}
        </p>
      </ImageBlockWithOverlay>
    ));
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title="Safe Browsing"
          background={banner}
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes."
        />
        <div className={Styles.servicesWrapper}>
          <div className="container">
            <h1>Services</h1>
            <div className={Styles.sectionWrapper}>
              <div className={Styles.sectionLeft}>
                {nonFeaturedBlockList}
              </div>

              <div className={Styles.sectionRight}>
                {featuredBlockList}
              </div>
            </div>
          </div>
        </div>
        <BigTitleBlockSet />
      </div>
    );
  }
}

export default PersonalServices;
