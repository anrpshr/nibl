import React from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Styles from './mobileBanking.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import LeftImageText from '../../../LeftImageText';
import ContentCard from '../../../ContentCard';
import ContentHeader from '../../../ContentHeader';
import ExpansionPanel from '../../../ExpansionPanel';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';


// Images
import background from '../../../../assets/img/mobtop-bg.jpg';
import dummyImg from '../../../../assets/img/nibl-dummy.jpg';
import downloadIcon from '../../../../assets/img/download.png';

const MobileBanking = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      background={background}
      title="Mobile Banking"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className={Styles.greyBack}>
      <div className="container">
        <div className={Styles.heading}>
          <h1>Mobile Banking</h1>
        </div>
        <div className={Styles.contentWrapper}>
          <LeftImageText title="Details" image={dummyImg}>
            <p>
              NIBL Customers can now subscribe to NIBL’s Mobile (SMS) Banking and
              avail its services. Through this subscription NIBL customers can perform
              a number of financial and non-financial transactions up to NPR 5,000 per
              transaction, NPR 15,000 per day and NPR 50,000 per month (Bill Payment
              transactions do not have any limit).  Customers can use NIBL Mobile Banking
              Application or send an SMS to 37755 (NIBL’s short code) with the keyword
              (listed below) and avail the service. Customers will be charged Telecom’s
              standard SMS rate while sending the SMS.
            </p>
            <p>
              Customers can subscribe to this service, by filling out a simple form in the
              Customer Service Department. The PIN is provided to the customer in the PIN mailer.
              The service will be made available the instance customer sends a
              <span>PIN change request to 37755.</span>
            </p>
          </LeftImageText>
        </div>
        <div className={Styles.heading}>
          <h1>Features</h1>
        </div>
        <ContentCard>
          <div className={Styles.features}>
            <ul>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
              <li>
                <ChevronRight color="primary"/>
                Balance Inquiry
              </li>
            </ul>
          </div>
        </ContentCard>
      </div>
    </div>
    <div className="container">
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader>
          <span>S.N</span>
          <span>Details</span>
        </ContentHeader>
      </div>
      <div className={Styles.expansionPanelWrapper}>
        <ExpansionPanel
          isOrdered
          items={[
            {
              primary: <div className={Styles.expansionPanel}><div><p>Download NIBL Mobile (SMS) Banking Application</p></div>
                <a href="1"><img src={downloadIcon} alt=""/></a></div>,
              secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</div>
            },
            {
              primary: <div className={Styles.expansionPanel}><div><p>Download NIBL Mobile (SMS) Banking Application</p></div>
                <a href="1"><img src={downloadIcon} alt=""/></a></div>,
              secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</div>
            }
          ]}
        />
      </div>
    </div>
    <BigTitleBlockSet/>
  </div>
);

export default MobileBanking;
