import React from 'react';
import LocationOn from '@material-ui/icons/LocationOn';

import Styles from './lockers.module.scss';

// Compontents
import BannerBlock from '../../../BannerBlock';
import LeftImageText from '../../../LeftImageText';
import FeaturesAndBenefits from '../../../FeaturesAndBenefits';
import Strip from '../../../Strip';
import LockerSize from './LockerSize';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import banner from '../../../../assets/img/mobtop-bg.jpg';
import dummyImg from '../../../../assets/img/nibl-dummy.jpg';

const Lockers = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      background={banner}
      title="Locker"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className={Styles.greyBack}>
      <div className="container">
        <div className={Styles.heading}>
          <h1>Locker</h1>
          <a href="1" className={Styles.location}>
            <LocationOn/>
            <span>See the Locker Location</span>
          </a>
        </div>
        <LeftImageText title="Details" image={dummyImg}>
          <p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
            natoque penatibus et magnis dis parturient montes, nascetur
            ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
            pretium quis, sem. Nulla consequat massa quis enim.
          </p>
          <p>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
          Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
          natoque penatibus et magnis dis parturient montes, nascetur
          ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
          pretium quis, sem. Nulla consequat massa quis enim.
          </p>
          <p>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
          Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
          natoque penatibus et magnis dis parturient montes, nascetur
          ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
          pretium quis, sem. Nulla consequat massa quis enim.
          </p>
        </LeftImageText>
        <FeaturesAndBenefits firstTableTitle="Requirements" secondTableTitle="Features"/>
      </div>
    </div>
    <div className={Styles.strip}>
      <Strip btnTitle="Download">
        <span>Download the Locker application form.</span>
      </Strip>
    </div>
    <LockerSize />
    <BigTitleBlockSet/>
  </div>
);

export default Lockers;
