import React, { Component } from 'react';
import Styles from './lockers.module.scss';

// Componets
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import MediumLocker from './MediumLocker';

class LockerSize extends Component {
  state = {
    items: [
      {
        name: 'Medium',
        key: 'medium'
      },
      {
        name: 'Small',
        key: 'small'
      },
      {
        name: 'Large',
        key: 'large'
      },
      {
        name: 'Extra Large',
        key: 'extra-large'
      },
      {
        name: 'Available Lockers',
        key: 'available-lockers'
      }
    ],
    activeItemKey: 'medium'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.lockerSizeWrapper}>
        <div className="container">
          <div className={Styles.heading}>
            <h1>Locker Sizes</h1>
          </div>
          <TabNav
            onClick={(key) => { this.setState({ activeItemKey: key }); }}
            activeItemKey={activeItemKey}
            items={items}
          />
          <TabContent>
            <MediumLocker isActive={activeItemKey === 'medium'}/>
            <MediumLocker isActive={activeItemKey === 'small'}/>
          </TabContent>
        </div>
      </div>
    );
  }
}

export default LockerSize;
