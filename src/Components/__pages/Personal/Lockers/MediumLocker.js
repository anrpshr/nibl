import React from 'react';
import Styles from './lockers.module.scss';

// Component
import ContentHeader from '../../../ContentHeader';
import TableBody from '../../../TableBody';

const MediumLocker = () => (
  <div className={Styles.tableWrapper}>
    <ContentHeader>
      <div className={Styles.tableHead}>
        <div className={Styles.topHeading}>
          <div className={Styles.dimension}>Dimension in inches</div>
          <div className={Styles.charges}>Charges in NPR</div>
          <div>Locker Type</div>
          <div className={Styles.hwd}>
            <span>Height</span>
            <span>Width</span>
            <span>Depth</span>
          </div>
          <div className={Styles.deposits}>Key Deposits</div>
          <div className={Styles.anualCharges}>
            <span>Annual Charges Outside Valley</span>
            <span>Annual Charges Inside Valley</span>
          </div>
        </div>
      </div>
    </ContentHeader>
    <div className={Styles.tableBodyWrapper}>
      <TableBody>
        <div className={Styles.topHeading}>
          <div>1F/21</div>
          <div className={Styles.hwd}>
            <span>10.63</span>
            <span>13.86</span>
            <span>19.37</span>
          </div>
          <div className={Styles.deposits}>20,000</div>
          <div className={Styles.anualCharges}>
            <span>5,000</span>
            <span>6,000</span>
          </div>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.topHeading}>
          <div>1F/21</div>
          <div className={Styles.hwd}>
            <span>10.63</span>
            <span>13.86</span>
            <span>19.37</span>
          </div>
          <div className={Styles.deposits}>20,000</div>
          <div className={Styles.anualCharges}>
            <span>5,000</span>
            <span>6,000</span>
          </div>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.topHeading}>
          <div>1F/21</div>
          <div className={Styles.hwd}>
            <span>10.63</span>
            <span>13.86</span>
            <span>19.37</span>
          </div>
          <div className={Styles.deposits}>20,000</div>
          <div className={Styles.anualCharges}>
            <span>5,000</span>
            <span>6,000</span>
          </div>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.topHeading}>
          <div>1F/21</div>
          <div className={Styles.hwd}>
            <span>10.63</span>
            <span>13.86</span>
            <span>19.37</span>
          </div>
          <div className={Styles.deposits}>20,000</div>
          <div className={Styles.anualCharges}>
            <span>5,000</span>
            <span>6,000</span>
          </div>
        </div>
      </TableBody>
    </div>
  </div>
);

export default MediumLocker;