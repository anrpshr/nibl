import React from 'react';
import Styles from './atmLocation.module.scss';

// Components
import LocationListing from '../../../LocationListing';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

const AtmLocation = () => (
  <div className={Styles.wrapper}>
    <div className={Styles.sidebar}>
      <LocationListing/>
    </div>
    <BigTitleBlockSet/>
  </div>
);

export default AtmLocation;
