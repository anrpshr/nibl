import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import sitemap from '../../../routing/siteMap';
import Payments from './Payments';
import Services from './Services';
import Accounts from './Accounts';
import MobileBanking from './MobileBanking';
import Lockers from './Lockers';
import BranchlessBanking from './BranchlessBanking';
import DailyRates from './RatesAndFees/DailyRates';
import InterestRates from './RatesAndFees/InterestRates';
import Downloads from './Downloads';
import AtmLocation from './ATMLocation';

const Personal = () => (
  <Switch>
    <Route exact path={sitemap.personal.payments} component={Payments}/>
    <Route exact path={sitemap.personal.services} component={Services}/>
    <Route exact path={sitemap.personal.mobileBanking} component={MobileBanking}/>
    <Route exact path={sitemap.personal.lockers} component={Lockers}/>
    <Route exact path={sitemap.personal.branchlessBanking} component={BranchlessBanking}/>
    <Route exact path={sitemap.personal.dailyRates} component={DailyRates}/>
    <Route exact path={sitemap.personal.interestRates} component={InterestRates}/>
    <Route exact path={sitemap.personal.downloads} component={Downloads}/>
    <Route exact path={sitemap.personal.atmLocation} component={AtmLocation}/>
    <Route path={sitemap.personal.accounts} component={Accounts}/>
    <Redirect exact to={sitemap.personal.payments} />
  </Switch>
);

export default Personal;
