import React, { Component } from 'react';
import moment from 'moment';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DownIcon from '@material-ui/icons/ExpandMore';

// Components
import BannerBlock from '../../../../BannerBlock';
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';

// Images
import banner from '../../../../../assets/img/mobtop-bg.jpg';
import flag from '../../../../../assets/img/usa.png';

import Styles from './dailyRates.module.scss';

class DailyRates extends Component {
  constructor() {
    super();
    this.state = {
      selectedDate: moment()
    };
    this.pickerDialogRef = React.createRef();
  }

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  handleDateChange = (date) => {
    this.setState({ selectedDate: date });
  };

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  render() {
    const { selectedDate } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title="Foreign Rates"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
          background={banner}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.top}>

              <p>Select the date to view the exchange rate of NIBL</p>
              <div className={Styles.datePicker}>
                <div className={Styles.fakeDateInput}>
                  <input
                    required
                    readOnly
                    type="text"
                    value={selectedDate && moment(selectedDate).format('YYYY')}
                    style={{ width: 75 }}
                    onClick={this.openPickerDialog}
                  />
                  <DownIcon color="primary" style={{ fontSize: 16 }}/>
                </div>
                <div className={Styles.fakeDateInput}>
                  <input
                    required
                    readOnly
                    type="text"
                    value={selectedDate && moment(selectedDate).format('MM')}
                    onClick={this.openPickerDialog}
                  />
                  <DownIcon color="primary" style={{ fontSize: 16 }}/>
                </div>
                <div className={Styles.fakeDateInput}>
                  <input
                    required
                    readOnly
                    type="text"
                    value={selectedDate && moment(selectedDate).format('DD')}
                    onClick={this.openPickerDialog}
                  />
                  <DownIcon color="primary" style={{ fontSize: 16 }}/>
                </div>
              </div>
              <div className="datePickerHide">
                <MuiPickersUtilsProvider utils={MomentUtils}>
                  <DatePicker
                    ref={this.pickerDialogRef}
                    margin="normal"
                    label="Date Of Birth"
                    value={selectedDate}
                    onChange={this.handleDateChange}
                    style={{
                      width: '100%'
                    }}
                  />
                </MuiPickersUtilsProvider>
              </div>
            </div>
            <ContentHeader>
              <div className={Styles.tableGrid}>
                <span>S.N</span>
                <span>Currency</span>
                <span className={Styles.buying}>Buying Cash</span>
                <span className={Styles.others}>Buying Others</span>

                <span className={Styles.selling}>Selling Rate</span>
              </div>
            </ContentHeader>
            <TableBody>
              <div className={Styles.tableGrid}>
                <span>1</span>
                <span className={Styles.flag}>
                  <img src={flag} alt=""/>
                  USD
                </span>
                <span className={Styles.buying}>109.80</span>
                <span className={Styles.others}>110.35</span>
                <span className={Styles.selling}>110.95</span>
              </div>
            </TableBody>
            <TableBody>
              <div className={Styles.tableGrid}>
                <span>1</span>
                <span className={Styles.flag}>
                  <img src={flag} alt=""/>
                  USD
                </span>
                <span className={Styles.buying}>109.80</span>
                <span className={Styles.others}>110.35</span>
                <span className={Styles.selling}>110.95</span>
              </div>
            </TableBody>
            <TableBody>
              <div className={Styles.tableGrid}>
                <span>1</span>
                <span className={Styles.flag}>
                  <img src={flag} alt=""/>
                  USD
                </span>
                <span className={Styles.buying}>109.80</span>
                <span className={Styles.others}>110.35</span>
                <span className={Styles.selling}>110.95</span>
              </div>
            </TableBody>
            <TableBody>
              <div className={Styles.tableGrid}>
                <span>1</span>
                <span className={Styles.flag}>
                  <img src={flag} alt=""/>
                  USD
                </span>
                <span className={Styles.buying}>109.80</span>
                <span className={Styles.others}>110.35</span>
                <span className={Styles.selling}>110.95</span>
              </div>
            </TableBody>
            <TableBody>
              <div className={Styles.tableGrid}>
                <span>1</span>
                <span className={Styles.flag}>
                  <img src={flag} alt=""/>
                  USD
                </span>
                <span className={Styles.buying}>109.80</span>
                <span className={Styles.others}>110.35</span>
                <span className={Styles.selling}>110.95</span>
              </div>
            </TableBody>
            <TableBody>
              <div className={Styles.tableGrid}>
                <span>1</span>
                <span className={Styles.flag}>
                  <img src={flag} alt=""/>
                  USD
                </span>
                <span className={Styles.buying}>109.80</span>
                <span className={Styles.others}>110.35</span>
                <span className={Styles.selling}>110.95</span>
              </div>
            </TableBody>
          </div>
        </div>
      </div>
    );
  }
}


export default DailyRates;
