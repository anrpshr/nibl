import React from 'react';
import Styles from './interestRates.module.scss';

// Components
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';

const DepositsTable = () => (
  <div className={Styles.depositsTable}>
    <div className="heading">
      <h1>Deposit Interest Rate</h1>
    </div>
    <ContentHeader>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>S.N</span>
        <span className={Styles.saving}>Saving Accounts</span>
        <span className={Styles.annum}>% per annum</span>
      </div>
    </ContentHeader>
    <TableBody>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>1</span>
        <span className={Styles.saving}>Afnai Bachat Khata</span>
        <span className={Styles.annum}>3.50</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>2</span>
        <span className={Styles.saving}>Afnai Bachat Khata</span>
        <span className={Styles.annum}>3.50</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>3</span>
        <span className={Styles.saving}>Afnai Bachat Khata</span>
        <span className={Styles.annum}>3.50</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>4</span>
        <span className={Styles.saving}>Afnai Bachat Khata</span>
        <span className={Styles.annum}>3.50</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>5</span>
        <span className={Styles.saving}>Afnai Bachat Khata</span>
        <span className={Styles.annum}>3.50</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridWrapper}>
        <span className={Styles.sn}>6</span>
        <span className={Styles.saving}>Afnai Bachat Khata</span>
        <span className={Styles.annum}>3.50</span>
      </div>
    </TableBody>

  </div>
);

export default DepositsTable;
