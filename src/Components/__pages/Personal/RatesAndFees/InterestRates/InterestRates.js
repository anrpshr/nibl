import React, {Component } from 'react';
import Styles from './interestRates.module.scss';

// Compotnents
import BannerBlock from '../../../../BannerBlock';
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';
import TabNav from '../../../../TabNav';
import TabContent from '../../../../TabContent';
import BigTitleBlockSet from '../../../../Layouts/Block/BigTitleBlockSet';

import DepositsTable from './DepositsTable';

// Iamges
import banner from '../../../../../assets/img/mobtop-bg.jpg';

class InterestRates extends Component {
  state = {
    items: [
      {
        name: 'Deposit Interest Rates',
        key: 'deposit-interest-rates'
      },
      {
        name: 'Fixed Deposit per annum',
        key: 'fixed-depost-per-annum'
      },
      {
        name: 'Lending-Rates',
        key: 'lending-rates'
      }
    ],
    activeItemKey: 'deposit-interest-rates'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title="Interest Rates"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
          background={banner}
        />
        <div className="greyWrapper">
          <div className="container">
            <div className="heading">
              <h1>Base Rates</h1>
            </div>
            <ContentHeader>
              <div className={Styles.gridWrapper}>
                <span className={Styles.sn}>S.N</span>
                <span className={Styles.saving}>Saving Accounts</span>
                <span className={Styles.annum}>% per annum</span>
              </div>
            </ContentHeader>
            <TableBody>
              <div className={Styles.gridWrapper}>
                <span className={Styles.sn}>1</span>
                <span className={Styles.saving}>Base Rate</span>
                <span className={Styles.annum}>8.54</span>
              </div>
            </TableBody>
            <TableBody>
              <div className={Styles.gridWrapper}>
                <span className={Styles.sn}>2</span>
                <span className={Styles.saving}>Interest Spread LCY (As per NRB Directives)</span>
                <span className={Styles.annum}>4.24</span>
              </div>
            </TableBody>
          </div>
        </div>
        <div className={Styles.bottomSection}>
          <div className="container">
            <div className={Styles.noteText}>
              <span>Effective from 28th April, 2019 (15 Baisakh, 2076)</span>
            </div>

            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <div className={Styles.tabContent}>
              <TabContent>
                <DepositsTable isActive={activeItemKey === 'deposit-interest-rates'}/>
                <DepositsTable isActive={activeItemKey === 'fixed-depost-per-annum'}/>
                <DepositsTable isActive={activeItemKey === 'lending-rates'}/>
              </TabContent>
            </div>
          </div>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default InterestRates;
