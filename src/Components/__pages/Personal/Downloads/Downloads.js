import React, { Component } from 'react';
import Styles from './downloads.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

import SavingAccount from './SavingAccount';

// Images
import banner from '../../../../assets/img/mobtop-bg.jpg';

class Downloads extends Component {
  state = {
    items: [
      {
        name: 'Saving Account',
        key: 'saving-account'
      },
      {
        name: 'Business Accounts',
        key: 'business-account'
      },
      {
        name: 'Card Form',
        key: 'card-form'
      },
      {
        name: 'eBanking Form',
        key: 'ebanking-form'
      },
      {
        name: 'Alert Form',
        key: 'alert-form'
      },
      {
        name: 'SMS Banking',
        key: 'sms-banking'
      },
      {
        name: 'Branchless Banking',
        key: 'branchless-banking'
      },
      {
        name: 'Other Forms',
        key: 'other-forms'
      }
    ],
    activeItemKey: 'saving-account'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title="Downloads"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
          background={banner}
        />
        <div className="greyWrapper">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <TabContent>
              <SavingAccount isActive={activeItemKey === 'saving-account'}/>
              <SavingAccount isActive={activeItemKey === 'business-account'}/>
            </TabContent>
          </div>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default Downloads;
