import React, { Component } from 'react';
import Styles from './styles.module.scss';

import BannerBlock from '../../../BannerBlock';
import Ebanking from './Ebanking';
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

import paymentBg from '../../../../assets/img/payment-bg.jpg';

class Payments extends Component {
  state = {
    items: [
      {
        name: 'eBanking',
        key: 'e-banking'
      },
      {
        name: 'Bill Payments',
        key: 'bill-payments'
      }
    ],
    activeItemKey: 'e-banking'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock background={paymentBg} title="Payments" description="Here goes description" />
        <div className={Styles.iconBlockWrapper}>
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            <div className={Styles.ebankingWrapper}>
              <TabContent>
                <Ebanking isActive={activeItemKey === 'e-banking'} />
                <Ebanking isActive={activeItemKey === 'bill-payments'} />
              </TabContent>
            </div>
          </div>
        </div>
        <BigTitleBlockSet />
      </div>
    );
  }
}

export default Payments;
