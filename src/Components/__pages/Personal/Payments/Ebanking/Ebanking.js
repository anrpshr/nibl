import React, { Component } from 'react';

import Styles from './styles.module.scss';

// Components
import TabNavWithArrows from '../../../../TabNavWithArrows';
import TabContent from '../../../../TabContent';
import MobileTopUp from './MobileTopUp';

// Images
import mobIcon from '../../../../../assets/svg/mobile-topup.svg';
import mobIconRed from '../../../../../assets/svg/mobile-topup-red.svg';
import schoolIcon from '../../../../../assets/svg/school.svg';
import schoolIconRed from '../../../../../assets/svg/school-red.svg';
import uniIcon from '../../../../../assets/svg/edu-loan.svg';
import uniIconRed from '../../../../../assets/svg/edu-loan-red.svg';
import insuranceIcon from '../../../../../assets/svg/surakshya-bachat-dark.svg';
import insuranceIconRed from '../../../../../assets/svg/surakshya-bachat-red.svg';

class Ebanking extends Component {
  state = {
    items: [
      {
        name: 'Mobile Topup',
        id: 'mobile-topup',
        iconUrl: mobIcon,
        secondIcon: mobIconRed
      },
      {
        name: 'School',
        id: 'school',
        iconUrl: schoolIcon,
        secondIcon: schoolIconRed
      },
      {
        name: 'University',
        id: 'university',
        iconUrl: uniIcon,
        secondIcon: uniIconRed
      },
      {
        name: 'Insurance',
        id: 'insurance',
        iconUrl: insuranceIcon,
        secondIcon: insuranceIconRed
      }
    ],
    activeItemId: 'mobile-topup'
  }

  render() {
    const { items, activeItemId } = this.state;
    return (
      <div className={Styles.wrapper}>
        <div className={Styles.tabNavWrapper}>
          <TabNavWithArrows
            items={items}
            activeItemId={activeItemId}
            onSelect={(id) => { this.setState({ activeItemId: id }); }}
          />
        </div>
        <div className={Styles.tabContentWrapper}>
          <TabContent>
            <MobileTopUp isActive={activeItemId === 'mobile-topup'}/>
            <MobileTopUp isActive={activeItemId === 'school'}/>
          </TabContent>
        </div>
      </div>
    );
  }
}

export default Ebanking;
