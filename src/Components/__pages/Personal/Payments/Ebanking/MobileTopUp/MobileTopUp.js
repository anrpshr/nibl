import React from 'react';
import Styles from './mobileTopUp.module.scss';

// Components
import ContentHeader from '../../../../../ContentHeader';
import TableBody from '../../../../../TableBody';

const MobileTopUp = () => (
  <div className={Styles.wrapper}>
    <div className={Styles.heading}>
      <h1>NTC Topup</h1>
    </div>
    <div className={Styles.contentHeaderWrapper}>
      <ContentHeader>
        <span>S.N</span>
        <span>Details</span>
        <span>Payments</span>
      </ContentHeader>
    </div>
    <div className={Styles.tableRowWrapper}>
      <TableBody>
        <span>1</span>
        <span>NTC Post Paid Mobile Bills</span>
        <span><a href="1">Pay bill</a></span>
      </TableBody>
      <TableBody>
        <span>2</span>
        <span>NTC Post Paid Mobile Bills</span>
        <span><a href="1">Pay bill</a></span>
      </TableBody>
      <TableBody>
        <span>3</span>
        <span>NTC Post Paid Mobile Bills</span>
        <span><a href="1">Pay bill</a></span>
      </TableBody>
      <TableBody>
        <span>4</span>
        <span>NTC Post Paid Mobile Bills</span>
        <span><a href="1">Pay bill</a></span>
      </TableBody>
      <TableBody>
        <span>5</span>
        <span>NTC Post Paid Mobile Bills</span>
        <span><a href="1">Pay bill</a></span>
      </TableBody>
    </div>
    <div className={Styles.heading}>
      <h1>Ncell Top Up</h1>
    </div>
    <div className={Styles.contentHeaderWrapper}>
      <ContentHeader>
        <span>S.N</span>
        <span>Details</span>
        <span>Payments</span>
      </ContentHeader>
    </div>
    <div className={Styles.tableRowWrapper}>
      <TableBody>
        <span>1</span>
        <span>Ncell Post Paid Mobile Bills</span>
        <span><a href="1">Pay bill</a></span>
      </TableBody>
    </div>
  </div>
);

export default MobileTopUp;
