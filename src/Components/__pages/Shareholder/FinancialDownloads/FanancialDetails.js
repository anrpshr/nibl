import React, { Component } from 'react';

import Styles from './financialDownload.module.scss';

import TabContent from '../../../TabContent';
import Timeline from '../../../Timeline';

import FinancialDownloadTbl from './FinancialDownloadTbl';
import FinancialTransaction from './FinancialTransaction';

class FinancialDetails extends Component {
  state = {
    items: [
      2012, 2013, 2014, 2015, 2016
    ],
    activeItemKey: 2012
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <div className="container">
          <div className={Styles.tabNavWrapper}>
            <Timeline
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              items={items}
              activeItemKey={activeItemKey}
            />
          </div>
          <div className={Styles.heading}>
            <h1>Financial Details</h1>
          </div>
          <TabContent>
            <FinancialDownloadTbl isActive={activeItemKey === 2012}/>
            <FinancialDownloadTbl isActive={activeItemKey === 2013}/>
          </TabContent>
        </div>
        <div className={Styles.whiteBg}>
          <div className="container">
            <FinancialTransaction/>
          </div>
        </div>
      </div>
    );
  }
}

export default FinancialDetails;
