import React from 'react';
import Styles from './reports.module.scss';

import ContentHeader from '../../../ContentHeader';
import TableBody from '../../../TableBody';

const ReportsTable = () => (
  <div className={Styles.wrapper}>
    <div className={Styles.contentHeaderWrapper}>
      <ContentHeader>
        <div className={Styles.tableHeadTitles}>
          <span>S.N</span>
          <span>Details</span>
          <span>Downloads</span>
        </div>
      </ContentHeader>
    </div>
    <TableBody>
      <div className={Styles.tableBodyWrapper}>
        <span>1</span>
        <span>Download the annual report for year 2017-2018</span>
        <span className={Styles.downloadLinks}>
          <a href="1">Nepali</a>
          <a href="2">English</a>
        </span>
      </div>
    </TableBody>
  </div>
);

export default ReportsTable;
