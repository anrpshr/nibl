import React, { Component } from 'react';

import Styles from './shareholder.module.scss';

// Compontents
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import ReportsMain from './Reports';
import FinancialDownloads from './FinancialDownloads';
import FinancialTransaction from './FinancialDownloads/FinancialTransaction';
import Strategy from './Strategy';
import Stock from './Stock';

// Images
import background from '../../../assets/img/mobtop-bg.jpg';

class Shareholder extends Component {
  state = {
    items: [
      {
        name: 'Reports',
        key: 'reports'
      },
      {
        name: 'Financial Downloads',
        key: 'financial-downloads'
      },
      {
        name: 'Strategy',
        key: 'strategy'
      },
      {
        name: 'Stock',
        key: 'stock'
      }
    ],
    activeItemKey: 'reports'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={background}
          title="Shareholder"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className={Styles.innerWrapper}>
          <div className={Styles.tabNav}>
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
          </div>
        </div>
        <div className={Styles.tabContentWrapper}>
          <TabContent>
            <ReportsMain isActive={activeItemKey === 'reports'}/>
            <FinancialDownloads isActive={activeItemKey === 'financial-downloads'}/>
            <Strategy isActive={activeItemKey === 'strategy'} title="Strategy"/>
            <Stock
              isActive={activeItemKey === 'stock'}
              title="Stock"
              date="On Sunday, March 31, 2019"
            />
          </TabContent>
        </div>
        <div className="container">
          <BigTitleBlockSet />
        </div>
      </div>
    );
  }
}

export default Shareholder;
