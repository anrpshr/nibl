import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import sitemap from '../../../routing/siteMap';
import BoardOfDirectors from './BoardOfDirectors';

const Team = () => (
  <Switch>
    <Route exact path={sitemap.team.boardOfDirectors} component={BoardOfDirectors}/>
    <Redirect exact to={sitemap.team.boardOfDirectors}/>
  </Switch>
);

export default Team;
