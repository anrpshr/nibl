import React, { Component } from 'react';
import Styles from './boardOfDirectors.module.scss';

// components
import BannerBlock from '../../../BannerBlock';
import LeftImageText from '../../../LeftImageText';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import bodBg from '../../../../assets/img/bod-bg.jpg';
import chairmanImg from '../../../../assets/img/chairman-img.jpg';
import bodOne from '../../../../assets/img/bod1.jpg';
import bodTwo from '../../../../assets/img/bod2.jpg';
import bodThree from '../../../../assets/img/bod3.jpg';
import bodFour from '../../../../assets/img/bod4.jpg';
import bodFive from '../../../../assets/img/bod5.jpg';

class BoardOfDirectors extends Component {
  state = {
    activeUserIndex: 0,
    bods: [
      {
        image: chairmanImg,
        name: 'Jon Doe',
        designation: 'Chairman',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
        facebook: 'fb.com',
        twitter: 'twitter.com',
        linkedin: 'linkedin.com'
      },
      {
        image: bodOne,
        name: 'Jane Dove',
        designation: 'Vice Chairman',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
        facebook: 'fb.com',
        twitter: 'twitter.com',
        linkedin: 'linkedin.com'
      },
      {
        image: bodFour,
        name: 'Joe clover',
        designation: 'Vice Chairman',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
        facebook: 'fb.com',
        twitter: 'twitter.com',
        linkedin: 'linkedin.com'
      },
      {
        image: bodThree,
        name: 'James Dean',
        designation: 'Vice Chairman',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
        facebook: 'fb.com',
        twitter: 'twitter.com',
        linkedin: 'linkedin.com'
      },
      {
        image: bodFour,
        name: 'Dove Johns',
        designation: 'Vice Chairman',
        description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
        facebook: 'fb.com',
        twitter: 'twitter.com',
        linkedin: 'linkedin.com'
      },
      {
        image: bodFive,
        name: 'Ken Thompson',
        designation: 'Vice Chairman',
        description: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p>',
        facebook: 'fb.com',
        twitter: 'twitter.com',
        linkedin: 'linkedin.com'
      }
    ]
  }

  handleActiveuser = (index) => {
    this.setState({
      activeUserIndex: index
    });
  }

  render() {
    const { bods, activeUserIndex } = this.state;
    const activeUser = bods[activeUserIndex];
    const bodList = bods.map((bod, index) => (
      <div role="button" tabIndex="-1" onClick={() => { this.handleActiveuser(index); }} className={Styles.listElement}>
        <img src={bod.image} alt=""/>
      </div>
    ));
    return (
      <div className={Styles.wrapper}>
        <BannerBlock background={bodBg}/>
        <div className={Styles.innerWrapper}>
          <div className="container">
            <div className={Styles.heading}>
              <h1>Board Of Directors</h1>
            </div>

            <div className={Styles.content}>
              <LeftImageText title={activeUser.name} image={activeUser.image}>
                <span>{activeUser.designation}</span>
                <p dangerouslySetInnerHTML={{ __html: activeUser.description }} />
                <div className={Styles.socialIcons}>
                  <a href={activeUser.facebook}><i className="fab fa-facebook-f" /></a>
                  <a href={activeUser.twitter}><i className="fab fa-twitter" /></a>
                  <a href={activeUser.linkedin}><i className="fab fa-linkedin" /></a>
                </div>
              </LeftImageText>
            </div>

            <div className={Styles.list}>
              {bodList}
            </div>
          </div>
        </div>
        <div className="container">
          <BigTitleBlockSet />
        </div>
      </div>
    );
  }
}

export default BoardOfDirectors;
