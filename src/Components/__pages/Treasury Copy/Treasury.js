import React, { Component } from 'react';
import Styles from './treasury.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import ButtonRedBorder from '../../ButtonRedBorder';
import LeftImageText from '../../LeftImageText';
import Card from '../../Layouts/Cards';
import ImageBlockWithOverlay from '../../Layouts/Block/ImageBlockWithOverlay';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';


// Images
import banner from '../../../assets/img/mobtop-bg.jpg';
import image from '../../../assets/img/domesticImg.jpg';
import Dummy from '../../../assets/img/dummy.png';
import MasterCard from '../../../assets/img/card.png';
import Bill from '../../../assets/img/bill.png';
import Service from '../../../assets/img/service.png';
import Dummygirl from '../../../assets/img/dummygirl.png';
import secondaryBanner from '../../../assets/img/about-bg.jpg';

class Treasury extends Component {
  state = {
    posts: [],
    loading: true
  };

  componentDidMount() {
    this.setState({
      loading: false,
      posts: [
        {
          isFeatured: false,
          title: 'Fund Management',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'phising',
          id: 1,
          image: Dummy,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Treasury Bills/ Government Bonds',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'safe-banking',
          id: 2,
          image: null,
          icon: MasterCard,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Inter- bank placements:',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'email-fraud',
          id: 3,
          image: null,
          icon: Bill,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Forward Contracts',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'atm-skimmers',
          id: 4,
          image: null,
          icon: Service,
          backgroundColor: 'blue'
        },
        {
          isFeatured: true,
          title: 'NRB Report',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'online-safety',
          id: 5,
          image: {
            url: Dummygirl,
            width: 393,
            height: 636
          },
          icon: null,
          backgroundColor: null
        }
      ]
    });
  }

  render() {
    const { loading, posts } = this.state;
    const nonFeaturedBlocks = posts.filter(post => !post.isFeatured);
    const featuredBlocks = posts.filter(post => post.isFeatured);
    const nonFeaturedBlockList = nonFeaturedBlocks.map((post) => {
      if (post.image) {
        return (
          <ImageBlockWithOverlay title={post.title} image={post.image}>
            <p>
              {post.excerpt}
              <a href="/security/security-inside">more</a>
            </p>
          </ImageBlockWithOverlay>
        );
      } return (
        <Card
          color={post.backgroundColor}
          title={post.title}
          image={post.icon}
          content={post.excerpt}
        />
      );
    });
    const featuredBlockList = featuredBlocks.map(post => (
      <ImageBlockWithOverlay title={post.title} image={post.image.url}>
        <p>
          {post.excerpt}
        </p>
      </ImageBlockWithOverlay>
    ));
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
          title="Foreign Exchange"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        >
          <ButtonRedBorder title="See more"/>
        </BannerBlock>

        <div className="greyWrapper">
          <div className="container">
            <div className="heading">
              <h1>Treasury</h1>
            </div>
            <LeftImageText
              title="Overview"
              image={image}
            >
              <p>
                Nepal Investment Bank operates with a full- fledged Treasury
                Department within the consideration of the foreign exchange
                rules and regulations under the guidance of Nepal Rastra Bank (NRB)
                – central bank of Nepal. Being an active member of Foreign Exchange
                Dealers Association of Nepal (FEDAN), we work closely with all other
                commercial banks in the country. The Treasury Department offers basic
                services such as foreign currencies exchange (Buy/ Sell) and money market
                operations as well as specific services such as foreign currency forward
                contracts.
              </p>
              <p>
                The treasury department located in Head Office,
                Durbar Marg is fully equipped with state of the art
                technology for day- to- day dealings. The department relies on
                the various internationally acknowledged softwares like Reuters
                and Newswire18 for real time information on the foreign currencies
                and news in the international market. We aspire to be the most active
                player in the financial market of the country.
              </p>
            </LeftImageText>

            {
              loading
                ? ''
                : (
                  <div className={Styles.featured}>
                    <div className="heading">
                      <h1>More Functions</h1>
                    </div>
                    <div className={Styles.bigBlock}>
                      <div className={Styles.bigBlockLeft}>
                        {nonFeaturedBlockList}
                      </div>
                      <div className={Styles.bigBlockRight}>
                        {featuredBlockList}
                      </div>
                    </div>
                  </div>
                )
            }
          </div>
        </div>

        <BannerBlock
          background={secondaryBanner}
          title="Treasury Research"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        >
          <ButtonRedBorder title="See more"/>
        </BannerBlock>

        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default Treasury;
