import React from 'react';
import PropTypes from 'prop-types';
import Styles from './treasuryInsidePage.module.scss';

// Components
import ContentCard from '../../../ContentCard';

const PageWithBasicContent = ({ title, content }) => (
  <div className={Styles.pageWithBasicContent}>
    <div className="greyWrapper">
      <div className="container">
        <div className="heading">
          <h1>{title}</h1>
        </div>
        <ContentCard>
          {content}
          <h5>Updating the Exchange Rate</h5>
          <p>
            The department updates the exchange rates daily
            and as per the decisions of FEDAN. The exchange
            rates are based on the movements of INR against USD.
          </p>
          <h5>Check Foreign Currency Position</h5>
          <p>
            The department maintains the foreign currency position
            as per the needs of the bank. Mostly, the currencies are
            squared off at day end to minimize the risks related to
            exchange rate fluctuations. Mostly spot transactions are
            carried out for sales/ purchase of the foreign currencies.
          </p>
        </ContentCard>
      </div>
    </div>
  </div>
);

PageWithBasicContent.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default PageWithBasicContent;
