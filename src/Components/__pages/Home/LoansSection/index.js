import React, { Component } from 'react';
import handleViewport from 'react-in-viewport';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import Styles from '../style.module.scss';

// Components
import MinimalBlock from '../../../Layouts/Block/MinimalBlock';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import homeLoanImg from '../../../../assets/svg/home-loan.svg';
import businessLoanImg from '../../../../assets/svg/business-loan.svg';
import autoLoanImg from '../../../../assets/svg/auto-loan.svg';

class Loans extends Component {
  state = {
    minimalBlocks: []
  };

  componentDidMount() {
    this.setState({
      minimalBlocks: [
        {
          isMinimalBlock: true,
          title: 'Home Loan',
          icon: homeLoanImg,
          excerpt: 'Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
        },
        {
          isMinimalBlock: true,
          title: 'Business Loan',
          icon: businessLoanImg,
          excerpt: 'Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
        },
        {
          isMinimalBlock: true,
          title: 'Auto Loan',
          icon: autoLoanImg,
          excerpt: 'Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
        }
      ]
    });
  }

  render() {
    const { title, enterCount } = this.props;
    const { minimalBlocks } = this.state;

    const animationClass = classnames(Styles.block, {
      'animated fadeIn': enterCount > 0
    });

    const blocks = minimalBlocks.filter(block => block.isMinimalBlock);
    let animationTime = 0.5;
    const singleBlock = blocks.map((block, index) => {
      animationTime += 0.2;
      return (
        <div key={`block-${index}`} className={animationClass} style={{ animationDelay: `${animationTime}s` }}>
          <MinimalBlock
            image={block.icon}
            title={block.title}
            content={block.excerpt}
          />
        </div>
      );
    });

    return (
      <div className={Styles.loanWrapper}>
        <div className="container">
          <h1>{title}</h1>
          <div className={Styles.minimalBlockFlex}>
            {singleBlock}
          </div>
        </div>
      </div>
    );
  }
}

Loans.propTypes = {
  title: PropTypes.string.isRequired,
  enterCount: PropTypes.string.isRequired
};

export default handleViewport(Loans);
