import React, { Component } from 'react';

import SlickSlider from '../../SlickSlider';
import slider1 from '../../../assets/img/sliders/afnai-bachat-khata.png';
import slider2 from '../../../assets/img/sliders/recurring-deposit.jpg';
import slider3 from '../../../assets/img/sliders/gold-silver-loan.png';
import slider4 from '../../../assets/img/sliders/education-loan.png';

import Styles from './style.module.scss';

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  fade: true
};

class SliderSection extends Component {
  state = {
    slider: []
  };

  componentDidMount() {
    this.setState({
      slider: [
        {
          image: {
            url: slider1,
            width: 1920,
            height: 455,
            caption: 'Afnai Bachat Khata'
          }
        },
        {
          image: {
            url: slider2,
            width: 1920,
            height: 455,
            caption: 'Recurring Deposit'
          }
        },
        {
          image: {
            url: slider3,
            width: 1920,
            height: 455,
            caption: 'Gold and Silver Loan'
          }
        },
        {
          image: {
            url: slider4,
            width: 1920,
            height: 455,
            caption: 'Education Loan'
          }
        }
      ]
    });
  }

  render() {
    const { slider } = this.state;
    const singleSlide = slider.map((slide, i) => (
      <React.Fragment key={`slider-slide-${i}`}>
        <img src={slide.image.url} alt=""/>
        <div className="big-container">
          <div className="slider-content">
            <p>
              {slide.image.caption}
            </p>
          </div>
        </div>
      </React.Fragment>
    ));
    return (
      <div className="hero-slider">
        <div className={Styles.slider}>
          <SlickSlider settings={settings}>
            {singleSlide}
          </SlickSlider>
        </div>
      </div>
    );
  }
}

export default SliderSection;
