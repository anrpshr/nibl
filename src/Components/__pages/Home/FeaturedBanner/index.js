import React from 'react';
import Styles from '../style.module.scss';

import banner from '../../../../assets/img/f_banner.jpg';

const FeaturedBanner = () => (
  <div className={Styles.featuredBanner}>
    <img src={banner} alt=""/>
  </div>
);

export default FeaturedBanner;
