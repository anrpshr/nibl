import React from 'react';
import cn from 'classnames';

import Styles from './styles.module.scss';

import ForexBlock from '../../../ForexBlock';
import StockBlock from '../../../StockBlock';
import EmiCaclBlock from '../../../EmiCalcBlock';

const ForexStockEmiSection = () => (
  <div className={Styles.wrapper}>
    <div className="container">
      <div className={Styles.flexBlock}>
        <div className="heading">
          <h1>Forex</h1>
        </div>
        <div className="heading">
          <h1>Stock</h1>
        </div>
        <div className="heading">
          <h1>EMI Calculator</h1>
        </div>
      </div>

      <div className={Styles.flexBlock}>
        <div className={Styles.block}>
          <ForexBlock/>
        </div>
        <div className={Styles.block}>
          <StockBlock/>
        </div>
        <div className={cn(Styles.block, Styles.noPadding)}>
          <EmiCaclBlock/>
        </div>
      </div>
    </div>
  </div>
);

export default ForexStockEmiSection;
