import React from 'react';

import SavingCalculator from '../../../SavingCalculator';

import styles from '../style.module.scss';

const Calculator = () => (
  <div className="container">
    <div className={styles.calculatorWrap}>
      <SavingCalculator/>
    </div>
  </div>
);

export default Calculator;
