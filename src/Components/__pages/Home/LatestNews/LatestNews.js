import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Link } from 'react-router-dom';
import { Spin } from 'antd';

import { getV2 } from '../../../../services/generalApi.services';
import { API } from '../../../../constants';
import Styles from '../style.module.scss';

class LatestNews extends Component {
  state = {
    news: [],
    loading: true
  };

  limit = 3;

  componentDidMount() {
    getV2(generatePath(API.endPoints.NEWS_LATEST, { limit: this.limit }))
      .then((response) => {
        this.setState({
          news: response.pageContent.map(e => ({
            id: e.contentId,
            title: e.newsTitle,
            excerpt: e.newsDescription,
            content: e.newsContent,
            image: e.newsBanner
          })),
          loading: false
        });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Latest News', e);
      });
  }

  render() {
    const { news, loading } = this.state;
    const newsList = news.map(item => (
      <div className={Styles.newsSingle}>
        <img src={item.image} alt=""/>
        <div className={Styles.newsContent}>
          <div className={Styles.heading}>{item.title}</div>
          <p>
            {item.excerpt}
          </p>
          <div className={Styles.newsLink}>
            <Link to={`/news-content/${item.id}`}>Read More</Link>
          </div>
        </div>
      </div>
    ));
    return (
      <div className={Styles.newsWrapper}>
        <div className="container">
          <div className="heading">
            <h1>Latest News</h1>
          </div>
          <div className={Styles.newsGrid}>
            {
              loading
                ? (
                  <div className="page-loader __transparent __medium">
                    <Spin/>
                  </div>
                )
                : (
                  <>
                    {
                      news.length > 0
                        ? newsList
                        : (
                          <div className="page-loader __transparent __medium">
                            <span>No Data Available</span>
                          </div>
                        )
                    }
                  </>
                )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default LatestNews;
