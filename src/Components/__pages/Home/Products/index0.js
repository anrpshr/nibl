import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styles from '../style.module.scss';
// import { injectIntl, intlShape } from 'react-intl';

// Components
import Card from '../../../Layouts/Cards/index';
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';

// Images
import dummyKid from '../../../../assets/img/kid.png';
import ketaKeti from '../../../../assets/img/ketaketi.png';
import dummyImage from '../../../../assets/img/dummyimg.png';
import ezee from '../../../../assets/img/ezee.png';
import shield from '../../../../assets/img/safety.png';

class Accounts extends Component {
  state = {
    posts: [],
    loading: true
  };

  componentDidMount() {
    this.setState({
      loading: false,
      posts: [
        {
          isFeatured: true,
          title: 'E-Zee Saving Account',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'e-zee-saving-account',
          id: 1,
          image: {
            url: dummyKid,
            width: 393,
            height: 636
          },
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Keta Keti Bachat Khata',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'keta-keti-bachat-khata',
          id: 2,
          image: null,
          icon: ketaKeti,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Afnai Bachat Khata',
          excerpt: null,
          slug: 'afnai-bachat-khata',
          id: 3,
          image: dummyImage,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'E-Zee Student Account',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'ezee-student-account',
          id: 4,
          image: null,
          icon: ezee,
          backgroundColor: 'blue'
        },
        {
          isFeatured: false,
          title: 'Surakshya Bachat Khata',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'surakshya-bachat-khata',
          id: 5,
          image: null,
          icon: shield,
          backgroundColor: 'red'
        }
      ]
    });
  }

  render() {
    const { title } = this.props;
    const { loading, posts } = this.state;
    const featuredBlock = posts.filter(post => post.isFeatured);
    const nonFeaturedBlocks = posts.filter(post => !post.isFeatured);
    const featuredBlockList = featuredBlock.map(post => (
      <ImageBlockWithOverlay key={`featured-${post.id}`} title={post.title} image={post.image.url} >
        <p>
          {post.excerpt}
        </p>
      </ImageBlockWithOverlay>
    ));

    const nonFeaturedBlockList = nonFeaturedBlocks.map((post) => {
      if (!post.image) {
        return (
          <Card
            color={post.backgroundColor}
            title={post.title}
            image={post.icon}
            content={post.excerpt}
          />
        );
      } return <ImageBlockWithOverlay title={post.title} image={post.image} />;
    });

    return (
      <div className="container">
        {
          loading
            ? ''
            : (
              <div className={Styles.accountsWrapper}>
                <h1>{title}</h1>
                <div className={Styles.fullBlock}>
                  <div className={Styles.halfBlock}>
                    {featuredBlockList}
                  </div>
                  <div className={Styles.halfBlock}>
                    <div className={Styles.halfBlockFlexed}>
                      <div className={Styles.cardSpace}>
                        {nonFeaturedBlockList}
                      </div>
                      {/* <ImageBlockWithOverlay title="Afnai Bachat Khata" image={dummyImage} /> */}
                    </div>
                    {/* <div className={Styles.halfBlockFlexed}>
                      <div className={Styles.cardSpace}>
                        <Card
                          color="blue"
                          title="E-Zee Student Account"
                          image={ezee}
                          content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                          Aenean commodo ligula eget dolor. Aenean massa Cum sociis
                          natoque penatibus et magnis dis parturient montes."
                        />
                      </div>
                      <Card
                        color="red"
                        title="Surakshya Bachat khata"
                        image={shield}
                        content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                        Aenean commodo ligula eget dolor. Aenean massa Cum sociis
                        natoque penatibus et magnis dis parturient montes."
                      />
                    </div> */}
                  </div>
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

Accounts.propTypes = {
  title: PropTypes.string.isRequired
};

export default Accounts;
