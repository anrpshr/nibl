import React, { Component } from 'react';
import handleViewport from 'react-in-viewport';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { generatePath } from 'react-router';
import { Link } from 'react-router-dom';

// Components
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
import Loading from '../../../Loading';
import { getV2 } from '../../../../services/generalApi.services';
import { API } from '../../../../constants';
import Transformer from './transformer';

import Styles from '../style.module.scss';

class Products extends Component {
  state = {
    posts: [],
    loading: true
  };

  limit = 6;

  componentDidMount() {
    getV2(generatePath(API.endPoints.PRODUCT_LIST, { isFeatured: true, limit: this.limit }))
      .then((products) => {
        this.setState({ loading: false, posts: products.map(product => new Transformer(product)) });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Featured Products', e);
      });
  }

  render() {
    const { title, enterCount } = this.props;
    const { loading, posts } = this.state;
    const animatedClass = classnames({ 'animated fadeIn': enterCount > 0 });

    let animationTime = 0.5;
    const featuredBlockList = posts.map((post, i) => {
      animationTime += 0.2;
      return (
        <Link key={`product-card-${i}`} to={post.link}>
          <ImageBlockWithOverlay animatedClass={animatedClass} animationDelay={`${animationTime}s`} title={post.title} image={post.image}>
            <div dangerouslySetInnerHTML={{ __html: post.excerpt }}/>
          </ImageBlockWithOverlay>
        </Link>
      );
    });

    return (
      <div className="white-gradient">
        <div className={classnames(Styles.productWrapper, 'product-wrapper')}>
          <div className="container">
            {
              !title && (
                <div className="heading">
                  <h1>Products</h1>
                </div>
              )
            }
            {
              loading
                ? (
                  <Loading color="red"/>
                )
                : (
                  <div className={classnames(Styles.accountsWrapper, 'accounts-wrapper')}>
                    <div className={Styles.fullBlock}>
                      <div className={Styles.halfBlockFlexed}>
                        {featuredBlockList}
                      </div>
                    </div>
                  </div>
                )
            }
          </div>
        </div>
      </div>
    );
  }
}

Products.propTypes = {
  enterCount: PropTypes.number.isRequired
};

export default handleViewport(Products, { rootMargin: '-10px' });
