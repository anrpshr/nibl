import { getRoute } from '../../../../utils/link.utils';

export default class Transformer {
  constructor(ProductListAPIObject) {
    this.title = ProductListAPIObject.pageDetails.pageTitle;
    this.image = ProductListAPIObject.overviewContent.productImage;
    this.excerpt = ProductListAPIObject.overviewContent.productOverview;
    this.link = getRoute(ProductListAPIObject.pageDetails.pageId);
  }
}
