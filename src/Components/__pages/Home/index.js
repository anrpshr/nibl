import React from 'react';
import { injectIntl, intlShape } from 'react-intl';

import SliderSection from './SliderSection';
import Featured from './FeaturedSection/index';
import ForexStockEmiSection from './ForexStockEmiSection';
import FeaturedBanner from './FeaturedBanner';
import Products from './Products/index';
import LatestNews from './LatestNews';

const Home = ({ intl }) => (
  <>
    <SliderSection />
    <div className="white-gradient">
      <Featured title={intl.messages['ui.sections.featuredServices.title']}/>
      <ForexStockEmiSection/>
    </div>
    <FeaturedBanner/>
    <Products/>
    <LatestNews/>
  </>
);

Home.propTypes = {
  intl: intlShape.isRequired
};

export default injectIntl(Home);
