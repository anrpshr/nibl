import React from 'react';
import PropTypes from 'prop-types';

import Styles from './style.module.scss';

const BlockSection = ({ title, children }) => (
  <div className="wrapper">
    <h4>{title}</h4>
    <div className={Styles.innerBlock}>
      {children}
    </div>
  </div>
);

BlockSection.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default BlockSection;
