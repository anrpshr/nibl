import React, { Component } from 'react';
import handleViewport from 'react-in-viewport';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import Styles from '../style.module.scss';

// Components
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
// import Card from '../../../Layouts/Cards/index';

// Images
// import Dummy from '../../../../assets/img/dummyimg.png';
// import MasterCard from '../../../../assets/img/card.png';
// import Bill from '../../../../assets/img/bill.png';
// import Service from '../../../../assets/img/service.png';
// import Dummygirl from '../../../../assets/img/dummygirl.png';
import ImgLocker from '../../../../assets/img/featured/locker.jpg';
import ImgKtaKti from '../../../../assets/img/featured/ktakti.jpg';
import ImgGreen from '../../../../assets/img/featured/green.jpg';
import ImgRD from '../../../../assets/img/featured/recurring.jpg';
import ImgCards from '../../../../assets/img/featured/cards.jpg';
import ImgUtility from '../../../../assets/img/featured/utility.jpg';

class Featured extends Component {
  state = {
    posts: [],
    loading: true
  };

  componentDidMount() {
    this.setState({
      loading: false,
      posts: [
        {
          isFeatured: false,
          title: 'Locker',
          excerpt: 'NIBL provides its customers a safety locker for the safe keeping of their valuables at an affordable rate.',
          slug: 'locker',
          id: 1,
          image: ImgLocker
        },
        {
          isFeatured: false,
          title: 'Keta Keti Bachat Khata',
          excerpt: 'Keta-Keti Bachat Khata encourages your children to save at an early age',
          slug: 'keta-keti',
          id: 1,
          image: ImgKtaKti,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Green Double Fixed Deposit',
          excerpt: 'Long term saving for long term financial security',
          slug: 'green',
          id: 1,
          image: ImgGreen,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Recurring Fixed Deposit',
          excerpt: 'Encouraging better savings with NIBL!',
          slug: 'rd',
          id: 1,
          image: ImgRD,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Cards',
          excerpt: '',
          slug: 'cards',
          id: 1,
          image: ImgCards,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Utility Payments',
          excerpt: '',
          slug: 'utut',
          id: 1,
          image: ImgUtility,
          icon: null,
          backgroundColor: null
        }
        // {
        //   isFeatured: false,
        //   title: 'MasterCard',
        //   excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo',
        //   slug: 'mastercard',
        //   id: 2,
        //   image: null,
        //   icon: MasterCard,
        //   backgroundColor: 'red'
        // }
      ]
    });
  }

  render() {
    const { enterCount } = this.props;
    const { loading, posts } = this.state;
    const animatedClass = classnames({ 'animated fadeIn': enterCount > 0 });
    const nonFeaturedBlocks = posts.filter(post => !post.isFeatured);
    let animationTime = 0.5;
    // const nonFeaturedBlockList = nonFeaturedBlocks.map((post, index) => {
    //   animationTime += 0.2;
    //   if (post.image) {
    //     return (
    //       <ImageBlockWithOverlay animatedClass={animatedClass} animationDelay={`${animationTime}s`} key={`nonFeaturedImage-${index}`} title={post.title} image={post.image}>
    //         <p>
    //           {post.excerpt}
    //           <br/>
    //         </p>
    //       </ImageBlockWithOverlay>
    //     );
    //   } return (
    //     <Card
    //       key={`card-${index}`}
    //       color={post.backgroundColor}
    //       title={post.title}
    //       image={post.icon}
    //       content={post.excerpt}
    //       animatedClass={animatedClass}
    //       animationDelay={`${animationTime}s`}
    //     />
    //   );
    // });

    const nonFeaturedBlockList = nonFeaturedBlocks.map((post, index) => {
      animationTime += 0.2;
      return (
        <ImageBlockWithOverlay animatedClass={animatedClass} animationDelay={`${animationTime}s`} key={`nonFeaturedImage-${index}`} title={post.title} image={post.image}>
          <p>
            {post.excerpt}
            <br/>
          </p>
        </ImageBlockWithOverlay>
      );
    });

    return (
      <div className="container">
        {
          loading
            ? ''
            : (
              <div className={Styles.featured}>
                <div className="heading">
                  <h1>Featured Services</h1>
                </div>
                <div className={Styles.bigBlock}>
                  <div className={Styles.bigBlockLeft}>
                    {nonFeaturedBlockList}
                  </div>
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

Featured.propTypes = {
  enterCount: PropTypes.number.isRequired
};

export default handleViewport(Featured, { rootMargin: '-10px' });
