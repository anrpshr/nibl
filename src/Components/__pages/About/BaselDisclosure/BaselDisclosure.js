import React, { Component } from 'react';
import Styles from './baselDisclosure.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import Timeline from '../../../Timeline';
import TabContent from '../../../TabContent';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

import FinancialDetails from './FinancialDetails';

// Images
import banner from '../../../../assets/img/mobtop-bg.jpg';

class BaselDisclosure extends Component {
  state = {
    items: [
      2012, 2013, 2014, 2015, 2016
    ],
    activeItemKey: 2012
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title=""
          description=""
          background={banner}
        />
        <div className="white-gradient">
          <div className="container">
            <Timeline
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              items={items}
              activeItemKey={activeItemKey}
            />
            <TabContent>
              <FinancialDetails isActive={activeItemKey === 2012}/>
              <FinancialDetails isActive={activeItemKey === 2013}/>
            </TabContent>
          </div>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default BaselDisclosure;
