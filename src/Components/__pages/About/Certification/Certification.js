import React from 'react';
import Styles from './certification.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import ContentHeader from '../../../ContentHeader';
import ExpansionPanel from '../../../ExpansionPanel';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import banner from '../../../../assets/img/mobtop-bg.jpg';
import downloadIcon from '../../../../assets/img/download.png';

const Certification = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      title="certification"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
      background={banner}
    />
    <div className="greyWrapper">
      <div className="container">
        <div className="heading">
          <h1>Certification</h1>
        </div>
        <ContentHeader>
          <div className={Styles.contentHeader}>
            <span className={Styles.sn}>S.N</span>
            <span className={Styles.details}>Details</span>
          </div>
        </ContentHeader>
        <ExpansionPanel
          isOrdered
          items={[
            {
              primary:
              (
                <div className={Styles.primaryPanel}>
                  <div>
                    <p>
                      Download NIBL Mobile (SMS) Banking Application
                    </p>
                  </div>
                  <a href="1"><img src={downloadIcon} alt=""/></a>
                </div>
              ),
              secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>    
            }
          ]}
        />
      </div>
    </div>
    <BigTitleBlockSet/>
  </div>
);

export default Certification;
