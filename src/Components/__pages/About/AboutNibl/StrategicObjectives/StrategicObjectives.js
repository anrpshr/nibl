import React from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Styles from './strategicobjectives.module.scss';

import strategicBg from '../../../../../assets/img/strategic-obj-bg.jpg';

const StrategicObjectives = () => (
  <div className={Styles.wrapper} style={{ background: `url(${strategicBg})` }}>
    <div className="container">
      <div className={Styles.outer}>
        <div className={Styles.contentWrapper}>
          <div className={Styles.heading}>
            <h1>Strategic Objectives</h1>
          </div>
          <div className={Styles.contentList}>
            <ul>
              <li>
                <ChevronRight color="primary"/>
                To develop a customer oriented service culture with special emphasis on
                customer care and convenience
              </li>

              <li>
                <ChevronRight color="primary"/>
                To increase our market share by following a disciplined growth strategy
              </li>

              <li>
                <ChevronRight color="primary"/>
                To leverage our technology platform and pen scalable systems to achieve
                cost-effective operations, efficient MIS, improved delivery capability
                and high service standards
              </li>

              <li>
                <ChevronRight color="primary"/>
                To develop innovative products and services that attracts our targeted
                customers and market segments
              </li>

              <li>
                <ChevronRight color="primary"/>
                To continue to develop products and services that reduce our cost of funds
              </li>

              <li>
                <ChevronRight color="primary"/>
                To maintain a high quality assets portfolio to achieve strong and sustainable
                returns and to continuously build shareholders’ value
              </li>

              <li>
                <ChevronRight color="primary"/>
                To explore new avenues for growth and profitability
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default StrategicObjectives;
