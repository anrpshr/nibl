import React from 'react';
import PropTypes from 'prop-types';
import Styles from './valuesandethics.module.scss';

// Components
import PlainBlock from '../../../../PlainBLock';

// Images
import billIcon from '../../../../../assets/img/bill.png';

const ValuesAndEthics = ({ title, excerpt }) => (
  <div className={Styles.wrapper}>
    <div className={Styles.headings}>
      <h1>{title}</h1>
      <p>
        {excerpt}
      </p>
    </div>
    <div className={Styles.innerContents}>
      <PlainBlock
        icon={billIcon}
        title="Customer Focus"
        description="At NIBL, our prime focus is to perfect our customer service. Customers are our first priority and driving force. We wish to gain customer confidence and be their trusted partner."
      />
      <PlainBlock
        icon={billIcon}
        title="Quality"
        description="We believe a quality service experience is a paramount to our customers and we are strongly committed in fulfilling this ideal."
      />
      <PlainBlock
        icon={billIcon}
        title="Honesty and Integrity"
        description="We ensure the highest level of integrity to our customers, creating an ongoing relationship of trust and confidence. We treat our customers with honesty, fairness and respect."
      />
      <PlainBlock
        icon={billIcon}
        title="Belief in our people"
        description="We recognize that employees are our most valuable asset and our competitive strength. We respect the worth and dignity of individual employees who devote their careers for the progress of the Bank."
      />
      <PlainBlock
        icon={billIcon}
        title="Teamwork"
        description="We are a firm believer in team work and feel that loyal and motivated teams can produce extraordinary results. We are drived by a performance culture where recognition and rewards are based on individual merit and demonstrated track record."
      />
      <PlainBlock
        icon={billIcon}
        title="Good Corporate Governance"
        description=" Effective Corporate Governance procedures are essential to achieve and maintain public trust and confidence in any company, more so in a banking company. At NIBL, we are committed in following best practices resulting in good corporate governance."
      />
      <PlainBlock
        icon={billIcon}
        title="Corporate Social Responsibility"
        description="As a responsible corporate citizen, we consider it important to act in a responsible manner towards the environment and society. Our commitment has always been to behave ethically and contribute towards the improvement of quality of life of our people, the community and greatly the society, of which we are an integral part."
      />
    </div>
  </div>
);

ValuesAndEthics.propTypes = {
  title: PropTypes.string.isRequired,
  excerpt: PropTypes.string.isRequired
};

export default ValuesAndEthics;
