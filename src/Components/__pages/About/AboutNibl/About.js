import React, { Component } from 'react';
import Styles from './aboutnibl.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import TabNav from '../../../TabNav';
import AboutContent from './AboutContent';
import ValuesAndEthics from './ValuesAndEthics';
import StrategicObjectives from './StrategicObjectives';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import banner from '../../../../assets/img/about-bg.jpg';

class AboutNibl extends Component {
  state = {
    items: [
      {
        name: 'About Nibl',
        key: 'about-nibl'
      },
      {
        name: 'Values and Ethics',
        key: 'values-and-ethics'
      },
      {
        name: 'Strategic Objectives',
        key: 'strategic-objectives'
      }
    ],
    activeItemKey: 'about-nibl'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock background={banner} title="Our Vision is to be the most preferred provider of Financial Services in Nepal" />
        <div className={Styles.greyBack}>
          <div className="container">
            <div className={Styles.tabNavWrapper}>
              <TabNav
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            {/* <div className={Styles.tabContentWrapper}>
              <TabContent>
                <AboutContent isActive={activeItemKey === 'about-nibl'} />
                <ValuesAndEthics
                  isActive={activeItemKey === 'values-and-ethics'}
                  title="Values and Ethics"
                  excerpt="Our core values tell us, our customers and the communities we serve, who we really are; what we are about; and the principles by which we pledge to conduct business. In essence, we believe that success can only be achieved by living our core values and principles: "
                />
                <StrategicObjectives isActive={activeItemKey === 'strategic-objectives'} />
              </TabContent>
            </div> */}
            <AboutContent />
          </div>
        </div>
        {/* Grey wrapper div ends */}

        <div className={Styles.valuesEthicWrapper}>
          <div className="container">
            <ValuesAndEthics
              isActive={activeItemKey === 'values-and-ethics'}
              title="Values and Ethics"
              excerpt="Our core values tell us, our customers and the communities we serve, who we really are; what we are about; and the principles by which we pledge to conduct business. In essence, we believe that success can only be achieved by living our core values and principles: "
            />
          </div>
        </div>

        <StrategicObjectives />
        <BigTitleBlockSet />
      </div>
    );
  }
}

export default AboutNibl;
