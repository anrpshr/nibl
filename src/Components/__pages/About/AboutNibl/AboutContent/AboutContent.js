import React from 'react';
import Styles from './aboutcontent.module.scss';


// Components
import LeftImageText from '../../../../LeftImageText';
import LeftTextImage from '../../../../LeftTextImage';

// Images
import niblImg from '../../../../../assets/img/nibl-branch.jpg';
import missionImg from '../../../../../assets/img/mission.jpg';

const AboutContent = () => (
  <div className={Styles.wrapper}>
    <h1>About NIBL</h1>
    <LeftImageText title="Overview" image={niblImg}>
      <p>
        Nepal Investment Bank Ltd. (NIBL), previously Nepal Indosuez Bank Ltd.,
        was established in 1986 as a joint venture between Nepalese and French
        partners. The French partner (holding 50% of the capital of NIBL) was
        Credit Agricole Indosuez, a subsidiary of one of the largest banking
        group in the world.
        <br/><br/>
        Later, in 2002 a group of Nepalese companies comprising of bankers,
        professionals, industrialists and businessmen acquired the 50% shareholding
        of Credit Agricole Indosuez in Nepal Indosuez Bank Ltd., and accordingly
        the name of the Bank also changed to Nepal Investment Bank Ltd.
        <br/><br/>
        At present the Bank's shareholding pattern is as follows:
        <span>Promoters - 69%</span>
        <span>General Public - 31%</span>
        <br/>
        We believe that NIBL, which is managed by a team of experienced bankers
        and professionals having proven track record, can offer you what you're looking for.
      </p>
    </LeftImageText>

    <LeftTextImage title="Mission" image={missionImg}>
      <p>
        To be the leading Nepali bank, delivering world class service through
        the blending of state-of-the-art technology and visionary management
        in partnership with competent and committed staff, to achieve sound
        financial health with sustainable value addition to all our stakeholders.
        We are committed to do this mission while ensuring the highest levels of
        ethical standards, professional integrity, corporate governance and
        regulatory compliance.
      </p>
    </LeftTextImage>
  </div>
);

export default AboutContent;
