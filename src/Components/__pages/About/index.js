import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import sitemap from '../../../routing/siteMap';
import AboutNibl from './AboutNibl';
import Downloads from './Downloads';
import Certification from './Certification';
import BaselDisclosure from './BaselDisclosure';

const About = () => (
  <Switch>
    <Route exact path={sitemap.about.aboutnibl} component={AboutNibl}/>
    <Route exact path={sitemap.about.downloads} component={Downloads}/>
    <Route exact path={sitemap.about.certification} component={Certification}/>
    <Route exact path={sitemap.about.baselDisclosure} component={BaselDisclosure}/>
    <Redirect exact to={sitemap.about}/>
  </Switch>
);

export default About;
