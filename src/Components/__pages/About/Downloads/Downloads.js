import React, { Component } from 'react';

// Components
import BannerBlock from '../../../BannerBlock';
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

import DownloadTable from './DownloadTable';

import Styles from './downloads.module.scss';

// Images
import background from '../../../../assets/img/compliance-bg.jpg';

class Downloads extends Component {
  state = {
    items: [
      {
        name: 'Saving Account',
        key: 'saving-account'
      },
      {
        name: 'Business Account',
        key: 'business-account'
      },
      {
        name: 'Card-Form',
        key: 'card-form'
      },
      {
        name: 'eBanking-Form',
        key: 'eBanking-form'
      },
      {
        name: 'Alert-Form',
        key: 'Alert-form'
      },
      {
        name: 'SMS-Banking',
        key: 'sms-banking'
      },
      {
        name: 'Branchless-Banking',
        key: 'card-banking'
      }
    ],
    activeItemKey: 'saving-account'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={background}
          title="Downloads"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="greyWrapper">
          <div className="container">
            <div className={Styles.tabNavWrapper}>
              <TabNav
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            <TabContent>
              <DownloadTable isActive={activeItemKey === 'saving-account'}/>
              <DownloadTable isActive={activeItemKey === 'business-account'}/>
            </TabContent>
          </div>
        </div>
        <div className="bigTitleBlockSetWrapper">
          <BigTitleBlockSet/>
        </div>
      </div>
    );
  }
}

export default Downloads;
