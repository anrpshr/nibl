import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import sitemap from '../../../routing/siteMap';
import Aml from './AML';

const Compliance = () => (
  <Switch>
    <Route exact path={sitemap.compliance.aml} component={Aml}/>
    <Redirect exact to={sitemap.compliance.aml}/>
  </Switch>
);

export default Compliance;
