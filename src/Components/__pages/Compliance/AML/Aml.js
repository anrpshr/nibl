import React from 'react';
import Styles from './aml.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import LeftImageText from '../../../LeftImageText';
import LeftTextImage from '../../../LeftTextImage';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import complianceBg from '../../../../assets/img/compliance-bg.jpg';
import dummyImg from '../../../../assets/img/compliance1.jpg';
import dummyKyc from '../../../../assets/img/compliance2.jpg';

const Aml = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      title="Compliance"
      background={complianceBg}
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className={Styles.contentWrapper}>
      <div className="container">
        <div className={Styles.heading}>
          <h1>Compliance</h1>
        </div>
        <LeftImageText title="AML Compliance" image={dummyImg}>
          <p>
            The Bank fully complies with the provisions of Nepalese Money Laundering
            Prevention Act, 2008 and regulations made thereunder and the Guidelines
            of our central bank viz. Nepal Rastra Bank (NRB) regarding anti-money
            laundering. In addition to above the Bank has its own policies and
            procedures to combat to prevent laundering of criminally earned money
            using its services. The Bank's Management strictly ensures the compliance
            with all statutory and regulatory requirements, including designating
            Focal Officer for this specific purpose and conducting training for
            staff at all levels.
            <br/><br/>
            Bank's compliance with Anti-Money-Laundering requirements and procedures
            is monitored by the NRB and by Bank's internal and external auditors.
            The Bank's customers must fill up the Know Your Customer (KYC) form to meet all the regulatory requirements.
            <br/><br/>
            Additional information regarding Anti-Money Laundering (AML)
            and Combating the Financing of Terrorism (CFT) from Nepal Rastra Bank(NRB) is available in NRB's Website.
          </p>
        </LeftImageText>

        <LeftTextImage title="KYC Compliance" image={dummyKyc}>
        To be the leading Nepali bank, delivering world class service through the
        blending of state-of-the-art technology and visionary management in
        partnership with competent and committed staff, to achieve sound
        financial health with sustainable value addition to all our stakeholders.
        We are committed to do this mission while ensuring the highest levels of
        ethical standards, professional integrity, corporate governance and
        regulatory compliance.
        </LeftTextImage>
      </div>
    </div>
    <div className={Styles.bigTitleBlockSetWrapper}>
      <div className="container">
        <BigTitleBlockSet />
      </div>
    </div>
  </div>
);

export default Aml;
