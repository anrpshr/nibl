import React from 'react';
import Styles from './styles.module.scss';

import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import background from '../../../assets/img/compliance-bg.jpg';
import euroFlag from '../../../assets/img/euro-flag.png';
import gbpFlag from '../../../assets/img/gbp-flag.png';
import canadaFlag from '../../../assets/img/canda-flag.png';
import chfFlag from '../../../assets/img/chf-flag.png';


const CorrespondentBank = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      background={background}
      title="Correspondent Bank"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className={Styles.innerWrapper}>
      <div className="container">
        <div className="heading">
          <h1>Correspondent Bank</h1>
        </div>
        <ContentHeader>
          <div className={Styles.tableHeadTitles}>
            <span>SN</span>
            <span>Currency</span>
            <span>Bank</span>
            <span>Swift Address</span>
          </div>
        </ContentHeader>
        <TableBody>
          <div className={Styles.tableBodyElements}>
            <span>1</span>
            <span>
              Flag
              <img className={Styles.flag} src={euroFlag} alt=""/>
            </span>
            <span>Royal Bank of Canada, Toronto</span>
            <span>ROYCCAT2</span>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.tableBodyElements}>
            <span>2</span>
            <span>
              Flag
              <img className={Styles.flag} src={gbpFlag} alt=""/>
            </span>
            <span>Royal Bank of Canada, Toronto</span>
            <span>ROYCCAT2</span>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.tableBodyElements}>
            <span>3</span>
            <span>
              Flag
              <img className={Styles.flag} src={chfFlag} alt=""/>
            </span>
            <span>Royal Bank of Canada, Toronto</span>
            <span>ROYCCAT2</span>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.tableBodyElements}>
            <span>4</span>
            <span>
              Flag
              <img className={Styles.flag} src={canadaFlag} alt=""/>
            </span>
            <span>Royal Bank of Canada, Toronto</span>
            <span>ROYCCAT2</span>
          </div>
        </TableBody>
      </div>
    </div>
    <div className="container">
      <BigTitleBlockSet/>
    </div>
  </div>
);

export default CorrespondentBank;
