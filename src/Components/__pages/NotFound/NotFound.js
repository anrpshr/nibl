import React from 'react';

import './NotFound.scss';

const NotFound = () => (
  <div id="notfound-page">
    <div>This page is currently in development.</div>
    <a href="/">Home</a>
  </div>
);

export default NotFound;
