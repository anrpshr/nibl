import React from 'react';
import Styles from './niblCapital.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';

const NiblCapital = () => (
  <div className={Styles.niblCapital}>
    <BannerBlock
      background={banner}
      title="NIBL Capital"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className="white-gradient main--content">
      <div className="container">
        <div className="heading">
          <h1>Capital Work Plan</h1>
        </div>
        <div className={Styles.basicText}>
          Paid Up Capital Plan for 800 Crores Amount in Crores
        </div>
        <ContentHeader>
          <div className={Styles.capitalTbl}>
            <div className={Styles.cellOne}>Particulars</div>
            <div className={Styles.cellTwo}>Ashad end 2072</div>
            <div className={Styles.cellThree}>Ashad end 2073</div>
            <div className={Styles.cellFour}>Ashad end 2074</div>
            <div className={Styles.cellFive}>Total</div>
          </div>
        </ContentHeader>
        <TableBody>
          <div className={Styles.capitalTbl}>
            <div className={Styles.cellOne}>Opening paid up</div>
            <div className={Styles.cellTwo}>10</div>
            <div className={Styles.cellThree}>20</div>
            <div className={Styles.cellFour}>20</div>
            <div className={Styles.cellFive}>50</div>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.capitalTbl}>
            <div className={Styles.cellOne}>Opening paid up</div>
            <div className={Styles.cellTwo}>10</div>
            <div className={Styles.cellThree}>20</div>
            <div className={Styles.cellFour}>20</div>
            <div className={Styles.cellFive}>50</div>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.capitalTbl}>
            <div className={Styles.cellOne}>Opening paid up</div>
            <div className={Styles.cellTwo}>10</div>
            <div className={Styles.cellThree}>20</div>
            <div className={Styles.cellFour}>20</div>
            <div className={Styles.cellFive}>50</div>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.capitalTbl}>
            <div className={Styles.cellOne}>Opening paid up</div>
            <div className={Styles.cellTwo}>10</div>
            <div className={Styles.cellThree}>20</div>
            <div className={Styles.cellFour}>20</div>
            <div className={Styles.cellFive}>50</div>
          </div>
        </TableBody>
        <TableBody>
          <div className={Styles.capitalTbl}>
            <div className={Styles.cellOne}>Opening paid up</div>
            <div className={Styles.cellTwo}>10</div>
            <div className={Styles.cellThree}>20</div>
            <div className={Styles.cellFour}>20</div>
            <div className={Styles.cellFive}>50</div>
          </div>
        </TableBody>
      </div>
    </div>
  </div>
);

export default NiblCapital;
