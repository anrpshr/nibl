import React from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Styles from './prithivi.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import LeftImageText from '../../../LeftImageText';
import ButtonRedBorder from '../../../ButtonRedBorder';
import BigTitleBlock from '../../../Layouts/Block/BigTitleBlock';
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Images
import background from '../../../../assets/img/compliance-bg.jpg';
import prithiImg from '../../../../assets/img/compliance1.jpg';
import bottomBg from '../../../../assets/img/strategic-obj-bg.jpg';
import downArrow from '../../../../assets/img/down-arrow.png';
import dummy from '../../../../assets/img/dummy.png';
import niblImg from '../../../../assets/img/nibl-dummy.jpg';

const PritiviRemitance = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      background={background}
      title="Prithivi Remitance"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className={Styles.greyInner}>
      <div className="container">
        <div className={Styles.heading}>
          <h1>Prithivi Remitance</h1>
        </div>
        <LeftImageText
          title="Overview"
          image={niblImg}
        >
          <p>
          Prithivi Remit is an internet based money transfer product introduced by Nepal
          Investment Bank Limited. It is web based remittance system that is accessible
          to our branches and domestic as well as international agents. It ensures prompt
          delivery of the remittance to beneficiaries. Prithivi Remit is monitored and
          serviced by NIBL Remittance Department to deliver fast and reliable service.
            <br/>
          It is a Java (front-end software) based money transfer system that automates
          inward and outward remittance.
          </p>
          <ButtonRedBorder title="Remitance"/>
        </LeftImageText>

        <div className={Styles.heading}>
          <h1>Main Features</h1>
        </div>
        <div className={Styles.flexSection}>
          <BigTitleBlock title="Accessibility and Authentication">
            <ul className={Styles.list}>
              <li>
                <ChevronRight color="primary"/>
                Internet Based Technology, thus easily accessible to all our branches and agents
                network
              </li>
              <li>
                <ChevronRight color="primary"/>
                Provide real time update on remittance request for prompt processing and delivery of
                payment.
              </li>
              <li>
                <ChevronRight color="primary"/>
                Provide secure and reliable means for accessing the product, uploading the
                transaction at Remitting Agent end and downloading the transaction at
                Paying Agent end.
              </li>
              <li>
                <ChevronRight color="primary"/>
                Authentication using Digital Certificates, User Name and Password.
              </li>
            </ul>
          </BigTitleBlock>
          <ImageBlockWithOverlay title="Prithivi Remitance" image={dummy}>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget 
            </p>
            <a href="1">Apply Now</a>
          </ImageBlockWithOverlay>
        </div>
        <div className={Styles.flexSection}>
          <ImageBlockWithOverlay title="Prithivi Remitance" image={dummy}>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget 
            </p>
            <a href="1">Apply Now</a>
          </ImageBlockWithOverlay>
          <BigTitleBlock title="Fund Management and Reconciliation">
            <ul className={Styles.list}>
              <li>
                <ChevronRight color="primary"/>
                Internet Based Technology, thus easily accessible to all our branches and agents
                network
              </li>
              <li>
                <ChevronRight color="primary"/>
                Internet Based Technology, thus easily accessible to all our branches and agents
                network
              </li>
              <li>
                <ChevronRight color="primary"/>
                Internet Based Technology, thus easily accessible to all our branches and agents
                network
              </li>
            </ul>
          </BigTitleBlock>
        </div>
      </div>

      <div className={Styles.bottomSection} style={{ background: `url(${bottomBg})` }}>
        <div className="container">
          <div className={Styles.outerSection}>
            <div className={Styles.innerSection}>
              <BigTitleBlock title="Currency Conversion">
                <div className={Styles.elementsWrapper}>
                  <input placeholder="Rs 600"/>
                  <div className={Styles.currency}>
                    <div className={Styles.currencySelector}>
                      <img src={downArrow} alt=""/>
                      <select>
                        <option>Rs</option>
                        <option>USD</option>
                      </select>
                    </div>
                    <div className={Styles.currencySelector}>
                      <img src={downArrow} alt=""/>
                      <select>
                        <option>USD</option>
                        <option>Rs</option>
                      </select>
                    </div>
                  </div>
                  <ButtonRedBorder title="Apply Now"/>
                  <div className={Styles.number}>
                    5.38 USD
                  </div>
                </div>
              </BigTitleBlock>
              <div className={Styles.bottomList}>
                <ul>
                  <li>
                    <ChevronRight color="primary"/>
                    To continue to develop products and services that reduce our cost of funds
                  </li>
                  <li>
                    <ChevronRight color="primary"/>
                    Quoting deal rate to Remitting Agent.
                  </li>
                  <li>
                    <ChevronRight color="primary"/>
                    Currency conversion as per the quoted rate.
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <BigTitleBlockSet/>
  </div>
);

export default PritiviRemitance;
