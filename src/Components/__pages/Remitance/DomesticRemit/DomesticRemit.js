import React from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Styles from './domesticRemit.module.scss';

// Components
import BannerBlock from '../../../BannerBlock';
import LeftImageText from '../../../LeftImageText';
import ButtonRedBorder from '../../../ButtonRedBorder';
import BigTitleBlock from '../../../Layouts/Block/BigTitleBlock';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

// Iamges
import background from '../../../../assets/img/compliance-bg.jpg';
import bottomBg from '../../../../assets/img/strategic-obj-bg.jpg';
import niblImg from '../../../../assets/img/nibl-dummy.jpg';

const DomesticRemit = () => (
  <div className={Styles.wrapper}>
    <BannerBlock
      background={background}
      title="Domestic Remit"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
    />
    <div className={Styles.greyInner}>
      <div className="container">
        <div className={Styles.heading}>
          <h1>Domestic ebanking Remitance</h1>
        </div>
        <LeftImageText image={niblImg} title="Overview">
          <p>
          Domestic eBanking Remittance avails customers the facility to instantly transfer money
          to their relatives/friends online. This addresses the need of NIBL Customer for
          instant money transfer facility across Nepal, even in locations without NIBL branches.
            <br/>
          The payment can be sent against government issued valid identity card of your
          relatives/friends. Customers will need to fill the remittance form available in
          NIBL website. After filling the form click the Send Button, which will redirect
          to NIBL eBanking Login Page. After logging in and carrying out successful transaction
          Unique Number is provided which can be sent to friends/relatives to receive the payment.
            <br/>
          Note: eBanking User ID with Bill Payments enabled is required for Online Transaction.
          </p>
          <ButtonRedBorder title="Remitance"/>
        </LeftImageText>

        <div className={Styles.heading}>
          <h1>NIBL Domestic Remitance</h1>
        </div>
        <div className={Styles.flexSection}>
          <BigTitleBlock title="Sender Details">
            <form action="submit">
              <div className={Styles.formElements}>
                <label>Name</label>
                <input type="text" name="name"/>
              </div>
              <div className={Styles.formElements}>
                <label>Sender Address:</label>
                <input type="text" name="address"/>
              </div>
              <div className={Styles.formElements}>
                <label>Sender Mobile No:</label>
                <input type="text" name="contact"/>
              </div>
            </form>
          </BigTitleBlock>
          <BigTitleBlock title="Receiver Details">
            <form action="submit">
              <div className={Styles.formElements}>
                <label>Receiver Name</label>
                <input type="text" name="rname"/>
              </div>
              <div className={Styles.formElements}>
                <label>Receiver Address:</label>
                <input type="text" name="raddress"/>
              </div>
              <div className={Styles.formElements}>
                <label>Sender Mobile No:</label>
                <input type="text" name="rcontact"/>
              </div>
              <div className={Styles.formElements}>
                <label>ID No:</label>
                <input type="text" name="rid"/>
              </div>
              <div className={Styles.formElements}>
                <label>Amount to Send:</label>
                <input type="text" name="ramount"/>
              </div>
              <div className={Styles.formElements}>
                <label>Charge Amount NPR:</label>
                <input type="text" name="charge_amount"/>
              </div>
              <div className={Styles.formElements}>
                <label>Total Amount NPR:</label>
                <input type="text" name="rcontact"/>
              </div>
              <div className={Styles.buttons}>
                <ButtonRedBorder title="Send"/>
                <ButtonRedBorder title="Clear"/>
              </div>
            </form>
          </BigTitleBlock>
        </div>
      </div>
      <div className={Styles.bottomSection} style={{ background: `url(${bottomBg})` }}>
        <div className="container">
          <div className={Styles.bottomFlex}>
            <div className={Styles.benifits}>
              <h1>Benifits</h1>
              <ul>
                <li>
                  <ChevronRight color="primary"/>
                  To continue to develop products and services that reduce our cost of funds
                </li>
                <li>
                  <ChevronRight color="primary"/>
                  Quoting deal rate to Remitting Agent.
                </li>
                <li>
                  <ChevronRight color="primary"/>
                  Currency conversion as per the quoted rate.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <BigTitleBlockSet/>
  </div>
);

export default DomesticRemit;
