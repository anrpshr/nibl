import React from 'react';
import Styles from './findOurAgent.module.scss';

// Components
import Form from '../../../Form';
import ButtonRedBorder from '../../../ButtonRedBorder';

// Images
import banner from '../../../../assets/img/big-banner.jpg';

const FindOurAgent = () => (
  <div className={Styles.wrapper}>
    <Form
      title="Agent List"
      description="Domestic eBanking Remittance avails customers the facility to instantly transfer money to their relatives/friends online"
      background={banner}
      secondaryTitle="Find Our Agent"
      formTitle="AgentForm"
    >
      <div className={Styles.formBlocks}>
        <div className={Styles.block}>
          <span>Name</span>
          <select>
            <option>Adil Money Transfer</option>
            <option>XYZ Money Transfer</option>
            <option>ABC Money Transfer</option>
          </select>
        </div>

        <div className={Styles.block}>
          <span>Branch</span>
          <select>
            <option>Belbari</option>
            <option>Bansbari</option>
            <option>Belbari</option>
          </select>
        </div>

        <div className={Styles.block}>
          <span>District</span>
          <select>
            <option>Acham</option>
            <option>Kathmandu</option>
            <option>Lalitpur</option>
          </select>
        </div>

        <div className={Styles.block}>
          <span>Contact Person</span>
          <input type="text"/>
        </div>

        <div>
          <ButtonRedBorder title="submit"/>
        </div>
      </div>
    </Form>
  </div>
);

export default FindOurAgent;
