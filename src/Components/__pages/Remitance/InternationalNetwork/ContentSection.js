import React from 'react';
import Styles from './internationalNetwork.module.scss';

// Components
import ContentCard from '../../../ContentCard';

const ContentSection = () => (
  <div className={Styles.contentSection}>
    <div className="heading">
      <h1>Country Representatives</h1>
    </div>
    <ContentCard>
      <div className={Styles.contentCard}>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Birmingham Branch</h5>
          <div className={Styles.contentElement}>
            <span>276 Soho Road, Birmingham, B21 9LZ</span>
            <span>Phone: +44-8006350263</span>
            <span>Fax: +44-121 515 5929</span>
            <span>Email: <a href="1">birmingham.branch@uk.uaeexchange.com</a></span>
            <span>Working Hours: Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
            <span>Mon-Sat: 09:00 Hrs to 17:30 Hrs</span>
          </div>
        </div>
      </div>
    </ContentCard>
    <div className="heading _heading">
      <h1>NIBL Marketing Representatives</h1>
    </div>
    <ContentCard>
      <div className={Styles.contentCard}>
        <div className={Styles.content}>
          <h5>Ram Lama</h5>
          <div className={Styles.contentElement}>
            <span>Mobile:  +966558614540 & +966533109230</span>
            <span><a href="1">Email: rlbksa@gmail.com</a></span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Ram Lama</h5>
          <div className={Styles.contentElement}>
            <span>Mobile:  +966558614540 & +966533109230</span>
            <span><a href="1">Email: rlbksa@gmail.com</a></span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Ram Lama</h5>
          <div className={Styles.contentElement}>
            <span>Mobile:  +966558614540 & +966533109230</span>
            <span><a href="1">Email: rlbksa@gmail.com</a></span>
          </div>
        </div>
        <div className={Styles.content}>
          <h5>Ram Lama</h5>
          <div className={Styles.contentElement}>
            <span>Mobile:  +966558614540 & +966533109230</span>
            <span><a href="1">Email: rlbksa@gmail.com</a></span>
          </div>
        </div>
      </div>
    </ContentCard>
  </div>
);

export default ContentSection;