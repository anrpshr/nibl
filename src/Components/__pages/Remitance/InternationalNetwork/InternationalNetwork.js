import React, { Component } from 'react';
import Styles from './internationalNetwork.module.scss';

// Component
import BannerBlock from '../../../BannerBlock';
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

import ContentSection from './ContentSection';

// Images
import banner from '../../../../assets/img/compliance-bg.jpg';

class InternationalNetwork extends Component {
  state = {
    items: [
      {
        name: 'United Kingdom',
        key: 'united-kingdom'
      },
      {
        name: 'UAE',
        key: 'uae'
      },
      {
        name: 'Qatar',
        key: 'qatar'
      },
      {
        name: 'Saudi Arabia',
        key: 'saudi-arabia'
      },
      {
        name: 'Cyprus',
        key: 'cyprus'
      },
      {
        name: 'Kuwait',
        key: 'kuwait'
      },
      {
        name: 'United States',
        key: 'united-states'
      },
      {
        name: 'South Korea',
        key: 'south-korea'
      }
    ],
    activeItemKey: 'united-kingdom'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title="Internation Networks"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
          background={banner}
        />
        <div className="greyWrapper">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <div className={Styles.tabContent}>
              <TabContent>
                <ContentSection isActive={activeItemKey === 'united-kingdom'}/>
                <ContentSection isActive={activeItemKey === 'uae'}/>
              </TabContent>
            </div>
          </div>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default InternationalNetwork;
