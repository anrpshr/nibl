import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import sitemap from '../../../routing/siteMap';

// Components
import PrithiviRemit from './PrithiviRemit';
import DomesticRemit from './DomesticRemit';
import FindOurAgent from './FindOurAgent';
import InternationalNetworks from './InternationalNetwork';

const Remitance = () => (
  <Switch>
    <Route exact path={sitemap.remitance.prithiviRemit} component={PrithiviRemit}/>
    <Route exact path={sitemap.remitance.domesticRemit} component={DomesticRemit}/>
    <Route exact path={sitemap.remitance.findOurAgent} component={FindOurAgent}/>
    <Route exact path={sitemap.remitance.internationalNetworks} component={InternationalNetworks}/>
    <Redirect to={sitemap.remitance.prithiviRemit}/>
  </Switch>
);

export default Remitance;
