import React, { Component } from 'react';
import Styles from './career.module.scss';

// Components
import TabNav from '../../TabNav';
import BannerBlock from '../../BannerBlock';
import TabContent from '../../TabContent';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import JobsAndVacancies from './JobsAndVacancies';
import WhyWorkForUS from './WhyWorkForUs';
import Faq from './Faq';
import MeetOurEmployees from './MeetOurEmployees';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';

class Career extends Component {
  state={
    items: [
      {
        name: 'Jobs and Vacancies',
        key: 'jobs-and-vacancies'
      },
      {
        name: 'Why work for us',
        key: 'why-work-for-us'
      },
      {
        name: 'FAQ',
        key: 'faq'
      },
      {
        name: 'Meet our Employees',
        key: 'meet-our-employees'
      }
    ],
    activeItemKey: 'jobs-and-vacancies'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                stretch
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            <TabContent>
              <JobsAndVacancies isActive={activeItemKey === 'jobs-and-vacancies'}/>
              <WhyWorkForUS isActive={activeItemKey === 'why-work-for-us'}/>
              <Faq isActive={activeItemKey === 'faq'}/>
              <MeetOurEmployees isActive={activeItemKey === 'meet-our-employees'}/>
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default Career;
