import React from 'react';
import Styles from './career.module.scss';

// Components
import ContentCard from '../../ContentCard';

const WhyWorkForUs = () => (
  <div className={Styles.innerWrapper}>
    <div className="container">
      <div className="heading">
        <h1>Why Work For Us</h1>
      </div>
      <ContentCard>
        <div>
          <h5>Work Culture</h5>
          <p>
            To develop a customer centric work culture with special emphasis on
            customer care and convenience, the Bank ensures a highest level of
            integrity to our customer, creating an ongoing relationship of trust
            and confidence.
          </p>
          <p>
            We treat our customers with honesty, fairness and respect.
            We also value our employees and think it is our most valuable asset.
            We respect the worth and dignity of individual employees who devote
            their careers for the bank. We strongly believe that in investing in
            the development of our human resources would ultimately result in competent,
            confident and committed workforce.
          </p>
        </div>
        <div>
          <h5>Diversity</h5>
          <p>
            We believe that our success and competitiveness depends upon our
            ability to embrace diversity and realize the benefits of treating
            all stakeholders with care, appreciation and respect. We value diversity
            in our workforce. While hiring and promoting employees, we don’t distinguish
            between the gender, race, religion or age group. We have proportionate number
            of male and female staffs. Female employees are head of several departments
            and branches.
          </p>
        </div>
        <div>
          <h5>Team Work</h5>
          <p>
            Team work means to work together for a common purpose with full
            trust on team members. NIBL believes in team work and feels that
            loyal and motivated team can produce extraordinary results. The
            credit for the success of this bank goes to the team work of staff
            members working in close cooperation of each other. We give opportunities
            to our staff to work at cross-functional levels and learn new skills working
            with different people.
          </p>
        </div>
      </ContentCard>
    </div>
  </div>
);

export default WhyWorkForUs;
