import React from 'react';
import Styles from './career.module.scss';

// Components
import Form from '../../Form';
import ButtonRedBorder from '../../ButtonRedBorder';

// Images
import background from '../../../assets/img/big-banner.jpg';

const Application = () => (
  <div>
    <Form
      background={background}
      title="HR"
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
      secondaryTitle="HR VACANCY APPLICATON"
      formTitle="Vacancy Application"
    >
      <div className={Styles.formElement}>
        <span>Applicant Name:</span>
        <input className={Styles.name} type="text"/>
      </div>

      <div className={Styles.formElement}>
        <span>Applied Position:</span>
        <select>
          <option>Manager</option>
          <option>Assistant Manager</option>
        </select>
      </div>

      <div className={Styles.formElement}>
        <span>Curriculum Vitae:</span>
        <input className={Styles.file} type="file"/>
      </div>

      <div className={Styles.formElement}>
        <span>Captcha:</span>
        <input type="text"/>
      </div>

      <div className={Styles.formElement}>
        <span>Enter Captcha Text:</span>
        <div>
          <input type="text"/>
          <div className={Styles.button}>
            <ButtonRedBorder title="Send"/>
          </div>
        </div>
      </div>
    </Form>
  </div>
);

export default Application;
