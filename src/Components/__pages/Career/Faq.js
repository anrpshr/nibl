import React from 'react';
import Styles from './career.module.scss';

// Components
import ExpansionPanel from '../../ExpansionPanel';

const Faq = () => (
  <div className={Styles.innerWrapper}>
    <div className="container">
      <div className="heading">
        <h1>FAQ</h1>
      </div>
      <div className={Styles.faq}>
        <ExpansionPanel
          isOrdered
          items={[
            {
              primary: <div>Which browsers support NIBL eBanking?</div>,
              
              secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
            },
            {
              primary: <div>Does double clicking inside eBanking page work?</div>,
              
              secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
            },
            {
              primary: <div>How many times can I attempt to login?</div>,
              
              secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
            },
            {
              primary: <div>What are valid special characters that can be used in passwords?</div>,
              
              secondary: <div>Internet Explorer 5.5 or above, Mozilla 3.0, Opera 9.0, Safari supports NIBL eBanking, however the Back and Forward Button won’t work in eBanking System.</div>
            }
          ]}
        />
      </div>
    </div>
  </div>
);

export default Faq;
