export default class CAREERTransformer {
  constructor(CAREEROBJECT) {
    this.title = CAREEROBJECT.Title;
    this.content = CAREEROBJECT.Content;
  }
}