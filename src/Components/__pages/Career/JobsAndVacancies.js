import React from 'react';
import { NavLink } from 'react-router-dom';

import Styles from './career.module.scss';

// Components
import ContentCard from '../../ContentCard';
import SearchBar from '../../SearchBar';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';

const JobsAndVacancies = () => (
  <div className={Styles.innerWrapper}>
    <div className="container">
      <div className="heading">
        <h1>Jobs and Vacancies</h1>
      </div>
      <ContentCard>
        <p>
          Welcome to a world of opportunities at Nepal Investment Bank Limited.
          At NIBL, you can explore and develop your personal and professional
          potential within one of the most dynamic and diverse organizations in Nepal.
        </p>

        <div className={Styles.address}>
          <span>Head, Human Resource</span>
          <span>Nepal Investment Bank Ltd</span>
          <span>Durbar Marg, P.O. Box: 3412</span>
          <span>Kathmandu, Nepal</span>
        </div>
      </ContentCard>
    </div>

    <div className={Styles.vacanciesSection}>
      <div className="container">
        <SearchBar/>

        <div className="heading">
          <h1>Vacancies</h1>
        </div>

        <ContentHeader>
          <div className={Styles.tableHead}>
            <div className={Styles.sn}>
              S.N
            </div>
            <div className={Styles.position}>
              Position
            </div>
            <div className={Styles.deadline}>
              Deadline
            </div>
          </div>
        </ContentHeader>
        <ExpansionPanel
          isOrdered
          items={[
            {
              primary:
              (
                <div className={Styles.expansionGrid}>
                  <span className={Styles.expansionPosition}>HR</span>
                  <span className={Styles.expansionDeadline}>14th July, 2019</span>
                  <NavLink className={Styles.apply} exact to="/career/application">Apply Now</NavLink>
                </div>
              ),
              secondary:
              (
                <div className={Styles.expanded}>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                  natoque penatibus et magnis dis parturient montes, nascetur
                  ridiculus mus.
                </div>
              )
            },
            {
              primary:
              (
                <div className={Styles.expansionGrid}>
                  <span className={Styles.expansionPosition}>HR</span>
                  <span className={Styles.expansionDeadline}>14th July, 2019</span>
                  <a className={Styles.apply} href="1">Apply Now</a>
                </div>
              ),
              secondary:
              (
                <div className={Styles.expanded}>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                  natoque penatibus et magnis dis parturient montes, nascetur
                  ridiculus mus.
                </div>
              )
            },
            {
              primary:
              (
                <div className={Styles.expansionGrid}>
                  <span className={Styles.expansionPosition}>HR</span>
                  <span className={Styles.expansionDeadline}>14th July, 2019</span>
                  <a className={Styles.apply} href="1">Apply Now</a>
                </div>
              ),
              secondary:
              (
                <div className={Styles.expanded}>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                  natoque penatibus et magnis dis parturient montes, nascetur
                  ridiculus mus.
                </div>
              )
            },
            {
              primary:
              (
                <div className={Styles.expansionGrid}>
                  <span className={Styles.expansionPosition}>HR</span>
                  <span className={Styles.expansionDeadline}>14th July, 2019</span>
                  <a className={Styles.apply} href="1">Apply Now</a>
                </div>
              ),
              secondary:
              (
                <div className={Styles.expanded}>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                  natoque penatibus et magnis dis parturient montes, nascetur
                  ridiculus mus.
                </div>
              )
            },
            {
              primary:
              (
                <div className={Styles.expansionGrid}>
                  <span className={Styles.expansionPosition}>HR</span>
                  <span className={Styles.expansionDeadline}>14th July, 2019</span>
                  <a className={Styles.apply} href="1">Apply Now</a>
                </div>
              ),
              secondary:
              (
                <div className={Styles.expanded}>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                  Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                  natoque penatibus et magnis dis parturient montes, nascetur
                  ridiculus mus.
                </div>
              )
            }
          ]}
        />
      </div>
    </div>
  </div>
);

export default JobsAndVacancies;
