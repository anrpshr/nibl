import React, { Component } from 'react';
import Styles from './bankingDays.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BankingDaysTbl from './BankingDaysTbl';

// Images
import banner from '../../../assets/img/abt_banner.jpg';

class BankingDays extends Component {
  state = {
    items: [
      {
        name: 'Normal Working Hours (Cash Transaction)',
        key: 'normal-working-hrs'
      },
      {
        name: 'Evening Counter (Inside Valley)',
        key: 'evening-counter-in'
      },
      {
        name: 'Evening Counter (Outside Valley)',
        key: 'evening-counter-out'
      },
      {
        name: 'Saturday and Public Holidays',
        key: 'holidays'
      }
    ],
    activeItemKey: 'normal-working-hrs'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <>
        <BannerBlock
          background={banner}
        />
        <div className="white-gradeint main--content">
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                arrows
                stretch
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            <TabContent>
              <BankingDaysTbl isActive={activeItemKey === 'normal-working-hrs'}/>
              <BankingDaysTbl isActive={activeItemKey === 'evening-counter-in'}/>
              <BankingDaysTbl isActive={activeItemKey === 'evening-counter-out'}/>
              <BankingDaysTbl isActive={activeItemKey === 'holidays'}/>
            </TabContent>
          </div>
        </div>
      </>
    );
  }
}

export default BankingDays;
