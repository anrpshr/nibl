import React from 'react';

import Styles from './bankingDays.module.scss';

// Components
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';

const BankingDaysTbl = () => (
  <>
    <ContentHeader>
      <div className={Styles.gridBlock}>
        <span>S.N</span>
        <span className={Styles.day}>Day</span>
        <span className={Styles.total}>Total</span>
      </div>
    </ContentHeader>
    <TableBody>
      <div className={Styles.gridBlock}>
        <span>1</span>
        <span className={Styles.day}>Sunday - Thursday</span>
        <span className={Styles.total}>9:30 a.m. - 3:30 p.m.</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridBlock}>
        <span>1</span>
        <span className={Styles.day}>Sunday - Thursday</span>
        <span className={Styles.total}>9:30 a.m. - 3:30 p.m.</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridBlock}>
        <span>1</span>
        <span className={Styles.day}>Sunday - Thursday</span>
        <span className={Styles.total}>9:30 a.m. - 3:30 p.m.</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridBlock}>
        <span>1</span>
        <span className={Styles.day}>Sunday - Thursday</span>
        <span className={Styles.total}>9:30 a.m. - 3:30 p.m.</span>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.gridBlock}>
        <span>1</span>
        <span className={Styles.day}>Sunday - Thursday</span>
        <span className={Styles.total}>9:30 a.m. - 3:30 p.m.</span>
      </div>
    </TableBody>
  </>
);

export default BankingDaysTbl;
