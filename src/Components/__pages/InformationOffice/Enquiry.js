import React from 'react';
import LocationOn from '@material-ui/icons/LocationOn';
import Styles from './infoOffice.module.scss';

// Components
import ContentCard from '../../ContentCard';

const Enquiry = () => (
  <div className={Styles.enquiry}>
    <div className="heading">
      <h1>Enquiry</h1>
      {/* <a href="1" className={Styles.location}>
        <LocationOn/>
        <span>See in the map</span>
      </a> */}
    </div>
    <div className={Styles.flexBlock}>
      <div className={Styles.block}>
        <ContentCard>
          <div className={Styles.content}>
            <h5>Remittance (9:00 to 17:30)</h5>
            <span>Remittance Department </span>
            <span>Putalisadak, Kathmandu</span>
            <span>Tel: 4445305, 9851045225 </span>
            <span>Email: remittance@nibl.com.np</span>
          </div>
        </ContentCard>
      </div>

      <div className={Styles.block}>
        <ContentCard>
          <div className={Styles.content}>
            <h5>Dispute &amp; Grievance Handling (9:00 to 17:30)</h5>
            <span>Deepak Shrestha</span>
            <span>Putalisadak, Kathmandu</span>
            <span>Tel: 4445305, 9851045225 </span>
            <span>Email: remittance@nibl.com.np</span>
          </div>
        </ContentCard>
      </div>

      <div className={Styles.block}>
        <ContentCard>
          <div className={Styles.content}>
            <h5>Card Lost / Inquiry / Renewal (24hours)</h5>
            <span>Uttar Dhoka, Lazimpat, Kathmandu</span>
            <span>Toll Free No.16600100070 </span>
            <span>Direct line 4445301, 4005147, 9851126440</span>
            <span>Email: remittance@nibl.com.np</span>
          </div>
        </ContentCard>
      </div>
    </div>
  </div>
);

export default Enquiry;
