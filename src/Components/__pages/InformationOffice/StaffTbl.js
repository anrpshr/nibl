import React from 'react';
import LocationOn from '@material-ui/icons/LocationOn';
import Styles from './infoOffice.module.scss';

// Components
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';

const StaffTbl = () => (
  <div className={Styles.staffTbl}>
    {/* <a href="1" className={Styles.location}>
      <LocationOn/>
      <span>See the branch location</span>
    </a> */}
    <ContentHeader>
      <div className={Styles.table}>
        <div>S.N</div>
        <div className={Styles.service}>Service Status</div>
        <div className={Styles.male}>Male</div>
        <div className={Styles.female}>Female</div>
        <div className={Styles.total}>Total</div>
      </div>
    </ContentHeader>
    <TableBody>
      <div className={Styles.table}>
        <div>1</div>
        <div className={Styles.service}>Permanent</div>
        <div className={Styles.male}>598</div>
        <div className={Styles.female}>498</div>
        <div className={Styles.total}>1096</div>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.table}>
        <div>2</div>
        <div className={Styles.service}>Probation</div>
        <div className={Styles.male}>598</div>
        <div className={Styles.female}>498</div>
        <div className={Styles.total}>1096</div>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.table}>
        <div>3</div>
        <div className={Styles.service}>Contract</div>
        <div className={Styles.male}>598</div>
        <div className={Styles.female}>498</div>
        <div className={Styles.total}>1096</div>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.table}>
        <div>4</div>
        <div className={Styles.service}>Permanent</div>
        <div className={Styles.male}>598</div>
        <div className={Styles.female}>498</div>
        <div className={Styles.total}>1096</div>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.table}>
        <div>5</div>
        <div className={Styles.service}>Permanent</div>
        <div className={Styles.male}>598</div>
        <div className={Styles.female}>498</div>
        <div className={Styles.total}>1096</div>
      </div>
    </TableBody>
    <TableBody>
      <div className={Styles.table}>
        <div>6</div>
        <div className={Styles.service}>Permanent</div>
        <div className={Styles.male}>598</div>
        <div className={Styles.female}>498</div>
        <div className={Styles.total}>1096</div>
      </div>
    </TableBody>
  </div>
);

export default StaffTbl;
