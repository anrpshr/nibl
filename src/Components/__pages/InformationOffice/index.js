import React, { Component } from 'react';
import classnames from 'classnames';
import Styles from './infoOffice.module.scss';

// Components
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import StaffTbl from './StaffTbl';
import Enquiry from './Enquiry';

// Images
import banner from '../../../assets/img/corporate-bg.jpg';
import image from '../../../assets/img/gm.jpg';

class InformationOffice extends Component {
  state = {
    items: [
      {
        name: 'Gender Wise Total Staff',
        key: 'gender-wise-total-staff'
      },
      {
        name: 'Province Manager',
        key: 'province-manager'
      },
      {
        name: 'Enquiry',
        key: 'enquiry'
      }
    ],
    activeItemKey: 'gender-wise-total-staff'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
          title="Information Office"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="container">
          <div className={Styles.secondaryWrapper}>
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <div className={Styles.tabContent}>
              <TabContent>
                <StaffTbl isActive={activeItemKey === 'gender-wise-total-staff'}/>
                <StaffTbl isActive={activeItemKey === 'province-manager'}/>
                <Enquiry isActive={activeItemKey === 'enquiry'}/>
              </TabContent>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default InformationOffice;
