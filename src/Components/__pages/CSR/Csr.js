import React, { Component } from 'react';
import Styles from './csr.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import Latest from './Latest';
import Featured from './Featured';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';

class Csr extends Component {
  state = {
    items: [
      {
        name: 'Latest',
        key: 'latest'
      },
      {
        name: 'Featured',
        key: 'featured'
      }
    ],
    activeItemKey: 'latest'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
          title="Corporate Social Responsibility"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="white-gradient main--content">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <TabContent>
              <Latest isActive={activeItemKey === 'latest'}/>
              <Featured title="" isActive={activeItemKey === 'featured'}/>
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default Csr;
