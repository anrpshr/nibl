import React from 'react';
import PropTypes from 'prop-types';
import Styles from './csr.module.scss';

// Compontent
import CardImageAndText from '../../CardImageAndText';
import BigBlockTitleSet from '../../Layouts/Block/BigTitleBlockSet';

// Images
import contentImg from '../../../assets/img/contentImg.jpg';
import image1 from '../../../assets/img/news1.jpg';

const CsrDetail = ({ title, publishedDetail, content }) => (
  <div className={Styles.wrapper}>
    <div className="greyWrapper">
      <div className="container">
        <div className="heading">
          <h1>32 AGM Press Release{title}</h1>
        </div>
        <div className={Styles.content}>
          <div className={Styles.publishedDetail}>
            <span>Kathmandu, January 13, 2019 {publishedDetail}</span>
          </div>
          {content}
          <p>
            The 32nd Annual General Meeting (AGM) of Nepal Investment Bank Ltd
            (NIBL) held on 13 January, 2019 (29 Poush 2075) at Rastriya Sabha Griha,
            Kathmandu decided to distribute 40 % dividend - 18 % stock dividend and 22 %
            cash dividend - on its total paid-up capital to the shareholders.
          </p>
          <p>
            During the year, NIBL earned Operating Profit of NPR 4.95 billion as
            compared to previous year 4.68 billion. The Net Profit of the Bank for
            FY 2074-75 is NPR 3.65 billion. NIBL’s Paid-up capital will reach NPR 12.58
            billion (after issue of 18% Bonus Share for FY 2074-2075). In FY 2074-2075 NIBL’s
            deposits have increased by 11.45 %, reaching NPR 140 billion. Total lending of the
            bank increased by 14.44 % reaching NPR 120 billion. The NPA ratio of the bank is at 1.36 %.
          </p>
          <img src={contentImg} alt=""/>
          <p>
            The bank has more than 9 lakhs customer base catered from its 78 branches,
            8 extension counters across the country. NIBL has a network of 109 ATMs and
            leads a consortium 14 Visa Associate banks and 7 NPN member banks.
          </p>
          <p>
            During the fiscal year 2074-2075, NIBL Capital Markets Ltd. a wholly
            owned subsidiary of Nepal Investment Bank Limited acquired Ace Capital
            Limited to form NIBL Ace Capital Limited. It has become Nepal’s largest
            merchant bank to have paid-up capital of NPR 27 million with its branches
            located at Laldurbar, Lagankhel, Pokhara, Butwal, Birgunj and Biratnagar.
          </p>
        </div>
      </div>
    </div>

    <div className={Styles.moreNews}>
      <div className="container">
        <div className="heading">
          <h1>More News</h1>
        </div>
        <div className={Styles.cardWrapper}>
          <div className={Styles.card}>
            <CardImageAndText
              image={image1}
              title="32 AGM Press Release"
              excerpt="Kathmandu, January 13, 2019– The 32nd Annual General Meeting (AGM) of Nepal Investment Bank Ltd (NIBL) held on 13 January, 2019 (29 Poush 2075) at Rastriya Sabha Griha, Kathmandu..."
              link="csr/csr-detail"
            />
          </div>
          <div className={Styles.card}>
            <CardImageAndText
              image={image1}
              title="32 AGM Press Release"
              excerpt="Kathmandu, January 13, 2019– The 32nd Annual General Meeting (AGM) of Nepal Investment Bank Ltd (NIBL) held on 13 January, 2019 (29 Poush 2075) at Rastriya Sabha Griha, Kathmandu..."
              link="a.com"
            />
          </div>
          <div className={Styles.card}>
            <CardImageAndText
              image={image1}
              title="32 AGM Press Release"
              excerpt="Kathmandu, January 13, 2019– The 32nd Annual General Meeting (AGM) of Nepal Investment Bank Ltd (NIBL) held on 13 January, 2019 (29 Poush 2075) at Rastriya Sabha Griha, Kathmandu..."
              link="a.com"
            />
          </div>
        </div>
      </div>
    </div>
    <BigBlockTitleSet/>
  </div>
);

CsrDetail.propTypes = {
  title: PropTypes.string.isRequired,
  publishedDetail: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default CsrDetail;
