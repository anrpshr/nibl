import React from 'react';
import Styles from './csr.module.scss';

// Components
import CardImageAndText from '../../CardImageAndText';

// Images
import newsImg from '../../../assets/img/news1.jpg';
import newsImg1 from '../../../assets/img/news2.jpg';
import newsImg2 from '../../../assets/img/news3.jpg';

const Featured = () => (
  <>
    <div className="heading">
      <h1>Latest News</h1>
    </div>
    <div className={Styles.cardWrapper}>
      <div className={Styles.card}>
        <CardImageAndText
          image={newsImg}
          title="32 AGM Press Release"
          excerpt="Kathmandu, January 13, 2019– The 32nd Annual General Meeting (AGM) of Nepal Investment Bank Ltd (NIBL) held on 13 January, 2019 (29 Poush 2075) at Rastriya Sabha Griha, Kathmandu..."
          link="csr/csr-detail"
        />
      </div>
      <div className={Styles.card}>
        <CardImageAndText
          image={newsImg1}
          title="Press Release of EuroMoney Excellence-Best Bank Nepal 2018"
          excerpt="Nepal Investment Bank has won the award of ‘Nepal’s Best Bank’ from Euromoney magazine’s Awards for Excellence 2018, one of the most significant accolades in financial services."
          link="a.com"
        />
      </div>
      <div className={Styles.card}>
        <CardImageAndText
          image={newsImg2}
          title="Press Release of EuroMoney Excellence-Best Bank Nepal 2018"
          excerpt="Nepal Investment Bank has won the award of ‘Nepal’s Best Bank’ from Euromoney magazine’s Awards for Excellence 2018, one of the most significant accolades in financial services."
          link="a.com"
        />
      </div>
      <div className={Styles.card}>
        <CardImageAndText
          image={newsImg1}
          title="Press Release of EuroMoney Excellence-Best Bank Nepal 2018"
          excerpt="Nepal Investment Bank has won the award of ‘Nepal’s Best Bank’ from Euromoney magazine’s Awards for Excellence 2018, one of the most significant accolades in financial services."
          link="a.com"
        />
      </div>
      <div className={Styles.card}>
        <CardImageAndText
          image={newsImg2}
          title="Press Release of EuroMoney Excellence-Best Bank Nepal 2018"
          excerpt="Nepal Investment Bank has won the award of ‘Nepal’s Best Bank’ from Euromoney magazine’s Awards for Excellence 2018, one of the most significant accolades in financial services."
          link="a.com"
        />
      </div>
      <div className={Styles.card}>
        <CardImageAndText
          image={newsImg}
          title="Press Release of EuroMoney Excellence-Best Bank Nepal 2018"
          excerpt="Nepal Investment Bank has won the award of ‘Nepal’s Best Bank’ from Euromoney magazine’s Awards for Excellence 2018, one of the most significant accolades in financial services."
          link="a.com"
        />
      </div>
    </div>
  </>
);

export default Featured;
