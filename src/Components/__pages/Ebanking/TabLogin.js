import React from 'react';

import { GLOBALS } from '../../../constants';
import styles from './ebanking.module.scss';

const TabLogin = () => (
  <div>
    <h1 className="heading">LOGIN</h1>
    <div className={styles.loginWrapper}>
      <a href={GLOBALS.loginURL} target="_blank" rel="noreferrer noopener">Login</a>
    </div>
  </div>
);

export default TabLogin;
