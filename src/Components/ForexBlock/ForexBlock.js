import React, { Component } from 'react';
import { Spin } from 'antd';
import { Link } from 'react-router-dom';

import { API, GLOBALS } from '../../constants';
import { getV2 } from '../../services/generalApi.services';
import Styles from './forexBlockStyles.module.scss';

class ForexBlock extends Component {
  state = {
    items: [],
    loading: true
  };

  componentDidMount() {
    // getV2(API.endPoints.FOREX_DATE_CURRENT)
    getV2('/forexbydate/2020-01-19')
      .then((response) => {
        const items = response.pageContent;
        const currencies = Object.keys(GLOBALS.FOREX_CURRENCIES);
        const filteredItems = items.filter(i => currencies.indexOf(i.currency) > -1);
        this.setState({ items: filteredItems, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch FOREX', e);
      });
  }

  render() {
    const { items, loading } = this.state;

    const forexList = items.map(item => (
      <div className={Styles.block}>
        <div className={Styles.innerBlock}>
          <img src={GLOBALS.FOREX_CURRENCIES[item.currency]} alt=""/>
          <span className={Styles.currency}>{item.currency}</span>
        </div>
        <div className={Styles.innerBlock}>
          <span>Buy: {item.buyingRate}</span>
          <span>Sell: {item.sellingRate}</span>
        </div>
      </div>
    ));

    return (
      <>
        {
          loading
            ? (
              <div className="page-loader __transparent _medium">
                <Spin/>
              </div>
            )
            : (
              <>
                {
                  forexList.length > 0
                    ? (
                      <>
                        <div className={Styles.flexBlock} >
                          {forexList}
                        </div>
                        <Link to="/personal-banking/rates-and-fees/foreign-exchange-rates" className={Styles.button}>View</Link>
                      </>
                    )
                    : (
                      <span>No data available</span>
                    )
                }
              </>
            )
        }
      </>
    );
  }
}

export default ForexBlock;
