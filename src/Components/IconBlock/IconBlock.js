import React from 'react';
import { string } from 'prop-types';

import Styles from './styles.module.scss';

/**
 * @visibleName Icon Block
 */

const IconBlock = ({ icon, title }) => (
  <div className={Styles.iconBlockWrapper}>
    <img src={icon} alt="" />
    <p>
      {title}
    </p>
  </div>
);

IconBlock.propTypes = {
  icon: string.isRequired,
  title: string.isRequired
};


export default IconBlock;
