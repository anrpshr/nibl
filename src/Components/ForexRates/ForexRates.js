import React, { Component } from 'react';
import moment from 'moment';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DownIcon from '@material-ui/icons/ExpandMore';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

import { getV2 } from '../../services/generalApi.services';
import { API } from '../../constants';
import ContentHeader from '../ContentHeader';
import TableBody from '../TableBody';
import css from './ForexRates.module.scss';

class ForexRates extends Component {
  state = {
    loading: true,
    selectedDate: moment(),
    rates: []
  }

  pickerDialogRef = React.createRef();

  componentDidMount() {
    this.getRates();
  }

  getRates = () => {
    const { selectedDate } = this.state;
    const date = selectedDate.format('YYYY-MM-DD');
    getV2(generatePath(API.endPoints.FOREX_DATE, { date }))
      .then((response) => {
        this.setState({ rates: response.pageContent, loading: false });
      })
      .catch();
  }

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  handleDateChange = (date) => {
    this.setState({ selectedDate: date, loading: true }, () => {
      this.getRates();
    });
  };

  render() {
    const { selectedDate, rates, loading } = this.state;

    const dailyRatesRows = rates.map((item, index) => (
      <TableBody key={`rate-row-${item.forexId}`}>
        <div className={css.tableGrid}>
          <span>{index + 1}</span>
          <span className={css.flag}>
            <img src={item.flag} alt=""/>
            {item.currency}
          </span>
          <span className={css.buying}>{item.buyingCash}</span>
          <span className={css.others}>{item.buyingRate}</span>
          <span className={css.selling}>{item.sellingRate}</span>
        </div>
      </TableBody>
    ));

    return (
      <>
        <div className={css.top}>
          <p>Select the date to view the exchange rate for that date</p>
          <div className={css.datePicker}>
            <div className={css.fakeDateInput}>
              <input
                required
                readOnly
                type="text"
                value={selectedDate && moment(selectedDate).format('YYYY-MM-DD')}
                style={{ width: 130 }}
                onClick={this.openPickerDialog}
              />
              <DownIcon color="primary" style={{ fontSize: 16 }}/>
            </div>
          </div>
          <div className="datePickerHide">
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <DatePicker
                ref={this.pickerDialogRef}
                margin="normal"
                label="Date Of Birth"
                value={selectedDate}
                onChange={this.handleDateChange}
                style={{
                  width: '100%'
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
        </div>
        <ContentHeader grey>
          <div className={css.tableGrid}>
            <span>S.N</span>
            <span>Currency</span>
            <span className={css.buying}>Buying Cash</span>
            <span className={css.others}>Buying Rate</span>

            <span className={css.selling}>Selling Rate</span>
          </div>
        </ContentHeader>
        {
          loading
            ? (
              <div className="page-loader __transparent __medium">
                <Spin/>
              </div>
            )
            : (
              <>
                {
                  dailyRatesRows.length > 0
                    ? dailyRatesRows
                    : (
                      <div className={css.empty}>
                        No rates for this date. Please select another date
                      </div>
                    )
                }
              </>
            )
        }
      </>
    );
  }
}

export default ForexRates;
