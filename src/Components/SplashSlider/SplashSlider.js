import React, { Component } from 'react';
import { Spin, Icon } from 'antd';
import Slider from 'react-slick';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../redux';
import { setModalOpen, setModalClose, setSplashClose } from '../../redux/actions/common.actions';

import './SplashSlider.scss';

import slide1 from '../../assets/img/popup/Notice1.jpg';
import slide2 from '../../assets/img/popup/Notice2.jpg';
import slide3 from '../../assets/img/popup/Notice3.jpg';
import slide4 from '../../assets/img/popup/Notice4.png';
import slide5 from '../../assets/img/popup/Notice5.jpg';

const mapStateToProps = ({ common }) => ({ common });

class SplashSlider extends Component {
  state = {
    isLoading: true,
    slides: []
  };

  sliderRef = React.createRef();

  componentDidMount() {
    store.dispatch(setModalOpen());
    this.getSlides();
    document.addEventListener('keydown', this.listenToKeyboard);
  }

  componentWillUnmount() {
    store.dispatch(setModalClose());
    document.removeEventListener('keydown', this.listenToKeyboard);
  }

  listenToKeyboard = (e) => {
    console.log(e);
    if (e.keyCode === 27) this.closeSplash();
    if (e.keyCode === 39) this.sliderRef.slickNext();
    if (e.keyCode === 37) this.sliderRef.slickPrev();
  }

  getSlides = () => {
    this.setState({
      slides: [
        { src: slide1 },
        { src: slide2 },
        { src: slide3 },
        { src: slide4 },
        { src: slide5 }
      ]
    }, () => {
      this.setState({ isLoading: false });
    });
  }

  closeSplash = () => {
    store.dispatch(setSplashClose());
  }

  render() {
    const { isLoading, slides } = this.state;
    const { common } = this.props;
    const loadingIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

    const sliderSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      arrows: false,
      initialSlide: common.splashInitialSlide,
      adaptiveHeight: true
    };

    const sliderList = slides.map(slide => (
      <div className="splash-slide-wrap">
        <img src={slide.src} alt=""/>
      </div>
    ));

    return (
      <>
        {
          slides.length > 0 && (
            <div id="splash-slider">
              {
                isLoading
                  ? <Spin indicator={loadingIcon}/>
                  : (
                    <div className="slider-wrap">
                      <div className="close-slider">
                        <CloseIcon style={{ color: '#fff' }} onClick={this.closeSplash}/>
                      </div>
                      <Slider {...sliderSettings} ref={(c) => { this.sliderRef = c; }}>
                        { sliderList }
                      </Slider>
                    </div>
                  )
              }
            </div>
          )
        }
      </>
    );
  }
}

SplashSlider.propTypes = {
  common: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(SplashSlider);
