import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GoSearch, MdChevronRight } from 'react-icons/all';
import { Link } from 'react-router-dom';
import { ClickAwayListener } from '@material-ui/core';

import store from '../../redux';
import { hideSearchBlock } from '../../redux/actions/common.actions';
import styles from './SearchBlock.module.scss';

const mapStateToProps = ({ common }) => ({ common });

class SearchBlock extends Component {
  state = {
    searchTerm: '',
    popularSearchList: [],
    searchResults: []
  };

  debounceFlag = true;

  allResults = [
    {
      name: 'eBanking',
      url: '/'
    },
    {
      name: 'Credit Cards',
      url: '/'
    },
    {
      name: 'Help',
      url: '/'
    },
    {
      name: 'New Account',
      url: '/'
    },
    {
      name: 'EMI Calculator',
      url: '/'
    },
    {
      name: 'Contact',
      url: '/'
    }
  ];

  componentDidMount() {
    this.setState({
      popularSearchList: [
        {
          name: 'eBanking',
          url: '/'
        },
        {
          name: 'Credit Card',
          url: '/'
        },
        {
          name: 'Careers',
          url: '/'
        },
        {
          name: 'eBanking',
          url: '/'
        },
        {
          name: 'Credit Card',
          url: '/'
        },
        {
          name: 'Careers',
          url: '/'
        }
      ]
    });
  }

  handleSearch = (val) => {
    // if (this.debounceFlag) {
    //   this.setState({ searchTerm: val });
    //   this.debounceFlag = false;
    // }
    // window.setTimeout(() => {
    //   this.debounceFlag = true;
    // }, 300);
    if (val !== '') {
      this.setState({
        searchTerm: val,
        searchResults: this.allResults
      });
    } else {
      this.setState({
        searchTerm: '',
        searchResults: []
      });
    }
  }

  handleClickAway = () => {
    store.dispatch(hideSearchBlock());
  }

  render() {
    const { searchTerm, popularSearchList, searchResults } = this.state;

    const popularList = popularSearchList.map((term, index) => (
      <div key={`pop-search-item-${index}`} className={styles.item}>
        <MdChevronRight/>
        <Link to={term.url}>{term.name}</Link>
      </div>
    ));

    const searchList = searchResults.map((term, index) => (
      <div key={`pop-search-item-${index}`} className={styles.item}>
        <MdChevronRight/>
        <Link to={term.url}>{term.name}</Link>
      </div>
    ));

    return (
      <div className={styles.wrapper}>
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <div className={styles.inner}>
            <div className={styles.bar}>
              <div className={styles.icon}>
                <GoSearch/>
              </div>
              <div className={styles.entry}>
                <input
                  type="text"
                  placeholder="Start typing..."
                  value={searchTerm}
                  onChange={e => this.handleSearch(e.target.value)}
                />
              </div>
            </div>
            <div className={styles.results}>
              {
                searchResults.length > 0 && (
                  <div className={styles.searchResults}>
                    {searchList}
                  </div>
                )
              }
              {
                popularList.length > 0 && (
                  <div className={styles.popular}>
                    <div className={styles.title}>Popular Searches</div>
                    <div className={styles.popularList}>
                      {popularList}
                    </div>
                  </div>
                )
              }
            </div>
          </div>
        </ClickAwayListener>
      </div>
    );
  }
}

export default connect(mapStateToProps)(SearchBlock);
