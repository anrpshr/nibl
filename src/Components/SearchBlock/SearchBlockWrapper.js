import React from 'react';
import { connect } from 'react-redux';
import { objectOf, any } from 'prop-types';
import { CSSTransition } from 'react-transition-group';

import SearchBlock from './SearchBlock';

const mapStateToProps = ({ common }) => ({ common });

const SearchBlockWrapper = ({ common }) => (
  <CSSTransition
    in={common.searchBlockOpen}
    timeout={300}
    classNames="global-fade"
    unmountOnExit
  >
    <SearchBlock/>
  </CSSTransition>
);

SearchBlockWrapper.propTypes = {
  common: objectOf(any).isRequired
};

export default connect(mapStateToProps)(SearchBlockWrapper);
