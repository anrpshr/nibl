import React from 'react';
import Styles from './mapWithWhiteCard.module.scss';

// Images
import map from '../../assets/img/map.jpg';

const MapWithWhiteCard = () => (
  <div className={Styles.flexBlock}>
    <div className={Styles.leftBlock}>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.1808203340884!2d85.3156966149504!3d27.711702831907708!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1903e46cc0d7%3A0x1e4a3450efd18e9b!2sNepal%20Investment%20Bank%20Limited!5e0!3m2!1sen!2snp!4v1575265866984!5m2!1sen!2snp" width="100%" frameborder="0" style={{ border: 0, height: '100%' }} allowfullscreen=""></iframe>
    </div>
    <div className={Styles.rightBlock}>
      <h2>Nepal Investment Bank Ltd.</h2>

      <div className={Styles.element}>
        <span className={Styles.elemTitle}>Address:</span>
        <div className={Styles.elemContent}>
          <span>Durbar Marg, P.O. Box: 3412</span>
          <span>Kathmandu</span>
          <span>Nepal</span>
        </div>
      </div>

      <div className={Styles.element}>
        <span className={Styles.elemTitle}>Email:</span>
        <div className={Styles.elemContent}>
          <span>info@nibl.com.np</span>
        </div>
      </div>
      
      <div className={Styles.element}>
        <span className={Styles.elemTitle}>Telephone:</span>
        <div className={Styles.elemContent}>
          <span>977-1 4228229</span>
        </div>
      </div>

      <div className={Styles.element}>
        <span className={Styles.elemTitle}>Fax:</span>
        <div className={Styles.elemContent}>
          <span>977-1 4226349, 4228927</span>
        </div>
      </div>

    </div>
  </div>
);

export default MapWithWhiteCard;
