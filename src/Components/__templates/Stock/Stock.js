import React, { Component } from 'react';

// Components
import BannerBlock from '../../BannerBlock';
import ContentCard from '../../ContentCard';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';
import stockimg from '../../../assets/img/stock-img.png';
import './styles.scss';

class Stock extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: ''
  }

  componentDidMount() {
    this.setState({
      bannerImg: banner,
      bannerTitle: '',
      bannerDescription: ''
    });
  }

  render() {
    const {
      bannerTitle,
      bannerImg,
      bannerDescription
    } = this.state;

    return (
      <div className="white-gradient">
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
        />
        <div className="stocks-wrapper">
          <div className="container">
            <h1>Stock</h1>
            <ContentCard>
              <p className="red">On Sunday, March 31, 2019</p>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae in odio suscipit recusandae quam, a eos aut minus rem, et quis? Pariatur libero praesentium quae, nihil mollitia quisquam sint asperiores.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae soluta officiis labore eligendi laborum porro officia similique minus iste error impedit, consequuntur neque sapiente blanditiis natus suscipit cumque. Harum, officiis.</p>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque fugit ea minima nesciunt aperiam nam facere corporis et ullam. Non quia quae modi. Porro deleniti sit est, sapiente sequi vitae!</p>
            </ContentCard>
            <div className="stock-chart">
              <img src={stockimg} alt=""/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Stock;
