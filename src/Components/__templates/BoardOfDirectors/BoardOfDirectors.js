import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import BODTRANSFORM from './bod.transformer';
import Styles from './boardOfDirectors.module.scss';

class BoardOfDirectors extends Component {
  state = {
    bannerTitle: '',
    bannerCaption: '',
    bannerAlt: '',
    bannerImage: null,
    activeUserIndex: 0,
    bods: [],
    loading: true
  }

  mainRef = React.createRef();

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_BOD, { pageId }))
      .then((response) => {
        const transformedResponse = new BODTRANSFORM(response);
        this.setState({ ...transformedResponse, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch BOD', e);
      });
  }

  handleActiveuser = (index) => {
    this.setState({
      activeUserIndex: index
    }, () => {
      window.scrollTo({ top: window.scrollY + this.mainRef.getBoundingClientRect().top - 150, behavior: 'smooth' });
    });
  }

  render() {
    const {
      bods,
      activeUserIndex,
      loading,
      bannerTitle,
      bannerCaption,
      bannerAlt,
      bannerImage
    } = this.state;
    const activeUser = bods[activeUserIndex];
    const bodList = bods.map((bod, index) => (
      <div role="button" tabIndex="-1" onClick={() => { this.handleActiveuser(index); }} className={Styles.listElement}>
        <img src={bod.image} alt=""/>
        <div className={Styles.bodListText}>
          <span>{bod.name}</span>
          <span>{bod.designation}</span>
        </div>
      </div>
    ));
    return (
      <div className="white-gradient">
        <BannerBlock
          background={bannerImage}
          title={bannerTitle}
          description={bannerCaption}
          alt={bannerAlt}
        />
        <div className={Styles.innerWrapper}>
          <div className="container" ref={(c) => { this.mainRef = c; }}>
            <div className={Styles.heading}>
              <span>Board Of Directors</span>
            </div>

            {
              loading
                ? (
                  <div className="page-loader __transparent">
                    <Spin/>
                  </div>
                )
                : (
                  <>
                    <div className={Styles.content}>
                      <LeftImageText title={activeUser.name} image={activeUser.image}>
                        <span><b>{activeUser.designation}</b></span>
                        <div dangerouslySetInnerHTML={{ __html: activeUser.description }} />
                        <div className={Styles.socialIcons}>
                          {
                            activeUser.facebook && (
                              <a href={activeUser.facebook} target="_blank" rel="noopener noreferrer">
                                <i className="fab fa-facebook-f"/>
                              </a>
                            )
                          }
                          {
                            activeUser.twitter && (
                              <a href={activeUser.twitter} target="_blank" rel="noopener noreferrer">
                                <i className="fab fa-twitter" />
                              </a>
                            )
                          }
                          {
                            activeUser.linkedin && (
                              <a href={activeUser.linkedin} target="_blank" rel="noopener noreferrer">
                                <i className="fab fa-linkedin"/>
                              </a>
                            )
                          }
                        </div>
                      </LeftImageText>
                    </div>

                    <div className={Styles.list}>
                      {bodList}
                    </div>
                  </>
                )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default BoardOfDirectors;
