import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import Timeline from '../../Timeline';
import TabContent from '../../TabContent';
import FinancialDetails from '../../FinancialDetails';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import Transformer from './transformer';
import Styles from './styles.module.scss';

class Reports extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    pageTitle: '',
    items: [],
    activeItemKey: '',
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_DISCLOSURE, { pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Reports', e);
      });
  }

  render() {
    const {
      bannerTitle,
      bannerImg,
      bannerDescription,
      bannerAlt,
      loading,
      items,
      activeItemKey,
      pageTitle
    } = this.state;

    const financialTable = items.map(item => (
      <FinancialDetails
        key={item.year}
        details={item.details}
        isActive={activeItemKey === item.year}
        title={pageTitle}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient reset">
                  <div className="container">
                    <Timeline
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      items={items}
                      activeItemKey={activeItemKey}
                    />
                    <TabContent>
                      {financialTable}
                    </TabContent>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default Reports;
