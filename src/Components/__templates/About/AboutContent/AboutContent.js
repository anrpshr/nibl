import React from 'react';
import PropTypes from 'prop-types';
import Styles from './aboutcontent.module.scss';


// Components
import LeftImageText from '../../../LeftImageText';
import LeftTextImage from '../../../LeftTextImage';

const AboutContent = ({ overview, mission, leftImage, rightImage }) => (
  <div className={Styles.wrapper}>
    <h1>About NIBL</h1>
    <LeftImageText title="Overview" image={leftImage}>
      <div dangerouslySetInnerHTML={{ __html: overview }}/>
    </LeftImageText>

    <LeftTextImage title="Mission" image={rightImage}>
      <div dangerouslySetInnerHTML={{ __html: mission }} />
    </LeftTextImage>
  </div>
);

AboutContent.propTypes = {
  overview: PropTypes.string.isRequired,
  mission: PropTypes.string.isRequired,
  leftImage: PropTypes.string.isRequired,
  rightImage: PropTypes.string.isRequired
};

export default AboutContent;
