export default class Transformer {
  constructor(APIObject) {
    this.overview = APIObject.pageContent[0].overviewContent;
    this.mission = APIObject.pageContent[0].missionContent;
    this.leftImage = APIObject.pageContent[0].overviewLogo;
    this.rightImage = APIObject.pageContent[0].missionLogo;
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.valuesAndEthicsTitle = 'Our core values tell us, our customers and the communities we serve, who we really are; what we are about; and the principles by which we pledge to conduct business. In essence, we believe that success can only be achieved by living our core values and principles:';
    this.valuesAndEthics = APIObject.pageValueEthics.map(e => ({
      icon: e.icon,
      title: e.title,
      content: e.description
    }));
    this.strategicObjectives = APIObject.pageContent[0].objectiveContent;
  }
}
