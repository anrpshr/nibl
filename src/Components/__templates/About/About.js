import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

// Components
import BannerBlock from '../../BannerBlock';
import AboutContent from './AboutContent';
import ValuesAndEthics from './ValuesAndEthics';
import StrategicObjectives from './StrategicObjectives';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import Transformer from './transformer';

import Styles from './about.module.scss';

class About extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerCaption: '',
    bannerAlt: '',
    overview: '',
    mission: '',
    leftImage: '',
    rightImage: '',
    valuesAndEthicsTitle: '',
    valuesAndEthics: [],
    strategicObjectives: '',
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_OVERVIEW, { pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch content', e);
      });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerCaption,
      overview,
      mission,
      leftImage,
      rightImage,
      valuesAndEthicsTitle,
      valuesAndEthics,
      strategicObjectives,
      bannerAlt,
      loading
    } = this.state;

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerCaption}
                  alt={bannerAlt}
                />

                <div className="white-gradient">
                  <div className={Styles.greyBack}>
                    <div className="container">
                      <AboutContent
                        overview={overview}
                        mission={mission}
                        leftImage={leftImage}
                        rightImage={rightImage}
                      />
                    </div>
                  </div>

                  <div className={Styles.valuesEthicWrapper}>
                    <div className="container">
                      <ValuesAndEthics
                        title="Values and Ethics"
                        excerpt={valuesAndEthicsTitle}
                        valuesAndEthics={valuesAndEthics}
                      />
                    </div>
                  </div>
                </div>

                <StrategicObjectives>
                  <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: strategicObjectives }}/>
                </StrategicObjectives>
              </>
            )
        }
      </div>
    );
  }
}

export default About;
