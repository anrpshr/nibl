import React from 'react';
import PropTypes from 'prop-types';
import Styles from './valuesandethics.module.scss';

// Components
import PlainBlock from '../../../PlainBLock';

const ValuesAndEthics = ({ title, excerpt, valuesAndEthics }) => {
  const valuesAndEthicsList = valuesAndEthics.map((item, index) => (
    <PlainBlock
      key={`block-${index}`}
      icon={item.icon}
      title={item.title}
      description={item.content}
      htmlDescription
    />
  ));

  return (
    <div className={Styles.wrapper}>
      <div className={Styles.headings}>
        <h1>{title}</h1>
        <p>
          {excerpt}
        </p>
      </div>
      <div className={Styles.innerContents}>
        {valuesAndEthicsList}
      </div>
    </div>
  );
};

ValuesAndEthics.propTypes = {
  title: PropTypes.string.isRequired,
  excerpt: PropTypes.string.isRequired,
  valuesAndEthics: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default ValuesAndEthics;
