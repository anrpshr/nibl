import React from 'react';

import strategicBg from '../../../../assets/img/objectives-back.jpg';
import Styles from './strategicobjectives.module.scss';

const StrategicObjectives = ({ children }) => (
  <div className={Styles.wrapper} style={{ background: `url(${strategicBg})` }}>
    <div className="container">
      <div className={Styles.outer}>
        <div className={Styles.contentWrapper}>
          <div className={Styles.heading}>
            <h1>Strategic Objectives</h1>
          </div>
          <div className={Styles.contentList}>
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default StrategicObjectives;
