import React, { Component } from 'react';
import moment from 'moment';
import { generatePath } from 'react-router';
import { Spin } from 'antd';
import { withRouter } from 'react-router-dom';
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  EmailShareButton,
  EmailIcon
} from 'react-share';

import CardImageAndText from '../../CardImageAndText';
import Styles from './detailPage.module.scss';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import Transformer from './transformer';

class DetailPage extends Component {
  state = {
    post: {},
    loading: true,
    news: []
  };

  limit = 3;

  componentDidMount() {
    this.getPost();
    this.getNews();
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  componentDidUpdate(prevProps) {
    const { match } = this.props;
    if (match.params.id !== prevProps.match.params.id) {
      this.setState({ //eslint-disable-line
        loading: true
      }, () => {
        window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
        this.getPost();
        this.getNews();
      });
    }
  }

  getPost = () => {
    const { match } = this.props;
    getV2(generatePath(API.endPoints.NEWS_SINGLE, { pageId: match.params.id }))
      .then((content) => {
        const post = new Transformer(content);
        this.setState({ post, loading: false });
      })
      .catch((e) => {
        console.log('Error trying to fetch post', e);
      });
  }

  getNews = () => {
    getV2(generatePath(API.endPoints.NEWS_FEATURED, { limit: this.limit }))
      .then((response) => {
        this.setState({
          news: response.pageContent.map(e => ({
            id: e.contentId,
            title: e.newsTitle,
            excerpt: e.newsDescription,
            content: e.newsContent,
            image: e.newsBanner
          }))
        });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Featured News', e);
      });
  }

  render() {
    const { post, news, loading } = this.state;
    const { title, content, date } = post;

    news.splice(3, news.length);
    const newsList = news.map((item, i) => (
      <div key={`more-news-${i}`} className={Styles.card}>
        <CardImageAndText
          image={item.NewsBanner}
          title={item.NewsTitle}
          excerpt={item.NewsDescription}
          link={`/news-content/${item.Id}`}
        />
      </div>
    ));

    return (
      <div className={Styles.wrapper}>
        <div className="white-gradient main--content">
          {
            loading
              ? (
                <div className="page-loader __transparent">
                  <Spin/>
                </div>
              )
              : (
                <>
                  <div className="container">
                    <div className="heading">
                      <h1>{title}</h1>
                    </div>
                    <div className={Styles.content}>
                      <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: content }}/>
                      <div className={Styles.social}>
                        <FacebookShareButton
                          url={window.location}
                        >
                          <FacebookIcon size={32} round/>
                        </FacebookShareButton>
                        <TwitterShareButton url={window.location}>
                          <TwitterIcon size={32} round/>
                        </TwitterShareButton>
                        <EmailShareButton url={window.location}>
                          <EmailIcon size={32} round/>
                        </EmailShareButton>
                      </div>
                    </div>
                  </div>
                  {
                    news.length > 0 && (
                      <div className={Styles.moreNews}>
                        <div className="container">
                          <div className="heading">
                            <h1>Continue reading</h1>
                          </div>
                          <div className={Styles.cardWrapper}>
                            {newsList}
                          </div>
                        </div>
                      </div>
                    )
                  }
                </>
              )
          }
        </div>

      </div>
    );
  }
}

export default withRouter(DetailPage);
