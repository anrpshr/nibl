export default class Transformer {
  constructor(APIObject) {
    this.id = APIObject.pageContent[0].contentId;
    this.title = APIObject.pageContent[0].newsTitle;
    this.exerpt = APIObject.pageContent[0].newsTitle;
    this.content = APIObject.pageContent[0].newsContent;
    this.image = APIObject.pageContent[0].newsBanner;
    this.date = APIObject.pageContent[0].newsDate;
  }
}
