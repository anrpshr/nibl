import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';
import { connect } from 'react-redux';

import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import DepositsTable from './DepositsTable';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Transformer from './rates.transformer';

import Styles from './interestRates.module.scss';

const mapStateToProps = ({ currentpage, navigation }) => ({ currentpage, navigation });

class InterestRates extends Component {
  state = {
    loading: true,
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    rates: [],
    items: [],
    activeItemKey: ''
  }

  componentDidMount() {
    const { currentpage, navigation, pageId } = this.props;
    const topLevelPage = [...navigation.primary, ...navigation.secondary].find(r => r.slug === currentpage.slug);
    const topLevelId = topLevelPage.menuId;

    getV2(generatePath(API.endPoints.RATES, { topLevelId, pageId }))
      .then((content) => {
        const t = new Transformer(content);
        this.setState({
          ...t,
          loading: false,
          rates: [
            {
              title: 'Base Rate',
              perAnnum: 8.54
            },
            {
              title: 'Interest Spread LCY (As per NRB Directives)',
              perAnnum: 9.23
            }
          ]
        });
      })
      .catch();
  }

  render() {
    const {
      loading,
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      rates,
      items,
      activeItemKey
    } = this.state;

    const baseRates = rates.map((ratesItem, index) => (
      <TableBody key={`baseRates-${index}`}>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>{index + 1}</span>
          <span className={Styles.saving}>{ratesItem.title}</span>
          <span className={Styles.annum}>{ratesItem.perAnnum}</span>
        </div>
      </TableBody>
    ));

    const depositInterestRates = items.map((item, index) => (
      <DepositsTable
        key={`rates-${index}`}
        details={item.details}
        isActive={activeItemKey === item.key}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader __transparent">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <div className="heading">
                      <h1>Base Rates</h1>
                    </div>
                    <ContentHeader grey>
                      <div className={Styles.gridWrapper}>
                        <span className={Styles.sn}>S.N</span>
                        <span className={Styles.saving}>Saving Accounts</span>
                        <span className={Styles.annum}>% per annum</span>
                      </div>
                    </ContentHeader>
                    {baseRates}
                  </div>
                </div>

                <div className="main--content">
                  <div className="container">
                    <div className={Styles.noteText}>
                      <span>Effective from 28th April, 2019 (15 Baisakh, 2076)</span>
                    </div>

                    <TabNav
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      activeItemKey={activeItemKey}
                      items={items}
                    />
                    <div className={Styles.tabContent}>
                      <TabContent>
                        {depositInterestRates}
                      </TabContent>
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default connect(mapStateToProps)(InterestRates);
