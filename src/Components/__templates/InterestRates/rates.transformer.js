import { chain, zipObject } from 'lodash';
import slugs from 'slugs';

export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    const allItems = chain(APIObject.pageContent)
      .groupBy('contentCategory')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.items = allItems.filter(item => item.key !== 'cards');
    this.activeItemKey = this.items[0].key;
  }
}
