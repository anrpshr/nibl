import React from 'react';
import styles from './calculatorOverBg.module.scss';

import EmiCaclBlock from '../../EmiCalcBlock';

const CalculatorOverBg = () => (
  <div className={styles.wrapper}>
    <div className="white-gradient main--content">
      <div className={styles.inner}>
        <div className={styles.left}>
          <h2>Contact us for any queries or complaints</h2>
        </div>
        <div className={styles.right}>
          <h1 className="heading">EMI CALCULATOR</h1>
          <div className={styles.calculator}>
            <EmiCaclBlock/>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default CalculatorOverBg;
