import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styles from './services.module.scss';

// Components
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
import Card from '../../../Layouts/Cards';

const Services = ({ title, services }) => {
  const nonFeaturedBlock = services.filter(service => !service.isFeatured);
  const nonFeaturedBlockList = nonFeaturedBlock.map((service) => {
    if (!service.image) {
      return (
        <Card
          key={service.id}
          title={service.title}
          content={service.excerpt}
          image={service.icon}
          color={service.backgroundColor}
        />
      );
    } return (
      <ImageBlockWithOverlay
        key={service.id}
        title={service.title}
        image={service.image}
      >
        <p>
          {service.excerpt}
          <a href="1"> more</a>
        </p>
      </ImageBlockWithOverlay>
    );
  });

  const featuredBLock = services.filter(service => service.isFeatured);
  const featuredBLockList = featuredBLock.map(service => (
    <ImageBlockWithOverlay
      key={service.id}
      title={service.title}
      image={service.image.url}
    >
      <p>
        {service.excerpt}
        <a href="1"> more</a>
      </p>
    </ImageBlockWithOverlay>
  ));

  return (
    <div className={Styles.wrapper}>
      <div className="heading">
        <h1>{title}</h1>
      </div>
      <div className={Styles.bigBlock}>
        <div className={Styles.leftBlock}>
          {nonFeaturedBlockList}
        </div>
        <div className={Styles.rightBlock}>
          {featuredBLockList}
        </div>
      </div>
    </div>
  );
};

Services.propTypes = {
  title: PropTypes.string.isRequired,
  services: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Services;
