import React from 'react';
import PropTypes from 'prop-types';

// Import
import ExpansionPanel from '../../../ExpansionPanel';

const Faq = ({ faq }) => {
  const faqList = faq.map(faqItem => (
    {
      primary:
      (
        <div>
          {faqItem.primary}
        </div>
      ),
      secondary: <div>{faqItem.secondary}</div>
    }
  ));

  return (
    <>
      <div className="heading">
        <h1>Faq</h1>
      </div>
      <ExpansionPanel
        isOrdered
        items={faqList}
      />
    </>
  );
};

Faq.propTypes = {
  faq: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Faq;
