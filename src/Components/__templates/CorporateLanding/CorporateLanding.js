import React, { Component } from 'react';
import handleViewport from 'react-in-viewport';
import Styles from './corporatelanding.module.scss';

import BannerBlock from '../../BannerBlock';
import Forex from '../../Forex';
import SavingCalculator from '../../SavingCalculator';
import Services from './Services';
import Faq from './Faq';
import ForexStockEmiSection from '../../__pages/Home/ForexStockEmiSection';
import FeaturedBanner from '../../__pages/Home/FeaturedBanner';

import banner from '../../../assets/img/corporate-bg.jpg';
import dummyImage from '../../../assets/img/dummyimg.png';
import businessLoan from '../../../assets/img/business-loan.png';
import rsIcon from '../../../assets/img/bill.png';
import businessService from '../../../assets/img/branchless.png';
import verticalImg from '../../../assets/img/dummygirl.png';

class CorporateLanding extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    services: [],
    faq: []
  }

  componentDidMount() {
    this.setState({
      bannerImg: banner,
      bannerTitle: 'Corporate Banking',
      bannerDescription: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      services: [
        {
          isFeatured: false,
          title: 'Business Account',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'business-loan',
          id: 1,
          image: dummyImage,
          icon: null,
          backgroundColor: null
        },
        {
          isFeatured: false,
          title: 'Business Loan',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'business-loan',
          id: 2,
          image: null,
          icon: businessLoan,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Insurance and Risk Management',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'insurance-and-risk-management',
          id: 3,
          image: null,
          icon: rsIcon,
          backgroundColor: 'red'
        },
        {
          isFeatured: false,
          title: 'Business Services',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'business-services',
          id: 3,
          image: null,
          icon: businessService,
          backgroundColor: 'blue'
        },
        {
          isFeatured: true,
          title: 'Investment',
          excerpt: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.',
          slug: 'business-services',
          id: 3,
          image: {
            url: verticalImg,
            width: 393,
            height: 636
          },
          icon: rsIcon,
          backgroundColor: 'blue'
        }
      ],
      faq: [
        {
          primary: 'Lorem ipsum dolor sit amet?',
          secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.'
        },
        {
          primary: 'Lorem ipsum dolor sit amet!',
          secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa Cum sociis natoque penatibus et magnis dis parturient montes.'
        }
      ]
    });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      services,
      faq
    } = this.state;

    return (
      <>
        <BannerBlock
          background={bannerImg}
          title={bannerTitle}
          description={bannerDescription}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <Services services={services} title="Services"/>
            <ForexStockEmiSection/>
          </div>
        </div>
        <FeaturedBanner/>
        <div className="white-gradient main--content">
          <div className={Styles.faqWrapper}>
            <div className="container">
              <Faq faq={faq}/>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default handleViewport(CorporateLanding);
