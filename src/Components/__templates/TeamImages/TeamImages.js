import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import BODTRANSFORM from './team.transformer';
import Styles from './teamImages.module.scss';

class TeamImages extends Component {
  state = {
    bannerTitle: '',
    bannerCaption: '',
    bannerAlt: '',
    bannerImage: null,
    bods: [],
    loading: true
  }

  mainRef = React.createRef();

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_BOD, { pageId }))
      .then((response) => {
        const transformedResponse = new BODTRANSFORM(response);
        this.setState({ ...transformedResponse, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch BOD', e);
      });
  }

  render() {
    const {
      bods,
      loading,
      bannerTitle,
      bannerCaption,
      bannerAlt,
      bannerImage
    } = this.state;
    const bodList = bods.map((bod, index) => (
      <div role="button" tabIndex="-1" onClick={() => { this.handleActiveuser(index); }} className={Styles.listElement}>
        <img src={bod.image} alt=""/>
        <div className={Styles.bodListText}>
          <span>{bod.name}</span>
          <span>{bod.designation}</span>
        </div>
      </div>
    ));
    return (
      <div className="white-gradient">
        <BannerBlock
          background={bannerImage}
          title={bannerTitle}
          description={bannerCaption}
          alt={bannerAlt}
        />
        <div className={Styles.innerWrapper}>
          <div className="container" ref={(c) => { this.mainRef = c; }}>
            <div className={Styles.heading}>
              <span>Board Of Directors</span>
            </div>

            {
              loading
                ? (
                  <div className="page-loader __transparent">
                    <Spin/>
                  </div>
                )
                : (
                  <div className={Styles.list}>
                    {bodList}
                  </div>
                )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default TeamImages;
