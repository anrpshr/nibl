import React, { Component } from 'react';
import handleViewport from 'react-in-viewport';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import FeaturesAndBenefits from '../../FeaturesAndBenefits';
import Strip from '../../Strip';
import GeneralBlocks from '../../generalBlocks';
// import SliderWithVideo from '../../SliderWithVideo';
import HowToApplyBlock from '../../HowToApply';
import { getProduct } from '../../../services/product.services';
import Loading from '../../Loading';
import TabNav from '../../TabNav';
import ProductTransformer from './product.transformer';
import Styles from './overview.module.scss';

class Overview extends Component {
  state = {
    loading: true,
    bannerImg: null,
    bannerTitle: '',
    bannerDesc: '',
    bannerLink: '',
    productTitle: '',
    productImage: null,
    productImageAlt: null,
    productOverviewExcerpt: '',
    productDetails: '',
    firstTableTitle: '',
    firstTableContent: '',
    secondTableTitle: '',
    secondTableContent: '',
    blocks: [],
    featuredBlockImage: null,
    featuredImageAlt: null,
    featuredBlockImageCaption: null,
    featuredBlockImageTitle: null,
    featuredBlockImageLink: null,
    benefitsBlockImage: null,
    benefitsImageAlt: null,
    benefitsBlockImageCaption: null,
    benefitsBlockImageTitle: null,
    benefitsBlockImageLink: null,
    questions: [],
    accounts: [],
    activeTab: 'details'
  }

  detailsRef = React.createRef();

  fnbRef = React.createRef();

  blocksRef = React.createRef();

  componentDidMount() {
    const { pageId } = this.props;
    getProduct(pageId)
      .then((content) => {
        // transform and setState
        const formattedContent = new ProductTransformer(content);
        this.setState({
          loading: false,
          ...formattedContent
        });
      })
      .catch((e) => {
        console.log('Error while trying to get Product', e);
        this.setState({
          loading: false
        });
      });
  }

  handleTabClick = (key) => {
    this.setState({ activeTab: key });
    if (key === 'details') {
      window.scrollTo({ top: window.scrollY + this.detailsRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    } else if (key === 'features') {
      window.scrollTo({ top: window.scrollY + this.fnbRef.featuresRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    } else if (key === 'benefits') {
      window.scrollTo({ top: window.scrollY + this.fnbRef.benefitsRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    } else if (key === 'how-to-apply') {
      window.scrollTo({ top: window.scrollY + this.blocksRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    }
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDesc,
      bannerLink,
      productTitle,
      productImage,
      productImageAlt,
      productOverviewExcerpt,
      productDetails,
      firstTableTitle,
      firstTableContent,
      secondTableTitle,
      secondTableContent,
      blocks,
      loading,
      featuredBlockImage,
      featuredImageAlt,
      featuredBlockImageCaption,
      featuredBlockImageTitle,
      featuredBlockImageLink,
      benefitsImageAlt,
      benefitsBlockImage,
      benefitsBlockImageCaption,
      benefitsBlockImageTitle,
      benefitsBlockImageLink,
      questions,
      activeTab
    } = this.state;

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDesc}
                  link={bannerLink}
                  linkTitle="Apply Now"
                />
                <div className="white-gradient">
                  {
                    activeTab && (
                      <div className={Styles.tabs}>
                        <TabNav
                          items={[
                            {
                              name: 'Details',
                              key: 'details'
                            },
                            {
                              name: 'Features',
                              key: 'features'
                            },
                            {
                              name: 'Benefits',
                              key: 'benefits'
                            },
                            {
                              name: 'How To Apply',
                              key: 'how-to-apply'
                            }
                          ]}
                          activeItemKey={activeTab}
                          onClick={this.handleTabClick}
                        />
                      </div>
                    )
                  }
                  <div className={Styles.greyInner}>
                    <div className="container">
                      <div className={Styles.heading} ref={(c) => { this.detailsRef = c; }}>
                        <span>Product Overview</span>
                      </div>
                      <div className={Styles.contentWrapper}>
                        <LeftImageText
                          redTitle
                          title={productTitle}
                          excerpt={productOverviewExcerpt}
                          image={productImage}
                          imageHeight={400}
                          imageAlt={productImageAlt}
                        >
                          <div dangerouslySetInnerHTML={{ __html: productDetails }}/>
                          {/* <ButtonRedBorder title="Apply Now"/> */}
                        </LeftImageText>
                        <FeaturesAndBenefits
                          ref={(c) => { this.fnbRef = c; }}
                          firstTableTitle={firstTableTitle}
                          secondTableTitle={secondTableTitle}
                          firstTableContent={firstTableContent}
                          secondTableContent={secondTableContent}
                          features={firstTableContent}
                          benefits={secondTableContent}
                          featuredBlockImage={featuredBlockImage}
                          featuredImageAlt={featuredImageAlt}
                          featuredBlockImageCaption={featuredBlockImageCaption}
                          featuredBlockImageTitle={featuredBlockImageTitle}
                          featuredBlockImageLink={featuredBlockImageLink}
                          benefitsBlockImage={benefitsBlockImage}
                          benefitsImageAlt={benefitsImageAlt}
                          benefitsBlockImageCaption={benefitsBlockImageCaption}
                          benefitsBlockImageTitle={benefitsBlockImageTitle}
                          benefitsBlockImageLink={benefitsBlockImageLink}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className={Styles.searchSectionWrapper}>
                  <Strip text="Subscribe to get our new offer"/>
                </div>
                <div className="white-gradient main--content">
                  <div className="container" ref={(c) => { this.blocksRef = c; }}>
                    <HowToApplyBlock
                      questions={questions}
                    />
                    <GeneralBlocks blocks={blocks}/>
                    {/* // TODO - FIX */}
                    {/* <SliderWithVideo/> */}
                    {/* <div className={Styles.moreAcc}>
                      <div className="heading">
                        <h1>More Personal Accounts</h1>
                      </div>
                      <div className={Styles.gridBlock}>
                        <div className={Styles.block}>
                          <Card
                            title="Lotus Account"
                            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                            image={icon}
                          />
                        </div>
                        <div className={Styles.block}>
                          <Card
                            title="Afnai Bachat Khata"
                            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                            image={icon}
                          />
                        </div>
                        <div className={Styles.block}>
                          <Card
                            title="Saving Account"
                            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                            image={icon}
                          />
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default handleViewport(Overview);
