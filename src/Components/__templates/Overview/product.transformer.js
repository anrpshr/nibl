export default class ProductTransformer {
  constructor(ProductApiObject) {
    this.bannerImg = ProductApiObject[0].bannerDetails.bannerImage;
    this.bannerTitle = ProductApiObject[0].bannerDetails.bannerTitle || ProductApiObject[0].bannerDetails.bannerAlt;
    this.bannerDesc = ProductApiObject[0].bannerDetails.bannerCaption;
    this.bannerLink = null;
    this.productTitle = ProductApiObject[0].pageDetails.pageTitle;
    this.productImage = ProductApiObject[0].overviewContent.productImage;
    this.productImageAlt = ProductApiObject[0].overviewContent.productImageAlt;
    this.productOverviewExcerpt = ProductApiObject[0].overviewContent.productOverview;
    this.productDetails = ProductApiObject[0].overviewContent.productDetails;
    this.firstTableTitle = 'Features';
    this.firstTableContent = ProductApiObject[0].featureContent.productFeatures;
    this.featuredBlockImage = ProductApiObject[0].featureContent.featureImage;
    this.featuredBlockImageCaption = ProductApiObject[0].featureContent.featureImageDescription;
    this.featuredBlockImageTitle = ProductApiObject[0].featureContent.featureImageTitle;
    this.featuredBlockImageLink = ProductApiObject[0].featureContent.featureImageDetailLink;
    this.featuredImageAlt = ProductApiObject[0].featureContent.featureImageAlt;
    this.secondTableTitle = 'Benefits';
    this.secondTableContent = ProductApiObject[0].benefitContent.productBenefits;
    this.benefitsBlockImage = ProductApiObject[0].benefitContent.benefitImage;
    this.benefitsBlockImageCaption = ProductApiObject[0].benefitContent.benefitImageDescription;
    this.benefitsBlockImageTitle = ProductApiObject[0].benefitContent.benefitImageTitle;
    this.benefitsBlockImageLink = ProductApiObject[0].benefitContent.benefitImageDetailLink;
    this.benefitsImageAlt = ProductApiObject[0].benefitContent.benefitImageAlt;
    this.questions = [
      {
        question: ProductApiObject[0].faqContent.howToApplyQstn1,
        answer: ProductApiObject[0].faqContent.howToApplyAns1,
        link: ProductApiObject[0].faqContent.pageUploadedFile1
      },
      {
        question: ProductApiObject[0].faqContent.howToApplyQstn2,
        answer: ProductApiObject[0].faqContent.howToApplyAns2,
        link: ProductApiObject[0].faqContent.pageUploadedFile2
      }
    ];
    this.blocks = [
      {
        title: 'Tools',
        description: ProductApiObject[0].otherContent.toolsDescription,
        link: ProductApiObject[0].otherContent.toolsLink
      },
      {
        title: 'Downloads',
        description: ProductApiObject[0].otherContent.downloadsDescription,
        link: ProductApiObject[0].otherContent.downloadsLink
      },
      {
        title: 'Interest Rates',
        description: ProductApiObject[0].otherContent.interestRateDescription,
        link: ProductApiObject[0].otherContent.interestRateLink
      }
    ];
  }
}
