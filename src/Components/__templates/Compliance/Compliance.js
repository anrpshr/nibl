import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import LeftTextImage from '../../LeftTextImage';
import COMPLIANCETRANSFORM from './compliance.transformer';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Styles from './compliance.module.scss';

class Aml extends Component {
  state = {
    loading: true,
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    pageTitle: '',
    posts: []
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_COMPLIANCE, { pageId }))
      .then((content) => {
        const transformedContent = new COMPLIANCETRANSFORM(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Compliance content', e);
      });
  }

  render() {
    const {
      loading,
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      pageTitle,
      posts
    } = this.state;

    const postsList = posts.map((post, index) => {
      if (index % 2 === 0) {
        return (
          <LeftImageText key={`post ${index}`} title={post.title} image={post.image}>
            <>
              <div dangerouslySetInnerHTML={{ __html: post.description }}/>
              <div className={Styles.btn}>
                <a className="button-link" href={post.btnLink} download>{post.btnText}</a>
              </div>
            </>
          </LeftImageText>
        );
      }
      return (
        <LeftTextImage key={`post ${index}`} title={post.title} image={post.image}>
          <div dangerouslySetInnerHTML={{ __html: post.description }}/>
          <div className={Styles.btn}>
            <a className="button-link" href={post.btnLink} download>{post.btnText}</a>
          </div>
        </LeftTextImage>
      );
    });

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  background={bannerImg}
                  description={bannerDescription}
                  alt={bannerAlt}
                />
                <div className="white-gradient">
                  <div className={Styles.contentWrapper}>
                    <div className="container">
                      <div className={Styles.heading}>
                        <h1>{pageTitle}</h1>
                      </div>
                      {postsList}
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default Aml;
