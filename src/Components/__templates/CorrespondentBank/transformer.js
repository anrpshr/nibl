export default class Transformer {
  constructor(APIObject) {
    this.pageTitle = APIObject.pageDetails.pageTitle;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.details = APIObject.pageContent.map(e => ({
      currency: e.bankAlias,
      flag: e.flagImage,
      bank: e.bankName,
      swiftAddress: e.swiftAddress
    }));
  }
}
