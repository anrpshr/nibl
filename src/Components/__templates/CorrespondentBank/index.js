import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import Transformer from './transformer';
import Styles from './styles.module.scss';

class CorrespondentBank extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    pageTitle: '',
    details: [],
    loading: true
  };

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_CORRESPONDENT, { pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Correspondent Banks Data', e);
      });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      pageTitle,
      details,
      loading
    } = this.state;

    const correspondentBankTbl = details.map((item, index) => (
      <TableBody>
        <div className={Styles.tableHeadTitles}>
          <span className={Styles.sn}>{index + 1}</span>
          <span className={Styles.currency}>
            <img className={Styles.flag} src={item.flag} alt=""/>
            {item.currency}
          </span>
          <span className={Styles.bank}>{item.bank}</span>
          <span className={Styles.address}>{item.swiftAddress}</span>
        </div>
      </TableBody>
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDescription}
                  alt={bannerAlt}
                />
                <div className="white-gradient">
                  <div className={Styles.innerWrapper}>
                    <div className="container">
                      <div className="heading">
                        <h1>{pageTitle}</h1>
                      </div>
                      <ContentHeader grey>
                        <div className={Styles.tableHeadTitles}>
                          <span className={Styles.sn}>SN</span>
                          <span className={Styles.currency}>Currency</span>
                          <span className={Styles.bank}>Bank</span>
                          <span className={Styles.address}>Swift Address</span>
                        </div>
                      </ContentHeader>
                      {correspondentBankTbl}
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default CorrespondentBank;
