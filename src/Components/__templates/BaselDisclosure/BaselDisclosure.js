import React, { Component } from 'react';
import Styles from './baselDisclosure.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import Timeline from '../../Timeline';
import TabContent from '../../TabContent';

import FinancialDetails from '../../FinancialDetails';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';

class BaselDisclosure extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    items: [],
    activeItemKey: ''
  }

  componentDidMount() {
    this.setState({
      bannerImg: banner,
      bannerTitle: '',
      bannerDescription: '',
      items: [
        {
          year: 2012,
          details: [
            {
              primary: 'Download NIBL Mobile (SMS) Banking Application 2012?',
              secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
              downloadLink: '#'
            },
            {
              primary: 'Download NIBL Mobile (SMS) Banking Application?',
              secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
              downloadLink: '#'
            },
            {
              primary: 'Download NIBL Mobile (SMS) Banking Application?',
              secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
              downloadLink: '#'
            },
            {
              primary: 'Download NIBL Mobile (SMS) Banking Application?',
              secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
              downloadLink: '#'
            }
          ]
        },
        {
          year: 2013,
          details: [
            {
              primary: 'Download NIBL Mobile (SMS) Banking Application 2013?',
              secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
              downloadLink: '#'
            },
            {
              primary: 'Download NIBL Mobile (SMS) Banking Application?',
              secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
              downloadLink: '#'
            }
          ]
        },
        {
          year: 2014
        },
        {
          year: 2015
        }
      ],
      activeItemKey: 2012
    });
  }

  render() {
    const {
      bannerTitle,
      bannerImg,
      bannerDescription,
      items,
      activeItemKey
    } = this.state;

    const financialTable = items.map(item => (
      <FinancialDetails
        key={item.year}
        details={item.details}
        isActive={activeItemKey === item.year}
        title="BASEL DISCLOSURE"
      />
    ));

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
        />
        <div className="white-gradient reset">
          <div className="container">
            <Timeline
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              items={items}
              activeItemKey={activeItemKey}
            />
            <TabContent>
              {financialTable}
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default BaselDisclosure;
