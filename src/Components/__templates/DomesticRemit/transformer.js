export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.title = APIObject.pageDetails.pageTitle;
    this.domesticOverview = APIObject.pageContent[0].overview;
    this.domesticImage = APIObject.pageContent[0].overviewLogo;
    this.benefits = APIObject.pageContent[0].benefitsContent;
  }
}
