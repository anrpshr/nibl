import React, { Component } from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';
import { Row, Col, Spin } from 'antd';
import { generatePath } from 'react-router';

// Components
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import ButtonRedBorder from '../../ButtonRedBorder';
import BigTitleBlock from '../../Layouts/Block/BigTitleBlock';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
// Iamges
import background from '../../../assets/img/compliance-bg.jpg';
import bottomBg from '../../../assets/img/objectives-back.jpg';
import niblImg from '../../../assets/img/nibl-dummy.jpg';
import Styles from './domesticRemit.module.scss';
import Transformer from './transformer';

class DomesticRemit extends Component {
  state = {
    bannerImg: null,
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    title: '',
    domesticOverview: '',
    benefits: '',
    domesticImage: null,
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.REMITTANCE_DOMESTIC, { pageId }))
      .then((content) => {
        const t = new Transformer(content);
        this.setState({ ...t, loading: false });
      })
      .catch();
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      title,
      domesticOverview,
      benefits,
      domesticImage,
      loading
    } = this.state;

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDescription}
                  alt={bannerAlt}
                />
                <div className={Styles.greyInner}>
                  <div className="white-gradient main--content">
                    <div className="container">
                      <div className={Styles.heading}>
                        <h1>{title}</h1>
                      </div>
                      <LeftImageText image={domesticImage} title={title}>
                        <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: domesticOverview }}/>
                      </LeftImageText>

                      <div className={Styles.heading}>
                        <h1>NIBL Domestic Remitance</h1>
                      </div>
                      <div className={Styles.flexSection}>
                        <Row gutter={24} type="flex">
                          <Col span={12}>
                            <BigTitleBlock title="Sender Details">
                              <form action="submit">
                                <div className={Styles.formElements}>
                                  <label>Name</label>
                                  <input type="text" name="name"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Sender Address:</label>
                                  <input type="text" name="address"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Sender Mobile No:</label>
                                  <input type="text" name="contact"/>
                                </div>
                                <div className={Styles.buttons}>
                                  <ButtonRedBorder title="Send"/>
                                  <ButtonRedBorder title="Clear"/>
                                </div>
                              </form>
                            </BigTitleBlock>
                          </Col>
                          <Col span={12}>
                            <BigTitleBlock title="Receiver Details">
                              <form action="submit">
                                <div className={Styles.formElements}>
                                  <label>Receiver Name</label>
                                  <input type="text" name="rname"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Receiver Address:</label>
                                  <input type="text" name="raddress"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Sender Mobile No:</label>
                                  <input type="text" name="rcontact"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>ID No:</label>
                                  <input type="text" name="rid"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Amount to Send:</label>
                                  <input type="text" name="ramount"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Charge Amount NPR:</label>
                                  <input type="text" name="charge_amount"/>
                                </div>
                                <div className={Styles.formElements}>
                                  <label>Total Amount NPR:</label>
                                  <input type="text" name="rcontact"/>
                                </div>
                                <div className={Styles.buttons}>
                                  <ButtonRedBorder title="Send"/>
                                  <ButtonRedBorder title="Clear"/>
                                </div>
                              </form>
                            </BigTitleBlock>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </div>
                  <div className={Styles.bottomSection} style={{ background: `url(${bottomBg})` }}>
                    <div className="container">
                      <div className={Styles.bottomFlex}>
                        <div className={Styles.benifits}>
                          <h1>Benefits</h1>
                          <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: benefits }}/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default DomesticRemit;
