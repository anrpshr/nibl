import React, { Component } from 'react';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

import Locator from '../../Locator';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import ATMTransformer from './atm.transformer';
import Styles from './atmLocation.module.scss';

class AtmLocation extends Component {
  state = {
    locations: [],
    isLoading: true
  }

  componentDidMount() {
    const { pageId, hasPageContent } = this.props;
    getV2(generatePath(API.endPoints.ATM, { hasPageContent, pageId }))
      .then((locationContent) => {
        const transformedLocations = locationContent.pageContent.map(location => new ATMTransformer(location));
        this.setState({ locations: transformedLocations, isLoading: false });
      })
      .catch((e) => {
        console.log('Error occurred while fetching ATM locations', e);
      });
  }

  render() {
    const { locations, isLoading } = this.state;

    return (
      <div className={Styles.wrapper}>
        {
          isLoading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                {
                  locations.length > 0
                    ? <Locator title="Click here to see the nearest ATM location" locations={locations}/>
                    : (
                      <div className="page-loader">
                        <span>No data available</span>
                      </div>
                    )
                }
              </>
            )
        }
      </div>
    );
  }
}

export default AtmLocation;
