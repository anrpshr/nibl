export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.certification = APIObject.pageContent.map(e => ({
      primary: e.certificateTitle,
      secondary: e.certificateDescription,
      downloadLink: e.uploadedFile
    }));
  }
}
