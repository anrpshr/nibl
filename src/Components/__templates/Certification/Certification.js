import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import downloadIcon from '../../../assets/img/download.png';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import Transformer from './transformer';
import Styles from './certification.module.scss';

class Certification extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    pageTitle: '',
    certification: [],
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_CERTIFICATION, { pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error trying to fetch Certifications', e);
      });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      pageTitle,
      certification,
      loading
    } = this.state;

    const certificationTable = certification.map(item => (
      {
        primary:
        (
          <div className={`${Styles.primaryPanel}`}>
            <div>
              <p>
                {item.primary}
              </p>
            </div>
            <a href={`${item.downloadLink}`}><img src={downloadIcon} alt=""/></a>
          </div>
        ),
        secondary: <div>{item.secondary}</div>
      }
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <div className="heading">
                      <h1>{pageTitle}</h1>
                    </div>
                    <ContentHeader grey>
                      <div className={Styles.contentHeader}>
                        <span className={Styles.sn}>S.N</span>
                        <span className={Styles.details}>Details</span>
                      </div>
                    </ContentHeader>
                    <ExpansionPanel
                      isOrdered
                      items={certificationTable}
                    />
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default Certification;
