import { isFinite } from 'lodash';

export default class BranchTransformer {
  constructor(ATMApiObject) {
    this.id = ATMApiObject.id;
    this.name = ATMApiObject.locationName;
    this.address = ATMApiObject.address;
    this.lat = parseFloat(ATMApiObject.latitude);
    this.lng = parseFloat(ATMApiObject.longitude);
    this.phoneNumber = ATMApiObject.phoneNumber;
    this.faxNumber = ATMApiObject.faxNumber;
    this.isInsideValley = ATMApiObject.isInsideValley;
    this.isValid = isFinite(this.lat) && isFinite(this.lng);
  }
}
