import React from 'react';
import PropTypes from 'prop-types';
import Styles from './lockers.module.scss';

// Component
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';

const LockerTable = ({ details }) => {
  const lockerRow = details.map(detailItem => (
    <TableBody>
      <div className={Styles.topHeading}>
        <div>{detailItem.lockerType}</div>
        <div className={Styles.hwd}>
          <span>{detailItem.height}</span>
          <span>{detailItem.width}</span>
          <span>{detailItem.depth}</span>
        </div>
        <div className={Styles.deposits}>{detailItem.keyDeposits}</div>
        <div className={Styles.anualCharges}>
          <span>{detailItem.annualChargeOutsideValley}</span>
          <span>{detailItem.annualChargeInsideValley}</span>
        </div>
      </div>
    </TableBody>
  ));

  return (
    <div className={Styles.tableWrapper}>
      <ContentHeader grey>
        <div className={Styles.tableHead}>
          <div className={Styles.topHeading}>
            <div className={Styles.dimension}>Dimension in inches</div>
            <div className={Styles.charges}>Charges in NPR</div>
            <div>Locker Type</div>
            <div className={Styles.hwd}>
              <span>Height</span>
              <span>Width</span>
              <span>Depth</span>
            </div>
            <div className={Styles.deposits}>Key Deposits</div>
            <div className={Styles.anualCharges}>
              <span>Annual Charges Outside Valley</span>
              <span>Annual Charges Inside Valley</span>
            </div>
          </div>
        </div>
      </ContentHeader>
      <div className={Styles.tableBodyWrapper}>
        {lockerRow}
      </div>
    </div>
  );
};

LockerTable.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default LockerTable;
