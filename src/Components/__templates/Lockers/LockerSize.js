import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styles from './lockers.module.scss';

// Componets
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import LockerTable from './LockerTable';

class LockerSize extends Component {
  state = {
    activeItemKey: null
  };

  componentDidMount() {
    const { items } = this.props;
    if (items.length > 0) {
      this.setState({ activeItemKey: items[0].key });
    }
  }

  render() {
    const { activeItemKey } = this.state;
    const { items } = this.props;
    const lockerList = items.map(item => (
      <LockerTable key={`locker-${item.key}`} details={item.details} isActive={activeItemKey === item.key}/>
    ));

    return (
      <div className={Styles.lockerSizeWrapper}>
        <div className="container">
          <div className={Styles.heading}>
            <h1>Locker Sizes</h1>
          </div>
          <TabNav
            onClick={(key) => { this.setState({ activeItemKey: key }); }}
            activeItemKey={activeItemKey}
            items={items}
          />
          <TabContent>
            {lockerList}
          </TabContent>
        </div>
      </div>
    );
  }
}

LockerSize.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
};

LockerSize.defaultProps = {
  items: []
};

export default LockerSize;
