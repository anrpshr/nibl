import slugs from 'slugs';
import { chain, zipObject } from 'lodash';

export default class LockerTransformer {
  constructor(LockerAPIObject) {
    this.bannerImg = LockerAPIObject.bannerDetails.bannerImage;
    this.bannerTitle = LockerAPIObject.bannerDetails.bannerTitle;
    this.bannerDescription = LockerAPIObject.bannerDetails.bannerCaption;
    this.bannerAlt = LockerAPIObject.bannerDetails.bannerAlt;
    this.contentDescription = LockerAPIObject.pageContent.lockerDetails;
    this.lockerImage = LockerAPIObject.pageContent.lockerImage;
    this.lockerImageAlt = LockerAPIObject.pageContent.lockerImageAlt;
    this.lockerFeatures = {
      content: LockerAPIObject.pageContent.lockerFeatures,
      image: {
        url: LockerAPIObject.pageContent.featureImage,
        alt: LockerAPIObject.pageContent.featureImageAlt,
        title: LockerAPIObject.pageContent.featureImageTitle,
        description: LockerAPIObject.pageContent.featureImageDescription,
        link: LockerAPIObject.pageContent.featureImageDetailLink
      }
    };
    this.lockerRequirements = {
      content: LockerAPIObject.pageContent.lockerRequirements,
      image: {
        url: LockerAPIObject.pageContent.lockerRequirementsImage,
        alt: LockerAPIObject.pageContent.lockerRequirementsImageAlt,
        title: LockerAPIObject.pageContent.lockerRequirementsTitle,
        description: LockerAPIObject.pageContent.lockerRequirementsDescription,
        link: LockerAPIObject.pageContent.lockerRequirementsImageDetailLink
      }
    };

    // populate items
    this.items = chain(LockerAPIObject.pageLockerSizes)
      .groupBy('categoryName')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.activeItemKey = this.items[0].key;
  }
}
