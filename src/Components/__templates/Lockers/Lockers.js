import React, { Component } from 'react';
import LocationOn from '@material-ui/icons/LocationOn';
import { generatePath } from 'react-router';

import Styles from './lockers.module.scss';

// Compontents
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import FeaturesAndBenefits from '../../FeaturesAndBenefits';
import Strip from '../../Strip';
import LockerSize from './LockerSize';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import LockerTransformer from './lockers.transformer';
import Loading from '../../Loading';

class Lockers extends Component {
  state = {
    bannerImg: null,
    bannerAlt: '',
    bannerTitle: '',
    bannerDescription: '',
    lockerImage: null,
    lockerImageAlt: '',
    contentDescription: '',
    lockerFeatures: {
      content: null,
      image: {
        url: null,
        alt: null,
        title: null,
        description: null,
        link: null
      }
    },
    lockerRequirements: {
      content: null,
      image: {
        url: null,
        alt: null,
        title: null,
        description: null,
        link: null
      }
    },
    items: [],
    isLoading: true
  }

  componentDidMount() {
    const { pageId, hasPageContent } = this.props;
    getV2(generatePath(API.endPoints.LOCKER, { hasPageContent, pageId }))
      .then((lockers) => {
        const transformedLockers = new LockerTransformer(lockers);
        this.setState({ ...transformedLockers, isLoading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch lockers', e);
      });
  }

  render() {
    const {
      bannerImg,
      bannerAlt,
      bannerTitle,
      bannerDescription,
      contentDescription,
      lockerFeatures,
      lockerRequirements,
      items,
      lockerImage,
      lockerImageAlt,
      isLoading
    } = this.state;
    return (
      <div className={Styles.wrapper}>
        {
          isLoading
            ? (
              <Loading color="red" fullHeight/>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDescription}
                  alt={bannerAlt}
                />
                <div className="white-gradient">
                  <div className={Styles.greyBack}>
                    <div className="container">
                      <div className={Styles.heading}>
                        <h1>Locker</h1>
                        <a href="/" className={Styles.location}>
                          <LocationOn/>
                          <span>See the Locker Location</span>
                        </a>
                      </div>
                      <LeftImageText title="Details" image={lockerImage} imageAlt={lockerImageAlt}>
                        <div dangerouslySetInnerHTML={{ __html: contentDescription }}/>
                      </LeftImageText>
                      <FeaturesAndBenefits
                        firstTableTitle="Requirements"
                        secondTableTitle="Features"
                        firstTableContent={lockerRequirements.content}
                        secondTableContent={lockerFeatures.content}
                        features={lockerRequirements.content}
                        benefits={lockerFeatures.content}
                        featuredBlockImage={lockerRequirements.image.url}
                        featuredImageAlt={lockerRequirements.image.alt}
                        featuredBlockImageCaption={lockerRequirements.image.description}
                        featuredBlockImageTitle={lockerRequirements.image.title}
                        featuredBlockImageLink={lockerRequirements.image.link}
                        benefitsBLockImage={lockerFeatures.image.url}
                        benefitsImageAlt={lockerFeatures.image.alt}
                        benefitsBlockImageCaption={lockerFeatures.image.description}
                        benefitsBlockImageTitle={lockerFeatures.image.title}
                        benefitsBlockImageLink={lockerFeatures.image.link}
                      />
                    </div>
                  </div>
                  <div className={Styles.strip}>
                    <Strip btnTitle="Download" text="Download the Locker application form."/>
                  </div>
                  <LockerSize
                    items={items}
                  />
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default Lockers;
