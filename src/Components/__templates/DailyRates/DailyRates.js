import React, { Component } from 'react';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

import ForexRates from '../../ForexRates';
import BannerBlock from '../../BannerBlock';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Transformer from './rates.transformer';
import Styles from './dailyRates.module.scss';

class DailyRates extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      bannerImg: '',
      bannerTitle: '',
      bannerDescription: '',
      bannerAlt: ''
    };
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.FOREX_PAGE, { pageId }))
      .then((content) => {
        const t = new Transformer(content);
        this.setState({ ...t, loading: false });
      })
      .catch();
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      loading
    } = this.state;

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <ForexRates/>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}


export default DailyRates;
