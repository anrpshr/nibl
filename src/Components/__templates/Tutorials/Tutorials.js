import React, { Component } from 'react';
import Styles from './tutorials.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';

import Instructions from './Instructions';
import TutorialVideos from './TutorialVideos';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';
import TabContent from '../../TabContent';

class Tutorials extends Component {
  state = {
    items: [
      {
        name: 'Instructions',
        key: 'instructions'
      },
      {
        name: 'Videos',
        key: 'videos'
      }
    ],
    activeItemKey: 'instructions'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={banner}
          title="Product Video"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="white-gradient main--content">
          <div className={Styles.tabnavWrapper}>
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
          </div>
          <TabContent>
            <Instructions isActive={activeItemKey === 'instructions'}/>
            <TutorialVideos isActive={activeItemKey === 'videos'}/>
          </TabContent>
        </div>
      </div>
    );
  }
}

export default Tutorials;
