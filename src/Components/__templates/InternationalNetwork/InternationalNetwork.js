import React, { Component } from 'react';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';

import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import ContentSection from './ContentSection';
import Styles from './internationalNetwork.module.scss';
import Transformer from './transformer';

class InternationalNetwork extends Component {
  state = {
    bannerImg: null,
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    items: [
      {
        name: 'United Kingdom',
        key: 'united-kingdom'
      },
      {
        name: 'UAE',
        key: 'uae'
      },
      {
        name: 'Qatar',
        key: 'qatar'
      },
      {
        name: 'Saudi Arabia',
        key: 'saudi-arabia'
      },
      {
        name: 'Cyprus',
        key: 'cyprus'
      },
      {
        name: 'Kuwait',
        key: 'kuwait'
      },
      {
        name: 'United States',
        key: 'united-states'
      },
      {
        name: 'South Korea',
        key: 'south-korea'
      }
    ],
    activeItemKey: 'united-kingdom',
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.REMITTANCE_INTERNATIONAL, { pageId }))
      .then((content) => {
        const t = new Transformer(content);
        this.setState({ ...t, loading: false });
      })
      .catch();
  }

  render() {
    const { items, activeItemKey, loading, bannerDescription, bannerImg, bannerAlt, bannerTitle } = this.state;
    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <TabNav
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      activeItemKey={activeItemKey}
                      items={items}
                    />
                    <div className={Styles.tabContent}>
                      <TabContent>
                        <ContentSection isActive={activeItemKey === 'united-kingdom'}/>
                        <ContentSection isActive={activeItemKey === 'uae'}/>
                      </TabContent>
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default InternationalNetwork;
