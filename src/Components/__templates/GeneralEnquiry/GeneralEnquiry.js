import React from 'react';
import ChevronRight from '@material-ui/icons/ChevronRight';

import Styles from './generalEnquiry.module.scss';

// Components
import BannerBlock from '../../BannerBlock';
import ContentCard from '../../ContentCard';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

// Images
import banner from '../../../assets/img/mobtop-bg.jpg';

const GeneralEnquiry = () => (
  <div className="white-gradient main--content">
    <div className="container">
      <div className="heading">
        <h1>General Enquiries</h1>
      </div>

      <ContentCard>
        <div className={Styles.gridBlock}>
          <div className={Styles.leftBlock}>
            <div className={Styles.title}>
              Call
            </div>
            <div className={Styles.innerLeft}>
              <span>
                <ChevronRight/>
                Call at 014567890 or 01456789
              </span>
              <span>
                <ChevronRight/>
                Any time between 9 am - 5 pm
              </span>
              <span>
                <ChevronRight/>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
              </span>
            </div>
          </div>
          <div className={Styles.rightBlock}>
            <div className={Styles.title}>
              Call any time for
            </div>
            <div className={Styles.innerRight}>
              <span>
                <ChevronRight/>
                Lost / Stolen Cards
              </span>
              <span>
                <ChevronRight/>
                Credit Cards
              </span>
              <span>
                <ChevronRight/>
                ATMs
              </span>
              <span>
                <ChevronRight/>
                Transaction Disputes
              </span>
            </div>
          </div>
        </div>
      </ContentCard>
    </div>
  </div>
);

export default GeneralEnquiry;
