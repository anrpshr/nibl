import React from 'react';
import PropTypes from 'prop-types';
import LocationOn from '@material-ui/icons/LocationOn';
import Styles from './branchlessBanking.module.scss';

// Components
import ContentCard from '../../ContentCard';
import TabContent from '../../TabContent';

const BranchlessBankingContent = ({ title, content }) => (
  <div className={Styles.contentWrapper}>
    <div className={Styles.heading}>
      <h1>{title}</h1>
      <a href="1" className={Styles.location}>
        <LocationOn/>
        <span>See the Location</span>
      </a>
    </div>
    <ContentCard>
      {/* {TabContent} */}
      <p>
        NIBL has deployed a Branchless Banking System that is capable of providing
        Retail Banking features such as Cash Deposit/Withdrawal, Bill Payments,
        Fund Transfers and Inquiries to the NIBL Customers.
      </p>
      <p>
        Branchless Banking is an economical channel for delivering financial
        services without relying on the traditional bank branches. Branchless
        Banking provides basic banking services through NIBL Agents having
        Bio-metric POS devices (with finger print scanner). Branchless Banking
        customers are also provided with NIBL VISA Card, which along with their
        finger prints can be used to avail services through the POS.
      </p>
      <p>
        The Branchless Banking Service can be availed by New Customers
        by enrolling to this system and opening account with NIBL or
        availed by existing customers by simply enrolling to this system.
        Branchless Banking Services will be provided only in VDCs
      </p>
    </ContentCard>
  </div>
);

BranchlessBankingContent.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default BranchlessBankingContent;
