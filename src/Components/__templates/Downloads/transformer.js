import slugs from 'slugs';
import { chain, zipObject } from 'lodash';

export default class Transformer {
  constructor(DownloadsApiObject) {
    this.bannerImg = DownloadsApiObject.bannerDetails.bannerImage;
    this.bannerTitle = DownloadsApiObject.bannerDetails.bannerTitle;
    this.bannerDescription = DownloadsApiObject.bannerDetails.bannerCaption;
    this.bannerAlt = DownloadsApiObject.bannerDetails.bannerAlt;
    this.items = chain(DownloadsApiObject.pageContent)
      .groupBy('categoryName')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.activeItemKey = this.items[0].key;
  }
}
