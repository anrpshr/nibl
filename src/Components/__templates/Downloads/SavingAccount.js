import React from 'react';
import PropTypes from 'prop-types';
import Styles from './downloads.module.scss';

// Compontents
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';

// Images
import downloadIcon from '../../../assets/img/download.png';

const SavingAccount = ({ details }) => {
  const contentTable = details.map((detailsItem, index) => (
    {
      primary: (
        <div className={`${Styles.primaryPanel}`} key={`downloads-primary-${index}`}>
          <div>
            <p>
              {detailsItem.fileName}
            </p>
          </div>
          <a download href={`${detailsItem.uploadedFile}`}>
            <img src={downloadIcon} alt=""/>
          </a>
        </div>
      ),
      secondary: (
        <div key={`downloads-secondary-${index}`}>
          {detailsItem.fileDescription}
        </div>
      )
    }
  ));

  return (
    <div className={Styles.savingAccount}>
      <ContentHeader grey>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>S.N</span>
          <span className={Styles.details}>Details</span>
        </div>
      </ContentHeader>
      <ExpansionPanel
        isOrdered
        items={contentTable}
      />
    </div>
  );
};

SavingAccount.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object)
};

SavingAccount.defaultProps = {
  details: []
};

export default SavingAccount;
