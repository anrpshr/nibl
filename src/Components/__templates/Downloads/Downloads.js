import React, { Component } from 'react';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import SavingAccount from './SavingAccount';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Transformer from './transformer';

import Styles from './downloads.module.scss';

class Downloads extends Component {
  state = {
    loading: true,
    bannerImg: null,
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    items: [],
    activeItemKey: ''
  }

  limit = -1;

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_DOWNLOADS, { pageId, limit: this.limit }))
      .then((downloadContent) => {
        console.log(downloadContent);
        const transformedContent = new Transformer(downloadContent);
        this.setState({
          ...transformedContent,
          loading: false
        });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Downloads', e);
      });
  }

  handleNext = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === items.length - 1) {
      index = 0;
    } else {
      index++;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  handlePrevious = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === 0) {
      index = items.length - 1;
    } else {
      index--;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  render() {
    const {
      loading,
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      items,
      activeItemKey
    } = this.state;

    const contentTable = items.map((item, index) => (
      <SavingAccount
        key={`contentTable-${index}`}
        details={item.details}
        isActive={activeItemKey === item.key}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient reset">
                  <div className="container">
                    <TabNav
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      activeItemKey={activeItemKey}
                      items={items}
                      onLeftArrowClick={this.handlePrevious}
                      onRightArrowClick={this.handleNext}
                    />
                    <div className="heading">
                      <h1>Downloads</h1>
                    </div>
                    <TabContent>
                      {contentTable}
                    </TabContent>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default Downloads;
