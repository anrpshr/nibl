import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

// Components
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import ContentCard from '../../ContentCard';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import Transformer from './transformer';

// Images
import downloadIcon from '../../../assets/img/download.png';
import Styles from './mobileBanking.module.scss';

class MobileBanking extends Component {
  state = {
    bannerImg: null,
    bannerTitle: '',
    bannerDescription: '',
    productImage: null,
    contentDescription: '',
    features: '',
    downloadItems: [],
    loading: true
  }

  componentDidMount() {
    const { pageId, hasPageContent } = this.props;
    getV2(generatePath(API.endPoints.MOBILE_BANKING, { hasPageContent, pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error occurred while trying to fetch Mobile Banking Content', e);
      });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      contentDescription,
      downloadItems,
      productImage,
      features,
      loading
    } = this.state;

    const downloadList = downloadItems.map(downloadItem => (
      {
        primary: (
          <div className={Styles.expansionPanel}>
            <div>
              <p>{downloadItem.primary}</p>
            </div>
            <a href={downloadItem.downloadLink}><img src={downloadIcon} alt=""/></a>
          </div>
        ),
        secondary: (
          <div>
            {downloadItem.secondary}
          </div>
        )
      }
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDescription}
                />
                <div className="white-gradient" style={{ paddingBottom: 1 }}>
                  <div className={Styles.greyBack}>
                    <div className="container">
                      <div className={Styles.heading}>
                        <h1>Mobile Banking</h1>
                      </div>
                      <div className={Styles.contentWrapper}>
                        <LeftImageText title="Details" image={productImage}>
                          <div dangerouslySetInnerHTML={{ __html: contentDescription }}/>
                        </LeftImageText>
                      </div>
                      {
                        features && (
                          <>
                            <div className={Styles.heading}>
                              <h1>Features</h1>
                            </div>
                            <ContentCard>
                              <div className={Styles.features}>
                                <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: features }}/>
                              </div>
                            </ContentCard>
                          </>
                        )
                      }
                    </div>
                  </div>
                  <div className="container">
                    <div className={Styles.contentHeaderWrapper}>
                      <ContentHeader grey>
                        <span>S.N</span>
                        <span>Details</span>
                      </ContentHeader>
                    </div>
                    <div className={Styles.expansionPanelWrapper}>
                      <ExpansionPanel
                        isOrdered
                        items={downloadList}
                      />
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default MobileBanking;
