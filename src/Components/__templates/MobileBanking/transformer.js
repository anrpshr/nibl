export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.productImage = APIObject.pageContent[0].image;
    this.contentDescription = APIObject.pageContent[0].description;
    this.features = '';
    this.downloadItems = APIObject.pageDownloads.map(e => ({
      primary: e.fileName,
      secondary: e.fileDescription,
      downloadLink: e.fileUploaded
    }));
  }
}
