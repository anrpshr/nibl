import React from 'react';
import Styles from './reports.module.scss';

// Components
import ReportsContent from './ReportsContent';

const ReportsMain = () => (
  <div className={Styles.wrapper}>
    <ReportsContent title="Reports"/>
  </div>
);

export default ReportsMain;
