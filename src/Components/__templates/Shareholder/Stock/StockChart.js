import React from 'react';
import Styles from './stock.module.scss';

// Images
import chart from '../../../../assets/img/chart.jpg';

const StockChart = () => (
  <div className={Styles.chartWrapper}>
    <img src={chart} alt=""/>
    <span className={Styles.detailLink}>
      For more details
      <a href="1">Click Here</a>
    </span>
  </div>
);

export default StockChart;