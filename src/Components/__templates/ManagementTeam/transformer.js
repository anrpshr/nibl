import { chain, zipObject } from 'lodash';
import slugs from 'slugs';

export default class Transformer {
  constructor(APIObject) {
    this.bannerImage = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.items = chain(APIObject.pageContent)
      .groupBy('memberDesignation')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.activeItemKey = this.items[0].key;
  }
}
