import React from 'react';
import Styles from './managementTeam.module.scss';

// Component
import ContentHeader from '../../ContentHeader';

const ManagementTeamTbl = ({ content }) => (
  <div>
    <ContentHeader>
      <div className={Styles.tblGrid}>
        <span className={Styles.sn}>S.N</span>
        <span className={Styles.province}>Province</span>
        <span className={Styles.managers}>Managers</span>
        <span className={Styles.provinceOffice}>Province Office</span>
      </div>
    </ContentHeader>
    {content}
  </div>
);

export default ManagementTeamTbl;
