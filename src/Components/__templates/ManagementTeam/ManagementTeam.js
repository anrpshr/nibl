import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

// Components
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import ManagementTeamTbl from './ManagementTeamTbl';
import Transformer from './transformer';
import Styles from './managementTeam.module.scss';
import TableBody from '../../TableBody';

class ManagementTeam extends Component {
  state = {
    bannerImage: null,
    bannerTitle: '',
    bannerCaption: '',
    bannerAlt: '',
    items: [],
    activeItemKey: null,
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.ABOUT_BOD, { pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Team members', e);
      });
  }

  handleNext = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === items.length - 1) {
      index = 0;
    } else {
      index++;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  handlePrevious = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === 0) {
      index = items.length - 1;
    } else {
      index--;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  render() {
    const { items, activeItemKey, bannerAlt, bannerCaption, bannerImage, bannerTitle, loading } = this.state;

    const tableTabs = items.map((item, i) => {
      const tableContent = item.details.map((detail, j) => (
        <TableBody key={`team-member-table-${i}-${j}`}>
          <div className={Styles.tblGrid}>
            <span className={Styles.sn}>{i}</span>
            <span className={Styles.province}>{detail.memberProvince}</span>
            <span className={Styles.managers}>{detail.memberName}</span>
            <span className={Styles.provinceOffice}>{detail.memberProvinceOfficer}</span>
          </div>
        </TableBody>
      ));
      return <ManagementTeamTbl key={`team-table-${i}`} isActive={activeItemKey === item.key} content={tableContent}/>;
    });
    return (
      <>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock background={bannerImage} alt={bannerAlt} title={bannerTitle} description={bannerCaption}/>
                <div className="white-gradient main--content">
                  <div className="container">
                    <div className={Styles.tabNavWrapper}>
                      <TabNav
                        arrows
                        stretch
                        onClick={(key) => { this.setState({ activeItemKey: key }); }}
                        activeItemKey={activeItemKey}
                        items={items}
                        onLeftArrowClick={this.handlePrevious}
                        onRightArrowClick={this.handleNext}
                      />
                    </div>
                    <TabContent>
                      {tableTabs}
                    </TabContent>
                  </div>
                </div>
              </>
            )
        }
      </>
    );
  }
}

export default ManagementTeam;
