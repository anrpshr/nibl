import React from 'react';
import { Row, Col } from 'antd';

// Components
import ContactFrom from '../../ContactForm';
import Styles from './supportContact.module.scss';

const SupportContact = () => (
  <div className={Styles.wrapper}>
    <div className="container">
      <div className={Styles.flexBlock}>
        <div className={Styles.leftBlock}>
          <div className={Styles.generalTexts}>
            <span>CONTACT US FOR ANY QUERIES OR COMPLAINTS</span>
          </div>
        </div>
        <div className={Styles.rightBlock}>
          <ContactFrom>
            <h2>CONTACT FORM</h2>
            <Row gutter={24}>
              <Col span={12}>
                <div className={Styles.element}>
                  <span>First Name</span>
                  <input type="text"/>
                </div>
              </Col>
              <Col span={12}>
                <div className={Styles.element}>
                  <span>Last Name</span>
                  <input type="text"/>
                </div>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={12}>
                <div className={Styles.element}>
                  <span>Permanent Address</span>
                  <input type="text"/>
                </div>
              </Col>
              <Col span={12}>
                <div className={Styles.element}>
                  <span>Temporary Address</span>
                  <input type="text"/>
                </div>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={12}>
                <div className={Styles.element}>
                  <span>Phone Number</span>
                  <input type="tel"/>
                </div>
              </Col>
              <Col span={12}>
                <div className={Styles.elementCheckbox}>
                  <input type="checkbox"/>
                  Callback Request
                </div>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={12}>
                <div className={Styles.element}>
                  <span>Email Address</span>
                  <input type="tel"/>
                </div>
              </Col>
              <Col span={12}>
                <div className={Styles.elementDepartment}>
                  <span>Department</span>
                  <select>
                    <option>HR</option>
                    <option>Loan</option>
                    <option>CSD</option>
                    <option>Cash</option>
                  </select>
                </div>
              </Col>
            </Row>

            <Row>
              <div className={Styles.elementQuery}>
                <span>Your Query (250 words)</span>
                <textarea rows="5"/>
              </div>
            </Row>

            <button className={Styles.button}>Send</button>
          </ContactFrom>
        </div>
      </div>
    </div>
  </div>
);

export default SupportContact;
