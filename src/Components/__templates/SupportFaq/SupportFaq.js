import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import Tab from './Tab';
import BannerBlock from '../../BannerBlock';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Transformer from './transformer';
import Styles from './supportFaq.module.scss';

class SupportFaq extends Component {
  state = {
    loading: false,
    bannerImage: null,
    bannerTitle: '',
    bannerCaption: '',
    bannerAlt: '',
    pageTitle: '',
    items: [],
    activeItemKey: ''
  }

  componentDidMount() {
    const { pageId, hasPageContent } = this.props;
    getV2(generatePath(API.endPoints.FAQ, { hasPageContent, pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch FAQ', e);
      });
  }

  render() {
    const { items, activeItemKey, bannerAlt, bannerCaption, bannerImage, bannerTitle, loading, pageTitle } = this.state;
    const tabs = items.map((e, i) => (
      <Tab items={e.details} key={`faq-tab-${i}`} isActive={activeItemKey === e.key}/>
    ));
    return (
      <div>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImage}
                  alt={bannerAlt}
                  description={bannerCaption}
                  title={bannerTitle}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <div className={Styles.tabNavWrapper}>
                      <TabNav
                        onClick={(key) => { this.setState({ activeItemKey: key }); }}
                        activeItemKey={activeItemKey}
                        items={items}
                      />
                    </div>
                    <div className="heading">
                      <h1>{pageTitle}</h1>
                    </div>
                    <TabContent>
                      {tabs}
                    </TabContent>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default SupportFaq;
