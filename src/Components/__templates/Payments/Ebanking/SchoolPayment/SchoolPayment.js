import React from 'react';
import PropTypes from 'prop-types';

import Styles from './schoolPayment.module.scss';

// Components
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';

const MobileTopUp = ({ schools }) => {
  const schoolList = schools.map(schoolItem => (
    <TableBody>
      <span>1</span>
      <span>{schoolItem.title}</span>
      <span><a href="1">Pay bill</a></span>
    </TableBody>
  ));

  return (
    <div className={Styles.wrapper}>
      <div className={Styles.heading}>
        <h1>NTC Topup</h1>
      </div>
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader grey>
          <span>S.N</span>
          <span>Details</span>
          <span>Payments</span>
        </ContentHeader>
      </div>
      <div className={Styles.tableRowWrapper}>
        {schoolList}
      </div>
    </div>
  );
};

MobileTopUp.propTypes = {
  schools: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default MobileTopUp;
