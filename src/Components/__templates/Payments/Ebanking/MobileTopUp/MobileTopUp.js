import React from 'react';
import PropTypes from 'prop-types';

import Styles from './mobileTopUp.module.scss';

// Components
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';

const MobileTopUp = ({ ntcItems, ncellItems }) => {
  const ntcList = ntcItems.map(ntcItem => (
    <TableBody>
      <span>1</span>
      <span>{ntcItem.title}</span>
      <span><a href="1">Pay bill</a></span>
    </TableBody>
  ));

  const ncellList = ncellItems.map(ncellItem => (
    <TableBody>
      <span>1</span>
      <span>{ncellItem.title}</span>
      <span><a href="1">Pay bill</a></span>
    </TableBody>
  ));

  return (
    <div className={Styles.wrapper}>
      <div className={Styles.heading}>
        <h1>NTC Topup</h1>
      </div>
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader grey>
          <span>S.N</span>
          <span>Details</span>
          <span>Payments</span>
        </ContentHeader>
      </div>
      <div className={Styles.tableRowWrapper}>
        {ntcList}
      </div>
      <div className={Styles.heading}>
        <h1>Ncell Top Up</h1>
      </div>
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader grey>
          <span>S.N</span>
          <span>Details</span>
          <span>Payments</span>
        </ContentHeader>
      </div>
      <div className={Styles.tableRowWrapper}>
        {ncellList}
      </div>
    </div>
  );
};

MobileTopUp.propTypes = {
  ntcItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  ncellItems: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default MobileTopUp;
