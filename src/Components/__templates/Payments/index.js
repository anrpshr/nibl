import React, { Component } from 'react';
import Styles from './styles.module.scss';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BigTitleBlockSet from '../../Layouts/Block/BigTitleBlockSet';

import Ebanking from './Ebanking';

import paymentBg from '../../../assets/img/payment-bg.jpg';

class Payments extends Component {
  state = {
    items: [],
    activeItemKey: '',
    bannerImg: null,
    bannerTitle: '',
    bannerDescription: '',
    ntcItems: [],
    ncellItems: [],
    schools: []
  }

  componentDidMount() {
    this.setState({
      items: [
        {
          name: 'eBanking',
          key: 'e-banking'
        },
        {
          name: 'Bill Payments',
          key: 'bill-payments'
        }
      ],
      activeItemKey: 'e-banking',
      bannerImg: paymentBg,
      bannerTitle: '',
      bannerDescription: '',
      ntcItems: [
        {
          title: 'NTC Post Paid Mobile Bills'
        },
        {
          title: 'NTC Pre Paid Mobile Bills'
        },
        {
          title: 'NTC Landline Bill'
        },
        {
          title: 'NTC Adsl Bill'
        }
      ],
      ncellItems: [
        {
          title: 'Ncell Pre Paid Mobile Bills'
        },
        {
          title: 'Ncell Post Paid Mobile Bills'
        }
      ],
      schools: [
        {
          title: 'DAV'
        },
        {
          title: 'Rai School'
        },
        {
          title: 'Little Angels'
        },
        {
          title: 'Gems'
        }
      ]
    });
  }

  render() {
    const {
      items,
      activeItemKey,
      bannerImg,
      bannerTitle,
      bannerDescription,
      ntcItems,
      ncellItems,
      schools
    } = this.state;
    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={bannerImg}
          title={bannerTitle}
          description={bannerDescription}
        />
        <div className="white-gradient" style={{ paddingBottom: 1 }}>
          <div className={Styles.iconBlockWrapper}>
            <div className="container">
              <div className={Styles.tabnavWrapper}>
                <TabNav
                  onClick={(key) => { this.setState({ activeItemKey: key }); }}
                  activeItemKey={activeItemKey}
                  items={items}
                />
              </div>
              <div className={Styles.ebankingWrapper}>
                <TabContent>
                  <Ebanking
                    ntcItems={ntcItems}
                    ncellItems={ncellItems}
                    schools={schools}
                    isActive={activeItemKey === 'e-banking'}
                  />
                  <Ebanking isActive={activeItemKey === 'bill-payments'} />
                </TabContent>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Payments;
