import React, { Component } from 'react';
import { generatePath } from 'react-router';
import { Spin } from 'antd';

import BannerBlock from '../../BannerBlock';
import ContentCard from '../../ContentCard';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Transformer from './generalContent.transformer';

class FullWidthContentCard extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    title: '',
    content: '',
    loading: true
  }

  componentDidMount() {
    const { pageId, hasPageContent } = this.props;
    getV2(generatePath(API.endPoints.GENERAL, { pageId, hasPageContent }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch content', e);
      });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      title,
      content,
      loading
    } = this.state;

    return (
      <>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <div>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <div className="heading">
                      <h1>{title}</h1>
                    </div>
                    <ContentCard>
                      <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: content }}/>
                    </ContentCard>
                  </div>
                </div>
              </div>
            )
        }
      </>
    );
  }
}

export default FullWidthContentCard;
