import React from 'react';

import MapWithWhiteCard from '../../MapWithWhiteCard';

const CorporateOffice = () => (
  <>
    <div className="white-gradient main--content">
      <div className="container">
        <MapWithWhiteCard/>
      </div>
    </div>
  </>
);

export default CorporateOffice;
