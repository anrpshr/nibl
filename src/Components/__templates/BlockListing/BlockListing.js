import React from 'react';

import Products from '../../__pages/Home/Products';
import BannerBlock from '../../BannerBlock';
import banner from '../../../assets/img/mobtop-bg.jpg';
import './styles.scss';

const BlockListing = () => (
  <div className="block-listing-wrap">
    <BannerBlock
      title=""
      description=""
      background={banner}
    />
    <div className="white-gradient">
      <div className="main--content">
        <Products title="Accounts"/>
      </div>
    </div>
  </div>
);

export default BlockListing;
