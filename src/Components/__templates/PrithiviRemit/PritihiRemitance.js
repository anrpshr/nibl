import React, { Component } from 'react';
import classnames from 'classnames';
// import ChevronRight from '@material-ui/icons/ChevronRight';
import { Row, Col, Spin } from 'antd';
import { generatePath } from 'react-router';

// Components
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
// import ButtonRedBorder from '../../ButtonRedBorder';
import BigTitleBlock from '../../Layouts/Block/BigTitleBlock';
import ImageBlockWithOverlay from '../../Layouts/Block/ImageBlockWithOverlay';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';

// Images
import bottomBg from '../../../assets/img/objectives-back.jpg';
// import downArrow from '../../../assets/img/down-arrow.png';
// import dummy from '../../../assets/img/dummy.png';
// import niblImg from '../../../assets/img/nibl-dummy.jpg';
import Transformer from './transformer';
import Styles from './prithivi.module.scss';

class PritiviRemitance extends Component {
  state = {
    bannerImg: null,
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    title: '',
    firstFeatureTitle: '',
    firstFeatureContent: '',
    firstFeatureImage: '',
    firstFeatureImageTitle: '',
    firstFeatureImageDescription: '',
    secondFeatureTitle: '',
    secondFeatureContent: '',
    secondFeatureImage: '',
    secondFeatureImageTitle: '',
    secondFeatureImageDescription: '',
    currencyImage: '',
    currencyContent: '',
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.REMITTANCE_PRITHIVI, { pageId }))
      .then((response) => {
        const transformedContent = new Transformer(response);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch();
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      title,
      firstFeatureTitle,
      firstFeatureContent,
      firstFeatureImage,
      firstFeatureImageTitle,
      firstFeatureImageDescription,
      secondFeatureTitle,
      secondFeatureContent,
      secondFeatureImage,
      secondFeatureImageTitle,
      secondFeatureImageDescription,
      currencyContent,
      loading
    } = this.state;

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDescription}
                  alt={bannerAlt}
                />
                <div className="white-gradient">
                  <div className={Styles.greyInner}>
                    <div className="container">
                      <div className={Styles.heading}>
                        <h1>Prithivi Remitance</h1>
                      </div>
                      {/* <LeftImageText
                        title={title}
                        image=""
                      >
                        <div dangerouslySetInnerHTML={{ __html: '' }}/>
                      </LeftImageText> */}

                      <div className={Styles.heading}>
                        <h1 className="heading">Main Features</h1>
                      </div>
                      <div className={Styles.flexSection}>
                        <Row gutter={24} type="flex">
                          <Col span={17}>
                            <BigTitleBlock title={firstFeatureTitle}>
                              <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: firstFeatureContent }}/>
                            </BigTitleBlock>
                          </Col>
                          <Col span={7}>
                            <ImageBlockWithOverlay title={firstFeatureImageTitle} image={firstFeatureImage}>
                              <p>{firstFeatureImageDescription}</p>
                            </ImageBlockWithOverlay>
                          </Col>
                        </Row>
                        
                        
                      </div>
                      <div className={classnames(Styles.flexSection, Styles.flexSectionMob)}>
                        <Row type="flex" gutter={24}>
                          <Col span={7}>
                            <ImageBlockWithOverlay title={secondFeatureImageTitle} image={secondFeatureImage}>
                              <p>{secondFeatureImageDescription}</p>
                            </ImageBlockWithOverlay>
                          </Col>
                          <Col span={17}>
                            <BigTitleBlock title={secondFeatureTitle}>
                              <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: secondFeatureContent }}/>
                            </BigTitleBlock>
                          </Col>
                        </Row>
                      </div>
                    </div>

                    <div className={Styles.bottomSection} style={{ background: `url(${bottomBg})` }}>
                      <div className="container">
                        <div className={Styles.outerSection}>
                          <div className={Styles.innerSection}>
                            {/* <BigTitleBlock title="Currency Conversion">
                              <div className={Styles.elementsWrapper}>
                                <input placeholder="Rs 600"/>
                                <div className={Styles.currency}>
                                  <div className={Styles.currencySelector}>
                                    <img src={downArrow} alt=""/>
                                    <select>
                                      <option>Rs</option>
                                      <option>USD</option>
                                    </select>
                                  </div>
                                  <div className={Styles.currencySelector}>
                                    <img src={downArrow} alt=""/>
                                    <select>
                                      <option>USD</option>
                                      <option>Rs</option>
                                    </select>
                                  </div>
                                </div>
                                <ButtonRedBorder title="Apply Now"/>
                                <div className={Styles.number}>
                                  5.38 USD
                                </div>
                              </div>
                            </BigTitleBlock> */}
                            <div className={Styles.bottomList}>
                              <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: currencyContent }}/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default PritiviRemitance;
