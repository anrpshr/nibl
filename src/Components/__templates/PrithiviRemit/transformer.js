export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.title = APIObject.pageDetails.pageTitle;
    this.firstFeatureTitle = APIObject.pageContent[0].featureTitleA;
    this.firstFeatureContent = APIObject.pageContent[0].featureContentA;
    this.firstFeatureImage = APIObject.pageContent[0].featureImageA;
    this.firstFeatureImageTitle = APIObject.pageContent[0].featureImageTitleA;
    this.firstFeatureImageDescription = APIObject.pageContent[0].featureImageDescriptionA;
    this.secondFeatureTitle = APIObject.pageContent[0].featureTitleB;
    this.secondFeatureContent = APIObject.pageContent[0].featureContentB;
    this.secondFeatureImage = APIObject.pageContent[0].featureImageB;
    this.secondFeatureImageTitle = APIObject.pageContent[0].featureImageTitleB;
    this.secondFeatureImageDescription = APIObject.pageContent[0].featureImageDescriptionB;
    this.currencyImage = APIObject.pageContent[0].currencyImage;
    this.currencyContent = APIObject.pageContent[0].currencyContent;
  }
}
