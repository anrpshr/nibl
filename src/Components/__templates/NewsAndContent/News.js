import React from 'react';

import CardImageAndText from '../../CardImageAndText';
import Styles from './newsContent.module.scss';

const News = ({ news }) => {
  const newsList = news.map((post, index) => (
    <div key={`news-${index}`} className={Styles.card}>
      <CardImageAndText
        image={post.image}
        title={post.title}
        excerpt={post.excerpt}
        link={`/news-content/${post.id}`}
      />
    </div>
  ));

  return (
    <div>
      <div className="heading">
        <h1>Latest News</h1>
      </div>
      <div className={Styles.cardWrapper}>
        {newsList}
      </div>
    </div>
  );
};

export default News;
