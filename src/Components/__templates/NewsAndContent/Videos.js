import React, { Component } from 'react';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

import { API } from '../../../constants';
import { getV2 } from '../../../services/generalApi.services';
import VideoModal from '../../VideoModal';
import Styles from './newsContent.module.scss';
import VideoTransformer from './videos.transformer';

class Videos extends Component {
  state = {
    videos: [],
    loading: true
  };

  componentDidMount() {
    console.log(this.props);
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.VIDEOS, { pageId }))
      .then((content) => {
        const videos = content.pageContent.map(e => new VideoTransformer(e));
        this.setState({ videos, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch videos', e);
      });
  }

  render() {
    const { videos, loading } = this.state;
    const videosList = videos.map(video => (
      <div className={Styles.videoCardWrapper}>
        <div className={Styles.videoCard} key={`video-${video.id}`}>
          <VideoModal videoId="9xwazD5SyVg"/>
        </div>
      </div>
    ));

    return (
      <div className={Styles.videosWrapper}>
        <div className="heading">
          <h1>Videos</h1>
        </div>
        {
          loading
            ? (
              <div className="page-loader __transparent">
                <Spin/>
              </div>
            )
            : videosList
        }
      </div>
    );
  }
}

export default Videos;
