import React, { Component } from 'react';
import { Spin } from 'antd';
import { generatePath } from 'react-router';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import News from './News';
import Videos from './Videos';
import Process from './Process';
import Guidence from './Guidence';
import { getV2 } from '../../../services/generalApi.services';
import { API } from '../../../constants';
import Transformer from './news.transformer';
import Styles from './newsContent.module.scss';

class NewsAndContent extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    bannerAlt: '',
    items: [
      {
        name: 'News',
        key: 'news'
      },
      {
        name: 'Videos',
        key: 'videos'
      },
      {
        name: 'Process',
        key: 'process'
      },
      {
        name: 'Guidance',
        key: 'guidence'
      }
    ],
    activeItemKey: 'news',
    news: [],
    loading: true
  }

  componentDidMount() {
    const { pageId } = this.props;
    getV2(generatePath(API.endPoints.NEWS, { pageId }))
      .then((content) => {
        const transformedContent = new Transformer(content);
        this.setState({ ...transformedContent, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch News.', e);
      });
  }

  render() {
    const {
      items,
      activeItemKey,
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      news,
      loading
    } = this.state;
    const { pageId } = this.props;

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDescription}
                  alt={bannerAlt}
                />
                <div className="white-gradient main--content">
                  <div className="container">
                    <TabNav
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      activeItemKey={activeItemKey}
                      items={items}
                    />
                    <TabContent>
                      <News news={news} isActive={activeItemKey === 'news'}/>
                      <Videos pageId={pageId} isActive={activeItemKey === 'videos'}/>
                      <Process isActive={activeItemKey === 'process'}/>
                      <Guidence isActive={activeItemKey === 'guidence'}/>
                    </TabContent>
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default NewsAndContent;
