export default class VideoTransformer {
  constructor(APIVideoObject) {
    this.youtubeId = APIVideoObject.youtubeLinkId;
    this.id = APIVideoObject.contentId;
    this.name = APIVideoObject.name;
  }
}
