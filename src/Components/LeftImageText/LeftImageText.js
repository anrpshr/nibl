import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Row, Col } from 'antd';

import Image from '../Image';
import Styles from './leftimagetext.module.scss';

const LeftImageText = ({ title, children, image, excerpt, redTitle, imageHeight, imageAlt }) => (
  <div className={classnames(Styles.wrapper, { 'red-title': redTitle })}>
    <div className={classnames(Styles.section, '_section')}>
      <Row type="flex" gutter={30}>
        <Col span={8}>
          <div className={Styles.leftSection}>
            <Image src={image} height={imageHeight} alt={imageAlt}/>
          </div>
        </Col>
        <Col span={16}>
          <div className={Styles.rightSection}>
            <span className={Styles.longTitle} dangerouslySetInnerHTML={{ __html: excerpt }}/>
            <span className={classnames(Styles.title, '_title')}>{title}</span>
            {children}
          </div>
        </Col>
      </Row>
    </div>
  </div>
);

LeftImageText.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired,
  excerpt: PropTypes.string,
  redTitle: PropTypes.bool,
  imageAlt: PropTypes.string,
  imageHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

LeftImageText.defaultProps = {
  excerpt: '',
  redTitle: false,
  imageHeight: 'auto',
  imageAlt: ''
};

export default LeftImageText;
