import React, { Component } from 'react';
import handleViewPort from 'react-in-viewport';
import classnames from 'classnames';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';

import ButtonRedBorder from '../ButtonRedBorder';

import Styles from './forex.module.scss';
// Images
import FlagUsa from '../../assets/img/US.png';
import FlagUk from '../../assets/img/flag-gbp.png';
import FlagAus from '../../assets/img/AUD.png';
import FlagEuro from '../../assets/img/flag-eur.png';

const NextArrow = ({ className, style, onClick }) => (
  <button
    className={className}
    style={{ ...style, display: 'block', background: 'none' }}
    onClick={onClick}
  >
    <ChevronRight />
  </button>
);

const PrevArrow = ({ className, style, onClick }) => (
  <button
    className={className}
    style={{ ...style, display: 'block', background: 'none' }}
    onClick={onClick}
  >
    <ChevronLeft />
  </button>
);

class ForexSlider extends Component {
  state = {
    slider: []
  };

  componentDidMount() {
    this.setState(
      {
        slider: [
          {
            flag: FlagUsa,
            currency: 'USD',
            buying: '115.89',
            selling: '116.12'
          },
          {
            flag: FlagUk,
            currency: 'GBP',
            buying: '129.21',
            selling: '129.99'
          },
          {
            flag: FlagAus,
            currency: 'AUD',
            buying: '89.19',
            selling: '90.22'
          },
          {
            flag: FlagEuro,
            currency: 'Euro',
            buying: '110.89',
            selling: '111.12'
          },
          {
            flag: FlagEuro,
            currency: 'Euro',
            buying: '110.89',
            selling: '111.12'
          },
          {
            flag: FlagEuro,
            currency: 'Euro',
            buying: '110.89',
            selling: '111.12'
          }
        ]
      }
    );
  }

  render() {
    const { enterCount } = this.props;
    const classes = classnames(Styles.forexSliderWrapper, {
      'animated fadeIn': enterCount > 0
    });
    console.log(classes);
    const { slider } = this.state;
    const slides = slider.map((slide, index) => (
      <div key={`slide-${index}`} className={Styles.slide}>
        <img src={slide.flag} alt="" />
        <div className={Styles.forexValue}>
          <span>{slide.currency}</span>
          <span>
            Buying:
            {slide.buying}
          </span>
          <span>
            Selling:
            {slide.selling}
          </span>
        </div>
      </div>
    ));
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    };
    return (
      <div className={classes} style={{ animationDelay: '.15s' }}>
        <div className="heading">
          <h1>Forex</h1>
        </div>
        <div className={Styles.forexSlider}>
          <Slider {...settings}>
            {slides}
          </Slider>
        </div>
        <div className={Styles.more}>
          <ButtonRedBorder title="View"/>
        </div>
      </div>
    );
  }
}

ForexSlider.propTypes = {
  enterCount: PropTypes.number.isRequired
};

NextArrow.propTypes = {
  className: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  onClick: PropTypes.func
};

NextArrow.defaultProps = {
  className: '',
  style: {},
  onClick: null
};

PrevArrow.propTypes = {
  className: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  onClick: PropTypes.func
};

PrevArrow.defaultProps = {
  className: '',
  style: {},
  onClick: null
};

export default handleViewPort(ForexSlider, { rootMargin: '-15px' });
