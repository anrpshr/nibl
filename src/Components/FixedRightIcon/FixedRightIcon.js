import React, { Component } from 'react';
import { GoLocation } from 'react-icons/go';
import { MdPhone } from 'react-icons/all';
import { AiOutlineCalculator } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { Tooltip } from 'antd';

import store from '../../redux';
import { setSplashOpen } from '../../redux/actions/common.actions';
import Styles from './styles.module.scss';

/**
 * @visibleName Fixed Right Icons
 */

class FixedRightIcon extends Component {
  state = {
    concatSlideList: true
  }

  openSplashModal = (slideIndex) => {
    store.dispatch(setSplashOpen(slideIndex));
  }

  render() {
    const { concatSlideList } = this.state;

    return (
      <div className={Styles.outer}>
        <div className={Styles.wrapper}>
          <Tooltip title="Location" placement="left">
            <Link to="/personal-banking/services/atm" className={Styles.iconWrapper}>
              <div className={Styles.icon}>
                <GoLocation/>
              </div>
            </Link>
          </Tooltip>
          <Tooltip title="Contact" placement="left">
            <Link to="/support/contact-form" className={Styles.iconWrapper}>
              <div className={Styles.icon}>
                <MdPhone/>
              </div>
            </Link>
          </Tooltip>
          <Tooltip title="Calculator" placement="left">
            <Link to="/personal-banking/tools-and-calculator" className={Styles.iconWrapper}>
              <div className={Styles.icon}>
                <AiOutlineCalculator/>
              </div>
            </Link>
          </Tooltip>
        </div>
        <div className={Styles.info}>
          <div
            className={Styles.button}
            onClick={() => { this.openSplashModal(0); }}
            role="button"
            tabIndex="-1"
          >
            <span>i</span>
          </div>
          <div className={Styles.submenu}>
            <ul>
              <li>
                <span
                  role="button"
                  tabIndex="-1"
                  onClick={() => { this.openSplashModal(0); }}
                >
                  Green Double Deposit
                </span>
              </li>
              <li>
                <span
                  role="button"
                  tabIndex="-1"
                  onClick={() => { this.openSplashModal(1); }}
                >
                  KYC Form Update
                </span>
              </li>
              <li>
                <span
                  role="button"
                  tabIndex="-1"
                  onClick={() => { this.openSplashModal(2); }}
                >
                  NEA Online Payment
                </span>
              </li>
              {
                concatSlideList
                  ? (
                    <li>
                      <span
                        role="button"
                        tabIndex="-1"
                        onClick={() => { this.setState({ concatSlideList: false }); }}
                      >
                        View More
                      </span>
                    </li>
                  )
                  : (
                    <>
                      <li>
                        <span
                          role="button"
                          tabIndex="-1"
                          onClick={() => { this.openSplashModal(3); }}
                        >
                          Dormant Account Notice
                        </span>
                      </li>
                      <li>
                        <span
                          role="button"
                          tabIndex="-1"
                          onClick={() => { this.openSplashModal(4); }}
                        >
                          Absolute Aesthetics Discount
                        </span>
                      </li>
                    </>
                  )
              }
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default FixedRightIcon;
