import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import ThreeDots from '../ThreeDots';
import styles from './Loading.module.css';

const Loading = ({ color, fullHeight }) => {
  const classes = classnames(styles.wrapper, {
    [styles.full]: fullHeight
  });
  return (
    <div className={classes}>
      <ThreeDots
        variant="flashing"
        color={color}
      />
    </div>
  );
};

Loading.propTypes = {
  color: PropTypes.oneOf(['white', 'red']),
  fullHeight: PropTypes.bool
};

Loading.defaultProps = {
  color: 'white',
  fullHeight: false
};

export default Loading;
