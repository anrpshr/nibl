```js
<ExpansionPanel
  isOrdered
  items={[
    {
      primary: <div>Title</div>,
      secondary: <div>Description</div>
    },
    {
      primary: <div>Title 2</div>,
      secondary: <div>Description 2</div>
    }
  ]}
/>
```