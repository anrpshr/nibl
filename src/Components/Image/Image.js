import React from 'react';
import classnames from 'classnames';

import './Image.scss';

const Image = ({
  src, height, alt
}) => {
  const classes = classnames('image-wrapper');
  return (
    <div className={classes}>
      <img src={src} style={{ minHeight: height }} alt={alt}/>
    </div>
  );
};

export default Image;
