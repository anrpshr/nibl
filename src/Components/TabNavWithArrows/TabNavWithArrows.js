import React, { Component } from 'react';
import classnames from 'classnames';
import { arrayOf, shape, string, func } from 'prop-types';
import LeftArrowIcon from '@material-ui/icons/ChevronLeft';
import RightArrowIcon from '@material-ui/icons/ChevronRight';

import styles from './TabNavWithArrows.module.scss';

class TabNavWithArrows extends Component {
  handleRightClick = () => {
    const { onSelect, items, activeItemId } = this.props;
    const currentItemIndex = items.findIndex(item => item.id === activeItemId);
    const nextIndex = currentItemIndex + 1 === items.length ? 0 : currentItemIndex + 1;
    const nextItem = items[nextIndex];
    onSelect(nextItem.id);
  }

  handleLeftClick = () => {
    const { onSelect, items, activeItemId } = this.props;
    const currentItemIndex = items.findIndex(item => item.id === activeItemId);
    const nextIndex = currentItemIndex === 0 ? items.length - 1 : currentItemIndex - 1;
    const nextItem = items[nextIndex];
    onSelect(nextItem.id);
  }

  render() {
    const { onSelect, items, activeItemId } = this.props;
    const itemList = items.map((item) => {
      const itemClasses = classnames(styles.item, {
        [styles.active]: item.id === activeItemId
      });
      return (
        <div
          key={`tab-nav-arrows-${item.id}`}
          className={itemClasses}
          role="button"
          tabIndex="-1"
          onClick={() => { onSelect(item.id); }}
        >
          <img className={styles.redIcon} src={item.secondIcon} alt=""/>
          <img className={styles.greyIcon} src={item.iconUrl} alt=""/>
          <span>{item.name}</span>
        </div>
      );
    });
    return (
      <div className={styles.tabNavArrows}>
        <div className={styles.arrow}>
          <LeftArrowIcon onClick={this.handleLeftClick}/>
        </div>
        <div className={styles.items}>
          {itemList}
        </div>
        <div className={styles.arrow}>
          <RightArrowIcon onClick={this.handleRightClick}/>
        </div>
      </div>
    );
  }
}

TabNavWithArrows.propTypes = {
  items: arrayOf(shape({
    name: string.isRequired,
    id: string.isRequired,
    iconUrl: string.isRequired,
    secondIcon: string.isRequired
  })).isRequired,
  activeItemId: string.isRequired,
  onSelect: func.isRequired
};

export default TabNavWithArrows;
