```js
<TabNavWithArrows
  items={[
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'University',
      id: 'university'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Mobile Topup',
      id: 'mobile-topup'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Safety',
      id: 'safety'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Schools',
      id: 'schools'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Internet',
      id: 'internet'
    }
  ]}
  activeItemId="safety"
  onSelect={console.log}
/>
```
```js
<TabNavWithArrows
  items={[
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'University',
      id: 'university'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Mobile Topup',
      id: 'mobile-topup'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Safety',
      id: 'safety'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Schools',
      id: 'schools'
    },
    {
      iconUrl: '../../../src/assets/img/safety.png',
      name: 'Internet',
      id: 'internet'
    }
  ]}
  activeItemId="internet"
  onSelect={console.log}
/>
```