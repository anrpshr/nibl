import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Styles from './style.module.scss';

const BigTitleBlock = ({ title, children, animatedClass, animationDelay }) => (
  <div style={{ animationDelay: { animationDelay } }} className={classnames(animatedClass, Styles.bigTitleBlockWrapper, 'block-big-title', 'white-big-title-block', 'single-block')}>
    <div className={classnames(Styles.title, 'title')}>{title}</div>
    <div className={classnames(Styles.innerBlock, 'arrows')}>
      {children}
    </div>
  </div>
);

BigTitleBlock.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  animatedClass: PropTypes.string.isRequired,
  animationDelay: PropTypes.string.isRequired
};

export default BigTitleBlock;
