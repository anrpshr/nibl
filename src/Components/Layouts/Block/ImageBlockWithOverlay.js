import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Image from '../../Image';
import Styles from './style.module.scss';

const ImageBlockWithOverlay = ({
  title, children, image, animatedClass, animationDelay, link, imageHeight, alt
}) => (
  <div className={classnames(Styles.imageWrapper, 'vertical-image', 'horizontal-image', 'commonWrapper', animatedClass)} style={{ animationDelay }}>
    <Image src={image} height={imageHeight} alt={alt}/>
    <div className={classnames(Styles.overlay, 'mob-overlay')}>
      <h4>{title}</h4>
      <div className={Styles.details}>
        {children}
        {
          link && <a href={link}>Read More</a>
        }
      </div>
    </div>
  </div>
);

ImageBlockWithOverlay.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired,
  animatedClass: PropTypes.string.isRequired,
  animationDelay: PropTypes.string.isRequired
};

export default ImageBlockWithOverlay;
