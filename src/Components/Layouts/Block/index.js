import BigTitleBlock from './BigTitleBlock';
import ImageBlockWithOverlay from './ImageBlockWithOverlay';
import MinimalBlock from './MinimalBlock';

export default MinimalBlock;

export {
  BigTitleBlock,
  ImageBlockWithOverlay
};
