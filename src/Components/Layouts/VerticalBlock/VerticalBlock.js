import React from 'react';
// import PropTypes from 'prop-types';

import Styles from './styles.module.scss';

const VerticalBlock = ({ image }) => (
  <div className={Styles.blockWrapper}>
    <img src={image} alt="" />
    <div className={Styles.content}>
      <h4>Mobile (Sms) Banking</h4>
      <p>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
        Aenean commodo ligula eget dolor. Aenean massa Cum sociis
        natoque penatibus et magnis dis parturient montes.
        <a href="a">more</a>
      </p>
    </div>
  </div>
);

// VerticalBlock.propTypes = {
// };

export default VerticalBlock;
