import React from 'react';
import Styles from './tableHeader.module.scss';

/**
* @visibleName Table
* */

const TableHeader = () => (
  <div className={Styles.tableWrapper}>
    <div className={Styles.tableHeadWrapper}>
      <div className={Styles.tableHead}>
        Sno
      </div>
      <div className={Styles.tableHead}>
        Details
      </div>
      <div className={Styles.tableHead}>
        Payment
      </div>
    </div>
  </div>
);

export default TableHeader;
