import React from 'react';
import Search from '@material-ui/icons/Search';
import Styles from './locationListing.module.scss';


// Components
import ExpansionPanel from '../ExpansionPanel';

const LocationListing = () => (
  <div className={Styles.wrapper}>
    <div className={Styles.search}>
      <input type="text"/>
      <div className={Styles.icon}>
        <Search/>
      </div>
    </div>
    <div className={Styles.generalTexts}>
      <span>A-Z</span>
      <span className={Styles.redText}>Find the nearest Atm Location</span>
    </div>
    <div className={Styles.expansionPanel}>
      <ExpansionPanel
        isOrdered={false}
        compact
        items={[
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          },
          {
            primary: <div>NIBL Head Office</div>,
            secondary:
            (
              <div className={Styles.secondary}>
                <span>Durbar Marg, P.O. Box 3412</span>
                <span>Tel: 4228229, 4242530 </span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
                <span>Fax: 977-1-4226349, 4228927</span>
              </div>
            )
          }
        ]}
      />
    </div>
  </div>
);

export default LocationListing;
