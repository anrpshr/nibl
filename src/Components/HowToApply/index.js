import React from 'react';

import Styles from './styles.module.scss';

const HowToApply = ({
  questions
}) => {
  const list = questions.map(question => (
    <div className={Styles.sideBlock}>
      <span className={Styles.title}>{question.question}</span>
      <div className={Styles.content}>
        <div dangerouslySetInnerHTML={{ __html: question.answer }}/>
      </div>
      <div className={Styles.link}>
        {
          // TODO uncomment after API fix
          // question.link && <a href={question.link}>Apply Now</a>
        }
      </div>
    </div>
  ));
  return (
    <div className={Styles.gridBlock}>
      <div className={Styles.headingBlock}>
        <span>How To Apply</span>
      </div>
      {list}
    </div>
  );
};

export default HowToApply;
