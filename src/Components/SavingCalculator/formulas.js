/* eslint-disable max-len */
export const EMI = (amount, time, rate) => amount * ((rate / 1200) * Math.pow((1 + (rate / 1200)), (time * 12))) / (Math.pow((1 + (rate / 1200)), (time * 12)) - 1);
export const RD = (amount, time, rate) => (amount * ((Math.pow((1 + (rate / 400)), (time * 4))) - 1)) / (1 - Math.pow((1 + (rate / 400)), (-1 / 3)));
export const SI = (amount, time, rate) => (amount * time * rate) / 100;
