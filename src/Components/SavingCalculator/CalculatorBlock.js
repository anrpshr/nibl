import React, { Component } from 'react';
import Slider from 'rc-slider';
import cn from 'classnames';

import { BigTitleBlock } from '../Layouts/Block';
import './SavingCalculator.scss';
import 'rc-slider/assets/index.css';

class SavingCalculator extends Component {
  savingsMin = 10;

  savingsMax = 10000;

  timeMin = 1; // months

  timeMax = 120;

  interestMin = 0.01;

  interestMax = 5;

  state = {
    savingsAmount: this.savingsMin,
    timeAmount: this.timeMin,
    interestAmount: this.interestMin,
    activeOption: 'savings' // oneof - savings, time, interest
  };


  render() {
    const { savingsAmount, timeAmount, interestAmount, activeOption } = this.state;
    const timeYear = timeAmount / 12;
    const timeMonth = timeAmount % 12;
    let resultTitle = 'Your savings goal could be';
    if (activeOption === 'time') {
      resultTitle = 'To reach your savings goal it will take:';
    }
    if (activeOption === 'intereset') {
      resultTitle = 'You need to deposit this much to reach your goal:';
    }
    return (
      <div className="savings-calculator">
        <div className="main">
          <div className="head">
            <div
              className={cn('head-tab', { active: activeOption === 'savings' })}
              onClick={() => this.setState({ activeOption: 'savings' })}
              role="button"
              tabIndex="-1"
            >
              How much could I save?
            </div>
            <div
              className={cn('head-tab', { active: activeOption === 'time' })}
              onClick={() => this.setState({ activeOption: 'time' })}
              role="button"
              tabIndex="-1"
            >
              How long will it take?
            </div>
            <div
              className={cn('head-tab', { active: activeOption === 'interest' })}
              onClick={() => this.setState({ activeOption: 'interest' })}
              role="button"
              tabIndex="-1"
            >
              Monthly Calculations
            </div>
          </div>
          <div className="body">
            <div className="savings">
              Savings: Rs. <span className="text-label">{savingsAmount}</span> per month
              <Slider
                value={savingsAmount}
                min={this.savingsMin}
                max={this.savingsMax}
                onChange={(val) => { this.setState({ savingsAmount: val }); }}
              />
              <div className="range">
                <span>Rs. {this.savingsMin}</span>
                <span>Rs. {this.savingsMax}</span>
              </div>
            </div>
            <br/>
            <div className="time">
              Time: Rs. <span className="text-label">{parseInt(timeYear, 10)}</span> years <span className="text-label">{timeMonth}</span> months
              <Slider
                value={timeAmount}
                min={this.timeMin}
                max={this.timeMax}
                onChange={(val) => { this.setState({ timeAmount: val }); }}
              />
              <div className="range">
                <span>{this.timeMin} month</span>
                <span>{parseInt((this.timeMax / 12), 10)} years</span>
              </div>
            </div>
            <br/>
            <div className="interest">
              Interest: <span className="text-label">{interestAmount}</span> %
              <Slider
                value={interestAmount}
                min={this.interestMin}
                max={this.interestMax}
                step={0.01}
                onChange={(val) => { this.setState({ interestAmount: val }); }}
              />
              <div className="range">
                <span>{this.interestMin} %</span>
                <span>{this.interestMax} %</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SavingCalculator;
