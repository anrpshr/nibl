import React, { Component } from 'react';
import handleViewport from 'react-in-viewport';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import cn from 'classnames';
import { Select } from 'antd';

import { EMI, RD } from './formulas';
import { BigTitleBlock } from '../Layouts/Block';
import './SavingCalculator.scss';
import 'rc-slider/assets/index.css';

const { Option } = Select;

class SavingCalculator extends Component {
  amountMin = 1000;

  amountMax = 100000;

  timeMin = 1;

  timeMax = 30;

  interestMin = 1;

  interestMax = 30;

  state = {
    amount: {
      emi: this.amountMin,
      rd: this.amountMin
    },
    timeAmount: {
      emi: this.timeMin,
      rd: this.timeMin
    },
    interestAmount: {
      emi: this.interestMin,
      rd: this.interestMin
    },
    timeUnit: 'y',
    activeOption: 'emi' // oneof - emi, rd, si
  };

  render() {
    const {
      amount,
      timeAmount,
      interestAmount,
      activeOption,
      timeUnit
    } = this.state;
    const { enterCount, compact } = this.props;
    
    const time = timeUnit === 'm' ? timeAmount[activeOption] / 12 : timeAmount[activeOption];

    let answer = 0;

    const amountError = amount[activeOption] < this.amountMin || amount[activeOption] > this.amountMax || amount[activeOption] === '';
    let timeError = false;
    if (timeUnit === 'y') {
      timeError = timeAmount[activeOption] < this.timeMin || timeAmount[activeOption] > this.timeMax;
    } else {
      timeError = timeAmount[activeOption] < this.timeMin * 12 || timeAmount[activeOption] > this.timeMax * 12;
    }

    const rateError = interestAmount[activeOption] < this.interestMin || interestAmount[activeOption] > this.interestMax;

    if (amountError || timeError || rateError) {
      answer = null;
    } else if (activeOption === 'emi') {
      answer = EMI(amount.emi, time, interestAmount.emi);
    } else if (activeOption === 'rd') {
      answer = RD(amount.rd, time, interestAmount.rd);
    }

    const clacClasses = cn('main', {
      'animated fadeIn': enterCount > 0
    });
    const resultClasses = cn('result', {
      'animated fadeIn': enterCount > 0
    });
    const wrapperClasses = cn('savings-calculator', {
      compact
    });
    let resultTitle = 'Your savings goal could be';
    if (activeOption === 'rd') {
      resultTitle = 'To reach your savings goal it will take:';
    }
    return (
      <div className={wrapperClasses}>
        <div className={clacClasses} style={{ animationDelay: '.3s' }}>
          <div className="head">
            <div
              className={cn('head-tab', { active: activeOption === 'emi' })}
              onClick={() => this.setState({ activeOption: 'emi' })}
              role="button"
              tabIndex="-1"
            >
              EMI
            </div>
            <div
              className={cn('head-tab', { active: activeOption === 'rd' })}
              onClick={() => this.setState({ activeOption: 'rd' })}
              role="button"
              tabIndex="-1"
            >
              Recurring Deposits
            </div>
            {/* <div
              className={cn('head-tab', { active: activeOption === 'si' })}
              onClick={() => this.setState({ activeOption: 'si' })}
              role="button"
              tabIndex="-1"
            >
              Simple Interest
            </div> */}
          </div>
          <div className="body">
            <div className="savings">
              Amount: Rs.
              <input
                className="text-label"
                value={amount[activeOption]}
                onChange={(e) => {
                  const val = e.target.value;
                  this.setState((prevState) => {
                    const newAmount = { ...prevState.amount };
                    newAmount[activeOption] = val === '' ? '' : parseInt(val, 10);
                    return {
                      amount: newAmount
                    };
                  });
                }}
              />
              {
                amountError && <p className="error">Invalid Amount</p>
              }
              <Slider
                value={amount[activeOption]}
                min={this.amountMin}
                max={this.amountMax}
                onChange={val => this.setState((prevState) => {
                  const newAmount = { ...prevState.amount };
                  newAmount[activeOption] = val;
                  return { amount: newAmount };
                })}
              />
              <div className="range">
                <span>Rs. {this.amountMin}</span>
                <span>Rs. {this.amountMax}</span>
              </div>
            </div>
            <br/>
            <div className="time">
              Time:
              <input
                type="number"
                className="text-label"
                value={timeAmount[activeOption]}
                style={{ width: 90 }}
                onChange={(e) => {
                  const val = parseInt(e.target.value, 10);
                  this.setState((prevState) => {
                    const newTime = { ...prevState.timeAmount };
                    newTime[activeOption] = val;
                    return {
                      timeAmount: newTime
                    };
                  });
                }}
              />
              <Select
                defaultValue={timeUnit}
                onChange={(val) => {
                  const newTimeAmount = val === 'y' ? Math.floor(timeAmount[activeOption] / 12) : timeAmount[activeOption] * 12;
                  this.setState((prevState) => {
                    const newTime = { ...prevState.timeAmount };
                    newTime[activeOption] = newTimeAmount;
                    return {
                      timeAmount: newTime,
                      timeUnit: val
                    };
                  });
                }}
              >
                <Option value="y">Years</Option>
                <Option value="m">Months</Option>
              </Select>
              {
                timeError && <p className="error">Invalid Time</p>
              }
              <Slider
                value={timeAmount[activeOption]}
                min={timeUnit === 'y' ? this.timeMin : this.timeMin * 12}
                max={timeUnit === 'y' ? this.timeMax : this.timeMax * 12}
                onChange={val => this.setState((prevState) => {
                  const newTime = { ...prevState.timeAmount };
                  newTime[activeOption] = val;
                  return { timeAmount: newTime };
                })}
              />
              <div className="range">
                <span>{timeUnit === 'y' ? this.timeMin : this.timeMin * 12} {timeUnit}</span>
                <span>{timeUnit === 'y' ? this.timeMax : this.timeMax * 12} {timeUnit}</span>
              </div>
            </div>
            <br/>
            <div className="interest">
              Rate:
              <input
                className="text-label"
                value={interestAmount[activeOption]}
                style={{ width: 45 }}
                onChange={(e) => {
                  const val = parseInt(e.target.value, 10);
                  this.setState((prevState) => {
                    const newRate = { ...prevState.interestAmount };
                    newRate[activeOption] = val;
                    return { interestAmount: newRate };
                  });
                }}
              /> %
              {
                rateError && <p className="error">Invalid Interest Rate</p>
              }
              <Slider
                value={interestAmount[activeOption]}
                min={this.interestMin}
                max={this.interestMax}
                step={1}
                onChange={val => this.setState((prevState) => {
                  const newInterest = { ...prevState.interestAmount };
                  newInterest[activeOption] = val;
                  return { interestAmount: newInterest };
                })}
              />
              <div className="range">
                <span>{this.interestMin} %</span>
                <span>{this.interestMax} %</span>
              </div>
            </div>
          </div>
          <div className="calc-bottom">
            EMI Calculation: Rs. <span className="text-label _result">{ answer ? answer.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '--' }</span>
          </div>
        </div>
        {
          !compact && (
            <div className={resultClasses} style={{ animationDelay: '.6s' }}>
              <BigTitleBlock title={resultTitle}>
                {answer.toFixed(2)}
              </BigTitleBlock>
            </div>
          )
        }
      </div>
    );
  }
}

SavingCalculator.propTypes = {
  enterCount: PropTypes.number.isRequired,
  compact: PropTypes.bool
};

SavingCalculator.defaultProps = {
  compact: false
};

export default handleViewport(SavingCalculator);
