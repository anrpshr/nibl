import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import Styles from './cardImageAndText.module.scss';

const CardImageAndText = ({ image, title, excerpt, link }) => (
  <div className={Styles.wrapper}>
    <img src={image} alt=""/>
    <div className={classnames(Styles.cardText, '_card-text')}>
      <div className={Styles.title}>
        <span>{title}</span>
      </div>
      <p>
        {excerpt}
      </p>
    </div>
    <div className={Styles.link}>
      <Link to={link}>Read more</Link>
    </div>
  </div>
);

CardImageAndText.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  excerpt: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
};

export default CardImageAndText;
