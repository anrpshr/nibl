import React from 'react';
import PropTypes from 'prop-types';
import Styles from './leftTextImage.module.scss';

const LeftTextImage = ({ title, image, children }) => (
  <div className={Styles.wrapper}>
    <div className={Styles.section}>
      <div className={Styles.leftSection}>
        <span className={Styles.blockHeading}>{title}</span>
        {children}
      </div>
      <div className={Styles.rightSection}>
        <img src={image} alt=""/>
      </div>
    </div>
  </div>
);

LeftTextImage.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired
};

export default LeftTextImage;
