import React from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';

const SlickSlider = ({ settings, children }) => (
  <Slider {...settings}>
    {children}
  </Slider>
);

SlickSlider.propTypes = {
  settings: PropTypes.objectOf(PropTypes.any).isRequired,
  children: PropTypes.node.isRequired
};

export default SlickSlider;
