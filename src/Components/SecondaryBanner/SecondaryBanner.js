import React from 'react';
import PropTypes from 'prop-types';
import Styles from './secondaryBanner.module.scss';

// Components
import ButtonRedBorder from '../ButtonRedBorder';

const SecondaryBanner = ({ background, image, title, description }) => (
  <div className={Styles.wrapper}>
    <img src={background} alt=""/>
    <div className={Styles.flexSection}>
      <div className={Styles.block}>
        <img src={image} alt=""/>
      </div>
      <div className={Styles.blockRight}>
        <h1>{title}</h1>
        <div className={Styles.description}>
          <p>{description}</p>
        </div>
        <ButtonRedBorder title="Contact Now"/>
      </div>
    </div>
  </div>
);

SecondaryBanner.propTypes = {
  background: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

export default SecondaryBanner;
