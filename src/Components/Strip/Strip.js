import React from 'react';
import classnames from 'classnames';
import { bool, string } from 'prop-types';

import Styles from './strip.module.scss';

const Strip = ({ noInput, text }) => (
  <div className={classnames(Styles.wrapper, 'greyback')}>
    <div className={Styles.flexSection}>
      <div className={Styles.block}>
        <span>{text}</span>
      </div>
      <div className={Styles.block}>
        {
          noInput
            ? <button className={Styles.button}>Download</button>
            : <input type="text" placeholder="Enter your email"/>
        }
      </div>
    </div>
  </div>
);

Strip.propTypes = {
  noInput: bool,
  text: string
};

Strip.defaultProps = {
  noInput: false,
  text: ''
};

export default Strip;
