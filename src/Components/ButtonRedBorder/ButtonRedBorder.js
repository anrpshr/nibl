import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Styles from './buttonRedborder.module.scss';

/**
 * @visibleName Red border button
 */

const ButtonRedBorder = ({ title, onClick }) => (
  <div className={classnames(Styles.wrapper, 'common-class-btn')}>
    <button onClick={onClick}>{title}</button>
  </div>
);

ButtonRedBorder.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

ButtonRedBorder.defaultProps = {
  onClick: () => {}
};

export default ButtonRedBorder;