import React from 'react';
import moment from 'moment';

import Styles from './stockBlockStyles.module.scss';

import stockImage from '../../assets/img/stock_g.png';

const StockBlock = () => (
  <>
    <div className={Styles.back}>
      <div className={Styles.date}>
        On {moment().format('dddd, MMMM DD, YYYY')}
      </div>
      <table className={Styles.table}>
        <tbody>
          <tr>
            <td>Paid Up Value Nrs.</td>
            <td>100</td>
          </tr>
          <tr>
            <td>Closing Nrs.</td>
            <td>528.00</td>
          </tr>
          <tr>
            <td>Listed Shares</td>
            <td>106,264,343.00</td>
          </tr>
        </tbody>
      </table>

      <div className={Styles.stockGraph}>
        <a href="http://www.nepalstock.com/company/display/132" target="_blank" rel="noopener noreferrer">
          <img src={stockImage} alt=""/>
        </a>
      </div>
    </div>
  </>
);

export default StockBlock;
