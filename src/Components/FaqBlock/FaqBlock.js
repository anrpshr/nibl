import React from 'react';
import Styles from './faqBlock.module.scss';

// Components
import Accordian from '../Accordian';

const FaqBlock = () => (
  <div className={Styles.wrapper}>
    <div className={Styles.heading}>
      <h1>FAQ</h1>
    </div>
    <Accordian/>
  </div>
);

export default FaqBlock;
