import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Styles from './accordian.module.scss';

// Images
import downloadIcon from '../../assets/img/download.png';

const styles = {
  paper: {
    marginBottom: 0,
    marginTop: 18
  }
};

const Accordian = ({ classes }) => (
  <div className={classes.root}>
    <ExpansionPanel className={classes.paper}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <div className={Styles.flexWrapper}>
          <span>1</span>
          <span>Title 1</span>
          <a href="1">
            <img src={downloadIcon} alt=""/>
          </a>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
          sit amet blandit leo lobortis eget.
        </p>
      </ExpansionPanelDetails>
    </ExpansionPanel>
    <ExpansionPanel className={classes.paper}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <div className={Styles.flexWrapper}>
          <span>2</span>
          <span>Expansion Panel 2</span>
          <a href="1">
            <img src={downloadIcon} alt=""/>
          </a>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
          sit amet blandit leo lobortis eget.
        </p>
      </ExpansionPanelDetails>
    </ExpansionPanel>
    <ExpansionPanel className={classes.paper}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <div className={Styles.flexWrapper}>
          <span>3</span>
          <span>Another title</span>
          <a href="1">
            <img src={downloadIcon} alt=""/>
          </a>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
          sit amet blandit leo lobortis eget.
        </p>
      </ExpansionPanelDetails>
    </ExpansionPanel>
    <ExpansionPanel className={classes.paper}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <div className={Styles.flexWrapper}>
          <span>4</span>
          <span>Title 3</span>
          <a href="1">
            <img src={downloadIcon} alt=""/>
          </a>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
          sit amet blandit leo lobortis eget.
        </p>
      </ExpansionPanelDetails>
    </ExpansionPanel>
    <ExpansionPanel className={classes.paper}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <div className={Styles.flexWrapper}>
          <span>5</span>
          <span>A long title a long title</span>
          <a href="1">
            <img src={downloadIcon} alt=""/>
          </a>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
          sit amet blandit leo lobortis eget.
        </p>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  </div>
);

Accordian.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

export default withStyles(styles)(Accordian);
