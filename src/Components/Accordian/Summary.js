import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const Summary = ({ children }) => (
  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
    {children}
  </ExpansionPanelSummary>
);

Summary.propTypes = {
  children: PropTypes.node.isRequired
};

export default Summary;
