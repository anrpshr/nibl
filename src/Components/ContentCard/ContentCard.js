import React from 'react';
import PropTypes from 'prop-types';

import './ContentCard.scss';

/**
 * @visibleName Content Card
 */

const ContentCard = ({ children }) => (
  <div className="content-card grey-content-card">
    {children}
  </div>
);

ContentCard.propTypes = {
  children: PropTypes.node.isRequired
};

export default ContentCard;
