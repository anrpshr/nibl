```js
<ContentCard>
<h1>Si enim ita est, vide ne facinus facias, cum mori suadeas.</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Sed ille, ut dixi, vitiose. </p>

<ol>
	<li>Nec vero hoc oratione solum, sed multo magis vita et factis et moribus comprobavit.</li>
	<li>Nihil opus est exemplis hoc facere longius.</li>
	<li>Haec et tu ita posuisti, et verba vestra sunt.</li>
	<li>Oratio me istius philosophi non offendit;</li>
</ol>


<h2>Videsne, ut haec concinant?</h2>

<p>Faceres tu quidem, Torquate, haec omnia; Quo modo autem philosophus loquitur? Duo Reges: constructio interrete. An hoc usque quaque, aliter in vita? Quae contraria sunt his, malane? Qui convenit? </p>

<p>Tanta vis admonitionis inest in locis; Quod cum dixissent, ille contra. Poterat autem inpune; Primum in nostrane potestate est, quid meminerimus? </p>
</ContentCard>
```
