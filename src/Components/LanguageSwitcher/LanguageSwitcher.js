import React, { Component } from 'react';
import ExpandIcon from '@material-ui/icons/ExpandMore';
import { connect } from 'react-redux';

import store from '../../redux';
import { setLocaleAndRefresh } from '../../redux/actions/locale.actions';
import './LanguageSwitcher.scss';

const mapStateToProps = ({ locale }) => ({ locale });

class LanguageSwitcher extends Component {
  handleSetLocale = (newLocale) => {
    const { locale } = this.props;
    if (locale.lang === newLocale) return;
    store.dispatch(setLocaleAndRefresh(newLocale));
  }

  render() {
    const { locale } = this.props;
    const { lang } = locale;

    return false;

    return (
      <div className="language-switcher">
        <div className="active">
          { lang === 'en' ? 'EN' : 'NP' }
          <ExpandIcon color="primary"/>
        </div>
        <div className="more">
          <ul>
            <li role="button" tabIndex="-1" onClick={() => { this.handleSetLocale('en'); }}>EN</li>
            <li role="button" tabIndex="-1" onClick={() => { this.handleSetLocale('ne'); }}>NP</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(LanguageSwitcher);
