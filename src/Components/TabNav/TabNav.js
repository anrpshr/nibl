import React, { Component } from 'react';
import PropTypes from 'prop-types';
import anime from 'animejs';
import classnames from 'classnames';
import RightArrow from '@material-ui/icons/ChevronRight';
import LeftArrow from '@material-ui/icons/ChevronLeft';

import './TabNav.scss';

class TabNav extends Component {
  constructor() {
    super();
    this.itemsRef = React.createRef();
    this.indicatorRef = React.createRef();
    this.itemsContainerRef = React.createRef();
    this.itemsRefOffset = 0;
  }

  componentDidMount() {
    if (this.itemsRef.current) {
      this.animateIndicator();
    }
  }

  componentDidUpdate(prevProps) {
    const { activeItemKey, arrows, items } = this.props;
    if (prevProps.activeItemKey !== activeItemKey) {
      if (!arrows) {
        this.animateIndicator();
      } else {
        const activeItemIndex = items.findIndex(item => item.key === activeItemKey);
        const activeItem = this.itemsRef.current.children[activeItemIndex];
        const navContainerWidth = this.itemsContainerRef.current.getBoundingClientRect().width;
        const activeItemLeftOffset = activeItem.getBoundingClientRect().x;
        const activeItemWidth = activeItem.getBoundingClientRect().width;

        if (activeItemIndex === 0) {
          anime({
            targets: this.itemsRef.current,
            left: 0,
            easing: 'easeOutQuint',
            duration: 500,
            begin: () => {},
            complete: () => {
              this.itemsRefOffset = 0;
            }
          });
        } else if ((activeItemLeftOffset + Math.abs(this.itemsRefOffset) + activeItemWidth) > navContainerWidth) {
          anime({
            targets: this.itemsRef.current,
            left: -1 * (Math.abs(this.itemsRefOffset) + activeItemLeftOffset + activeItemWidth - navContainerWidth),
            easing: 'easeOutQuint',
            duration: 500,
            begin: () => {},
            complete: () => {
              this.itemsRefOffset = -1 * (Math.abs(this.itemsRefOffset) + activeItemLeftOffset + activeItemWidth - navContainerWidth);
            }
          });
        } else if ((activeItemLeftOffset - Math.abs(this.itemsRefOffset)) < 0) {
          anime({
            targets: this.itemsRef.current,
            left: activeItemLeftOffset - Math.abs(this.itemsRefOffset) + 69,
            easing: 'easeOutQuint',
            duration: 500,
            begin: () => {},
            complete: () => {
              this.itemsRefOffset = activeItemLeftOffset - Math.abs(this.itemsRefOffset) + 69;
            }
          });
        }
      }
    }
  }

  handleClick = (key) => {
    const { onClick } = this.props;
    onClick(key);
  }

  animateIndicator = () => {
    const { activeItemKey, items } = this.props;
    if (activeItemKey) {
      const activeItemIndex = items.findIndex(item => item.key === activeItemKey);
      const activeItem = this.itemsRef.current.children[activeItemIndex];
      let leftPosition = 0;
      for (let i = 0; i <= activeItemIndex; i++) {
        if (i !== 0) {
          leftPosition += this.itemsRef.current.children[i - 1].clientWidth + 40;
        } else {
          leftPosition = 20;
        }
      }
      anime({
        targets: this.indicatorRef.current,
        width: activeItem.clientWidth,
        left: leftPosition,
        easing: 'easeOutQuint',
        duration: 500,
        begin: () => {},
        complete: () => {}
      });
    }
  }

  render() {
    const { items, arrows, stretch, onLeftArrowClick, onRightArrowClick, activeItemKey } = this.props;

    const itemList = items.map(item => (
      <li key={`tab-nav-item-${item.key}`} style={{ display: 'inline-block' }} className={item.key === activeItemKey ? 'active' : ''}>
        {
          item.icon && <img src={item.icon} alt="" />
        }
        <span
          onClick={() => { this.handleClick(item.key); }}
          role="button"
          tabIndex="-1"
        >
          {item.name}
        </span>
      </li>
    ));

    const wrapperClasses = classnames('tab-nav-list tab-nav-left', {
      arrows,
      stretch
    });

    return (
      <div className={wrapperClasses}>
        {
          arrows && (
            <div className="span" role="button" tabIndex="-1" onClick={onLeftArrowClick}>
              <LeftArrow style={{ color: '#E50019' }}/>
            </div>
          )
        }
        {
          arrows
            ? (
              <div className="nav-header" ref={this.itemsContainerRef}>
                <ul ref={this.itemsRef}>
                  {itemList}
                </ul>
              </div>
            )
            : (
              <ul ref={this.itemsRef}>
                {itemList}
              </ul>
            )
        }
        {
          arrows && (
            <div className="span" role="button" tabIndex="-1" onClick={onRightArrowClick}>
              <RightArrow style={{ color: '#E50019' }}/>
            </div>
          )
        }
        {
          !arrows && (
            <div className="nav-indicator-rail">
              <div className="nav-indicator" ref={this.indicatorRef} style={{ left: 0 }}/>
            </div>
          )
        }
      </div>
    );
  }
}

TabNav.propTypes = {
  activeItemKey: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string,
    name: PropTypes.string
  })),
  onClick: PropTypes.func.isRequired,
  arrows: PropTypes.bool,
  stretch: PropTypes.bool,
  onLeftArrowClick: PropTypes.func,
  onRightArrowClick: PropTypes.func
};

TabNav.defaultProps = {
  activeItemKey: null,
  arrows: false,
  items: [],
  stretch: false,
  onLeftArrowClick: () => {},
  onRightArrowClick: () => {}
};

export default TabNav;
