```js
<div
style={{
  width: 500,
  margin: 'auto'
}}
>
<TabNav
  onClick={(key) => {console.log(key)}}
  activeItemKey="item-2"
  items={[
    {
      name: 'Item 1',
      key: 'item-1'
    },
    {
      name: 'Item 2',
      key: 'item-2'
    },
    {
      name: 'Item with a long name',
      key: 'long'
    }
  ]}
/>
</div>
```
