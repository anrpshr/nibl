import React from 'react';
import classnames from 'classnames';
import Search from '@material-ui/icons/Search';

import Styles from './searchBar.module.scss';

const SearchBar = () => (
  <div className={classnames(Styles.search, '_search')}>
    <div className={classnames(Styles.icon, 'greyIcon')}>
      <Search/>
    </div>
    <input type="text" placeholder="Search for Jobs"/>
  </div>
);

export default SearchBar;
