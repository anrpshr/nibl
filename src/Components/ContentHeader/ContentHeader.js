import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './ContentHeader.scss';

/**
 * Header for content
 * @visibleName Content Header
 */

const ContentHeader = ({ children, grey }) => {
  const classes = classnames('content-header', {
    grey
  });

  return (
    <div className={classes}>
      {children}
    </div>
  );
};

ContentHeader.propTypes = {
  children: PropTypes.node,
  grey: PropTypes.bool
};

ContentHeader.defaultProps = {
  children: '',
  grey: false
};

export default ContentHeader;
