import React from 'react';
import PropTypes from 'prop-types';
import Styles from './plainblock.module.scss';

/**
 * @visibleName Plain Block
 */

const PlainBlock = ({ icon, title, description, htmlDescription }) => (
  <div className={Styles.wrapper}>
    <div className={Styles.innerWrapper}>
      <img src={icon} alt=""/>
      <span>{title}</span>
      {
        htmlDescription
          ? (
            <div dangerouslySetInnerHTML={{ __html: description }}/>
          )
          : <p>{description}</p>
      }
    </div>
  </div>
);

PlainBlock.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  htmlDescription: PropTypes.bool
};

PlainBlock.defaultProps = {
  htmlDescription: false
};

export default PlainBlock;
