import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import store from '../../../redux';
import setCurrentpage from '../../../redux/actions/currentpage.actions';

const firstChildPath = (routeObj) => {
  if (!routeObj.pageTemplate || routeObj.pageTemplate === 'null') {
    const childRoute = routeObj.routes;
    if (!childRoute || childRoute.length < 0) {
      return '/';
    }

    if (childRoute) {
      return `/${routeObj.slug}${firstChildPath(routeObj.routes[0])}`;
    }

    return `/${routeObj.routes[0].slug}`;
  }

  return `/${routeObj.slug}`;
};

class MenuList extends Component {
  setCurrentPage = (slug) => {
    store.dispatch(setCurrentpage(slug));
  }

  render() {
    const { routes } = this.props;
    const menuList = routes.map((route, i) => {
      if (route.pageTemplate && route.pageTemplate !== 'null') {
        return (
          <NavLink
            onClick={() => { this.setCurrentPage(route.slug); }}
            key={`menu-${route.slug}-${i}`}
            to={`/${route.slug}`}
          >
            {route.name}
          </NavLink>
        );
      }

      return (
        <NavLink
          onClick={() => { this.setCurrentPage(route.slug); }}
          key={`menu-${route.slug}-${i}`}
          to={firstChildPath(route)}
        >
          {route.name}
        </NavLink>
      );
    });
    return (
      menuList
    );
  }
}

MenuList.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default MenuList;
