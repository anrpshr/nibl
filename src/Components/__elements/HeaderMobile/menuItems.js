const menuItems = [
  {
    name: 'Personal',
    link: '/personal',
    children: [
      {
        name: 'Accounts',
        link: '/personal/accounts',
        children: [
          {
            name: 'Keta Keti Bachat Khata',
            link: '/personal/accounts/keta-keti-bachat-khata'
          },
          {
            name: 'Lotus Account',
            link: '/personal/accounts/lotus'
          },
          {
            name: 'E-zee Account',
            link: '/personal/accounts/ezee'
          },
          {
            name: 'Afnai Bachat Khata',
            link: '/persoanl/accounts/afnai-bachat-khata'
          },
          {
            name: 'Students Savings Account',
            link: '/personal/accounts/students-savings-account'
          },
          {
            name: 'Savings Account',
            link: '/personal/accounts/savings-account'
          },
          {
            name: 'Surakchya Khata',
            link: '/personal/accounts/surakchya'
          }
        ]
      }
    ]
  },
  {
    name: 'Corporate',
    link: '/corporate'
  },
  {
    name: 'About Us',
    link: 'about'
  },
  {
    name: 'Remitance',
    link: '/remitance'
  },
  {
    name: 'News and Content',
    link: 'news'
  },
  {
    name: 'Shareholder',
    link: '/shareholder'
  },
  {
    name: 'Contact Us',
    link: '/contact'
  }
];

export default menuItems;
