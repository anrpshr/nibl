import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import RightIcon from '@material-ui/icons/ChevronRight';
import { withStyles } from '@material-ui/core/styles';

import menuItems from './menuItems';
import './MobileMenu.scss';

const styles = {
  paper: {
    marginBottom: 0,
    marginTop: 18
  },
  expandedPanel: {
    marginTop: 0
  },
  panelDetailsRoot: {
    paddingRight: 0,
    paddingLeft: 9
  }
};

const MobileMenu = ({ classes }) => {
  const menu = menuItems.map((item, itemIndex) => {
    const children = item.children && item.children.map((child, index) => {
      const grandChildren = child.children && child.children.map((grandChild, i) => (
        <div className="menu-item" key={`grandchild-${i}`}>
          <Link to={grandChild.link}>{grandChild.name}<RightIcon color="primary"/></Link>
        </div>
      ));
      return (
        <>
          {
            grandChildren
              ? (
                <ExpansionPanel key={`child-${index}`} classes={{ expanded: classes.expandedPanel }}>
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon color="primary"/>}
                    classes={{ expanded: classes.expandedPanel }}
                  >
                    {child.name}
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails classes={{ root: classes.panelDetailsRoot }}>
                    <div className="menu-items">
                      {grandChildren}
                    </div>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              )
              : (
                <div className="menu-item" key={`child-${index}`}>
                  <Link to={child.link}>{child.name}<RightIcon color="primary"/></Link>
                </div>
              )
          }
        </>
      );
    });
    return (
      <>
        {
          children
            ? (
              <ExpansionPanel key={`item-${itemIndex}`} classes={{ expanded: classes.expandedPanel }}>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon color="primary"/>}
                  classes={{ expanded: classes.expandedPanel }}
                >
                  {item.name}
                </ExpansionPanelSummary>
                <ExpansionPanelDetails classes={{ root: classes.panelDetailsRoot }}>
                  {children}
                </ExpansionPanelDetails>
              </ExpansionPanel>
            )
            : (
              <div className="menu-item" key={`item-${itemIndex}`}>
                <Link to={item.link}>{item.name}<RightIcon color="primary"/></Link>
              </div>
            )
        }
      </>
    );
  });
  return (
    <div className="mobile-menu">
      {menu}
    </div>
  );
};

MobileMenu.propTypes = {
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

export default withStyles(styles)(MobileMenu);
