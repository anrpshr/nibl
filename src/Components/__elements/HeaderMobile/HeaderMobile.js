import React, { Component } from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

import sitemap from '../../../routing/siteMap';
import LanguageSwitcher from '../../LanguageSwitcher';
import logo from '../../../assets/img/logo.png';
import ebankingLogo from '../../../assets/img/ebanking.png';
import MobileMenu from './MobileMenu';
import './HeaderMobile.scss';

class HeaderMobile extends Component {
  state = {
    drawerOpen: false
  }

  render() {
    const { drawerOpen } = this.state;
    return (
      <>
        <SwipeableDrawer
          anchor="left"
          open={drawerOpen}
          onClose={() => { this.setState({ drawerOpen: false }); }}
          onOpen={() => { this.setState({ drawerOpen: true }); }}
        >
          <MobileMenu/>
        </SwipeableDrawer>
        <div className="header-small">
          <MenuIcon color="secondary" style={{ fontSize: 45 }} onClick={() => { this.setState({ drawerOpen: true }); }}/>
          <div className="_logo">
            <img src={logo} alt="Nepal Investment Bank Ltd"/>
          </div>
          <div className="_right">
            <LanguageSwitcher/>
            <div className="_search">
              <SearchIcon style={{ fontSize: 35, color: '#fff' }}/>
            </div>
          </div>
        </div>
        <div className="e-banking-mobile">
          <Link to="/">
            <img src={ebankingLogo} alt=""/>
          </Link>
        </div>
      </>
    );
  }
}

export default HeaderMobile;
