import React, { Component } from 'react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import { GoSearch } from 'react-icons/all';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../../redux';
import { showSearchBlock } from '../../../redux/actions/common.actions';
import MenuList from '../MenuList';
import LanguageSwitcher from '../../LanguageSwitcher';
import MainMenu from './MainMenu';
import SearchBlockWrapper from '../../SearchBlock';

import logo from '../../../assets/img/logo.png';
import eBankingIcon from '../../../assets/img/icon-ebanking.png';
import Styles from './style.module.scss';

const mapStateToProps = ({ navigation }) => ({ navigation });

class Header extends Component {
  showSearchBlock = () => {
    store.dispatch(showSearchBlock());
  }

  render() {
    const { navigation } = this.props;
    return (
      <div className={Styles.headerWrapper}>
        <div className={Styles.topNav}>
          <div className="big-container">
            <div className={Styles.topNavFlex}>
              <div className={Styles.leftMenu}>
                <LanguageSwitcher/>
                <div className={Styles.topMenuInner}>
                  <MenuList routes={navigation.primary}/>
                </div>
              </div>
              <div className={Styles.rightMenu}>
                <div className={Styles.topMenuInner}>
                  <MenuList routes={navigation.secondary}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={Styles.bottomNav}>
          <div className="big-container">
            <div className={Styles.btmNavFlex}>
              <div className={Styles.left}>
                <div className={Styles.logo}>
                  <NavLink to="/"><img src={logo} alt="" /></NavLink>
                </div>
                <MainMenu/>
              </div>
              <div className={Styles.right}>
                <Link
                  className="e-banking-link"
                  to="/ebanking"
                >
                  <img src={eBankingIcon} alt=""/>
                  <span>ebanking</span>
                </Link>
                {/* <span
                  className={Styles.icon}
                  role="button"
                  tabIndex="-1"
                  onClick={this.showSearchBlock}
                >
                  <GoSearch/>
                </span> */}
              </div>
            </div>
          </div>
        </div>
        <SearchBlockWrapper/>
      </div>
    );
  }
}

Header.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(withRouter(Header));
