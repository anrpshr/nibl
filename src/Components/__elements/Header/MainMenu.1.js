import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import cn from 'classnames';

import DownIcon from '@material-ui/icons/ExpandMore';

import styles from './style.module.scss';
import girl from '../../../assets/img/girl.png';
import './MainMenu.scss';

class MainMenu extends Component {
  menuExtra = React.createRef();

  render() {
    return (
      <div className={cn(styles.btmMenuInner, 'main-menu')}>
        <ul>
          <li>
            <NavLink to="/accounts" exact>Accounts <DownIcon color="primary"/></NavLink>
            <div className="dropdown">
              <ul>
                <li>
                  <NavLink to="/personal/accounts/keta-keti-bachat-khata">Keta Keti Bachat Khata</NavLink>
                  <div className="submenu" ref={this.menuExtra}>
                    <ul>
                      <li><NavLink to="/personal/accounts/keta-keti-bachat-khata?p=overview">Product Overview</NavLink></li>
                      <li><NavLink to="/personal/accounts/keta-keti-bachat-khata?p=features">Features</NavLink></li>
                      <li><NavLink to="/personal/accounts/keta-keti-bachat-khata?p=lost-stolen-cards">Lost/Stolen Cards</NavLink></li>
                      <li><NavLink to="/personal/accounts/keta-keti-bachat-khata?p=tools-downloads">Tools and Downloads</NavLink></li>
                      <li><NavLink to="/personal/accounts/keta-keti-bachat-khata?p=product-video">Product Video</NavLink></li>
                      <li><NavLink to="/personal/accounts/keta-keti-bachat-khata?p=faq">FAQ</NavLink></li>
                    </ul>
                    <div className="submenu-extra">
                      <div className="menu-image">
                        <img src={girl} alt=""/>
                        <div className="_overlay">
                          <div className="_name">Keta Keti Bachat Khata</div>
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                          <Link to="/">Apply Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li><NavLink to="/personal/accounts/lotus">Lotus Account</NavLink></li>
                <li><NavLink to="/personal/accounts/eezee">E-zee Account</NavLink></li>
                <li><NavLink to="/personal/accounts/afnai-bachat-khata">Afnai Bachat Khata</NavLink></li>
                <li><NavLink to="/personal/accounts/students-savings">Students Savings Account</NavLink></li>
                <li><NavLink to="/personal/accounts/savings">Savings Account</NavLink></li>
                <li><NavLink to="/personal/accounts/surakchya">Surakchya Khata</NavLink></li>
              </ul>
            </div>
          </li>
          <li>
            <NavLink to="/loans" exact>Loans <DownIcon color="primary"/></NavLink>
            <div className="dropdown">
              <ul>
                <li>
                  <NavLink to="/">Home Loan</NavLink>
                  <div className="submenu" ref={this.menuExtra}>
                    <ul>
                      <li><NavLink to="/">Product Overview</NavLink></li>
                      <li><NavLink to="/">Features</NavLink></li>
                      <li><NavLink to="/">Lost/Stolen Cards</NavLink></li>
                      <li><NavLink to="/">Tools and Downloads</NavLink></li>
                      <li><NavLink to="/">Product Video</NavLink></li>
                      <li><NavLink to="/">FAQ</NavLink></li>
                    </ul>
                    <div className="submenu-extra">
                      <div className="menu-image">
                        <img src={girl} alt=""/>
                        <div className="_overlay">
                          <div className="_name">Home Loan</div>
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                          <Link to="/">Apply Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li><NavLink to="/">Education Loan</NavLink></li>
                <li><NavLink to="/">Gold/Silver Loan</NavLink></li>
                <li><NavLink to="/">Auto Loan</NavLink></li>
                <li><NavLink to="/">Cab Loan</NavLink></li>
                <li><NavLink to="/">Solar Loan</NavLink></li>
                <li><NavLink to="/">EMI Scheme</NavLink></li>
              </ul>
            </div>
          </li>
          <li>
            <NavLink to="/creditcard" exact>Credit Cards</NavLink>
          </li>
          <li>
            <NavLink to="/privilegebanking" exact>Privilege Banking <DownIcon color="primary"/></NavLink>
            <div className="dropdown">
              <ul>
                <li>
                  <NavLink to="/">Services</NavLink>
                  <div className="submenu" ref={this.menuExtra}>
                    <div className="submenu-details">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto iure et atque
                      nihil delectus explicabo
                      nulla earum laudantium natus? Eum, itaque illo! Nobis quo saepe, tenetur
                      omnis vero asperiores
                      reiciendis?
                    </div>
                    <div className="submenu-extra">
                      <div className="menu-image">
                        <img src={girl} alt=""/>
                        <div className="_overlay">
                          <div className="_name">Services</div>
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                          <Link to="/">Apply Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li><NavLink to="/">Contact</NavLink></li>
                <li><NavLink to="/">Location</NavLink></li>
              </ul>
            </div>
          </li>
          <li>
            <NavLink to="/personal/services" exact>Services <DownIcon color="primary"/></NavLink>
            <div className="dropdown">
              <ul>
                <li>
                  <NavLink to="/personal/atm-location">ATM</NavLink>
                  <div className="submenu" ref={this.menuExtra}>
                    <div className="submenu-details">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto iure et atque
                      nihil delectus explicabo
                      nulla earum laudantium natus? Eum, itaque illo! Nobis quo saepe, tenetur
                      omnis vero asperiores
                      reiciendis?
                    </div>
                    <div className="submenu-extra">
                      <div className="menu-image">
                        <img src={girl} alt=""/>
                        <div className="_overlay">
                          <div className="_name">ATM</div>
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                          <Link to="/">Apply Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li><NavLink to="/personal/payments">Payments</NavLink></li>
                <li><NavLink to="/personal/mobile-banking">Mobile banking</NavLink></li>
                <li><NavLink to="/personal/lockers">Locker</NavLink></li>
                <li><NavLink to="/personal/branchless-banking">Branchless Banking</NavLink></li>
              </ul>
            </div>
          </li>
          <li>
            <NavLink to="/rates" exact>Rates & Fees <DownIcon color="primary"/></NavLink>
            <div className="dropdown">
              <ul>
                <li>
                  <NavLink to="/personal/downloads">Downloads</NavLink>
                  <div className="submenu" ref={this.menuExtra}>
                    <div className="submenu-details">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto iure et atque
                      nihil delectus explicabo
                      nulla earum laudantium natus? Eum, itaque illo! Nobis quo saepe, tenetur
                      omnis vero asperiores
                      reiciendis?
                    </div>
                    <div className="submenu-extra">
                      <div className="menu-image">
                        <img src={girl} alt=""/>
                        <div className="_overlay">
                          <div className="_name">Downloads</div>
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                          <Link to="/">Apply Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li><NavLink to="/personal/daily-rates">Daily Rates</NavLink></li>
                <li><NavLink to="/personal/interest-rates">Interest Rate</NavLink></li>
              </ul>
            </div>
          </li>
          <li>
            <NavLink to="/tools" exact>Tools & Calculators <DownIcon color="primary"/></NavLink>
            <div className="dropdown">
              <ul>
                <li>
                  <NavLink to="/">Budget Calculator</NavLink>
                  <div className="submenu" ref={this.menuExtra}>
                    <div className="submenu-details">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto iure et atque
                      nihil delectus explicabo
                      nulla earum laudantium natus? Eum, itaque illo! Nobis quo saepe, tenetur
                      omnis vero asperiores
                      reiciendis?
                    </div>
                    <div className="submenu-extra">
                      <div className="menu-image">
                        <img src={girl} alt=""/>
                        <div className="_overlay">
                          <div className="_name">Budget Calculator</div>
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                          <Link to="/">Apply Now</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li><NavLink to="/">Home Loan Calculator</NavLink></li>
                <li><NavLink to="/">Auto Loan Caclulator</NavLink></li>
                <li><NavLink to="/">Insurance Calculator</NavLink></li>
                <li><NavLink to="/">Retirement Calculator</NavLink></li>
              </ul>
            </div>
          </li>
          <li>
            <NavLink to="/bank-assurance" exact>Bank Assurance</NavLink>
          </li>
        </ul>
      </div>
    );
  }
}

export default MainMenu;
