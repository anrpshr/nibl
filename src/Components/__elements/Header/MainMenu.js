import React, { Component } from 'react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import cn from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import DownIcon from '@material-ui/icons/ExpandMore';

import styles from './style.module.scss';
import girl from '../../../assets/img/girl.png';
import './MainMenu.scss';

const mapStateToProps = ({ currentpage, navigation }) => ({ currentpage, navigation });

const matchWorkaround = (path) => (isMatch, location) => isMatch || location.pathname.startsWith(path);

const firstChildPath = (routeObj) => {
  if (!routeObj.pageTemplate || routeObj.pageTemplate === 'null') {
    const childRoute = routeObj.routes;
    if (!childRoute || childRoute.length < 0) {
      return '/';
    }

    if (childRoute) {
      return `/${routeObj.slug}${firstChildPath(routeObj.routes[0])}`;
    }

    return `/${routeObj.routes[0].slug}`;
  }

  return `/${routeObj.slug}`;
};

class MainMenu extends Component {
  componentWillMount() {
    this.unlisten = this.props.history.listen((location, action) => {
      this.forceUpdate();
    });
  }

  render() {
    const { navigation, currentpage } = this.props;
    const flatNavigation = navigation && [...navigation.primary, ...navigation.secondary];
    const currentPage = flatNavigation && (flatNavigation.find(page => page.slug === currentpage.slug) || flatNavigation[0]);
    const currentSubPage = currentPage.routes;
    const menu = currentSubPage && currentSubPage.map((route, i) => {
      const currentPageSlug = route.slug;
      const hasChildren = route.hasChild && route.routes && route.routes.length > 0;
      const childPages = hasChildren && route.routes.map((childRoute, cIndex) => {
        const hasSections = childRoute.sections && childRoute.sections.length > 0;
        const submenu = hasSections && childRoute.sections.map((section, sIndex) => (
          <li key={`submenu-section-${sIndex}`}>
            <NavLink to={`/${currentpage.slug}/${currentPageSlug}/${childRoute.slug}?p=${section.slug}`}>{section.name}</NavLink>
          </li>
        ));
        return (
          <li key={`submenu-${route.slug}-${cIndex}`}>
            <NavLink to={`/${currentpage.slug}/${currentPageSlug}/${childRoute.slug}`}>{childRoute.name}</NavLink>
            {
              hasSections && (
                <div className="submenu">
                  <ul>
                    {submenu}
                  </ul>
                  <div className="submenu-extra">
                    <div className="menu-image">
                      <img src={girl} alt=""/>
                      <div className="_overlay">
                        <div className="_name">Keta Keti Bachat Khata</div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
                        <Link to="/">Apply Now</Link>
                      </div>
                    </div>
                  </div>
                </div>
              )
            }
          </li>
        );
      });
      return (
        <li key={`submenu-${route.slug}-${i}`}>
          <NavLink
            activeClassName="active"
            to={`/${currentpage.slug}${firstChildPath(route)}`}
            isActive={matchWorkaround(`${currentpage.slug}/${route.slug}`)}
            className={cn(styles.mainMenuA, { withChild: hasChildren })}
          >
            <span className={styles.menuText}>{route.name}</span>
            {hasChildren && <DownIcon color="primary"/>}
          </NavLink>
          {
            hasChildren && (
              <div className="dropdown">
                <ul>
                  {childPages}
                </ul>
              </div>
            )
          }
        </li>
      );
    });
    return (
      <div className={cn(styles.btmMenuInner, 'main-menu')}>
        <ul>
          {menu}
        </ul>
      </div>
    );
  }
}

MainMenu.propTypes = {
  currentpage: PropTypes.objectOf(PropTypes.string).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(withRouter(MainMenu));
