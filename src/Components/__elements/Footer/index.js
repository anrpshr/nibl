import React from 'react';
import { NavLink, Link } from 'react-router-dom';

import ScrollUpAnchor from '../../ScrollUpAnchor';
import Styles from './styles.module.scss';

const Footer = () => (
  <div className={Styles.footerWrapper}>
    <div className="container">
      <div className={Styles.gridBlock}>
        <div className={Styles.block}>
          <h4>Branches</h4>
          <span>
            The 78 Branches, 109 ATMs, 7 Extension
            <br/>
            Counters and 9 Revenue Collection Counters
            <br/>
            of NIBL dedicated to Customer Service.
          </span>
          <span>Contact To you Nearest Branch</span>
        </div>

        <div className={Styles.block}>
          <h4>Corporate Office</h4>
          <span>
            Durbar Marg, P.O. Box: 3412
            <br/>
            Kathmandu
            <br/>
            Nepal
          </span>
          <span>Email: Info@nibl.com.np</span>
          <span>Fax: (977-1) 4226349, 4228927</span>
        </div>

        <div className={Styles.block}>
          <h4>Help Desk</h4>
          <span>Toll Free Number : 1660-01-00070</span>
          <span>Mobile Number: +977-9851126440</span>
          <span>Email: cardhelpdesk@nibl.com.np</span>
        </div>

        <div className={Styles.block}>
          <h4>Contact Us</h4>
          <span>Durbar Marg, Kathmandu</span>
          <span>P.O. Box: 3412</span>
          <span>Phone: +977-1-4228229</span>
          <span>Hotline: +977-1-4421345</span>
          <span>Fax: +977-1-4226349, 4228927</span>
          <span>SWIFT: Investment</span>
          <span>Email: info@nibl.com.np </span>
        </div>

        <div className={Styles.block}>
          <h4>Reports and Downloads</h4>
          <div className={Styles.list}>
            <Link to="/about-us/downloads/downloads">Downloads</Link>
            <Link to="/about-us/investor-relation/reports">Reports</Link>
            <Link to="/about-us/disclosure/disclosure">Disclosure</Link>
            <Link to="/about-us/investor-relation/financial-downloads">Financial Downloads</Link>
          </div>
        </div>

        <div className={Styles.block}>
          <h4>Other Links</h4>
          <div className={Styles.list}>
            <Link to="/online-services/c-asba/c-asba">CASBA</Link>
            <Link to="/online-services/ips/ips">IPS</Link>
            <Link to="/personal-banking/faq/faq">FAQ</Link>
            <Link to="/personal-banking/rates-and-fees/interest-rates">Interest Rates</Link>
          </div>
        </div>
      </div>
    </div>
    <div className={Styles.bottomFooter}>
      <div className="container">
        <div className={Styles.footerFlex}>
          <div className={Styles.footerLinks}>
            <NavLink to="/contact/careers" exact>Career</NavLink>
            <NavLink to="/informationoffice" exact>Information Office</NavLink>
            <NavLink to="/csr" exact>CSR</NavLink>
            <a href="https://niblcapital.com/" rel="noopener noreferrer" target="_blank">NIBL Capital</a>
            <NavLink to="/365daysbanking" exact>365 Days Banking</NavLink>
            <NavLink to="/security" exact>Security</NavLink>
            <NavLink to="/treasury" exact>Treasury</NavLink>
            <div className={Styles.copyright}>
              <span>Copyright © {new Date().getFullYear()}. Nepal Investment Bank Limited | All Rights Reserved.</span>
            </div>
          </div>
          <div className={Styles.social}>
            <a href="1"><i className="fab fa-facebook-f" /></a>
            <a href="1"><i className="fab fa-twitter" /></a>
          </div>
        </div>
      </div>
    </div>
    <ScrollUpAnchor/>
  </div>
);

export default Footer;
