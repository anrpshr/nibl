import { createMuiTheme } from '@material-ui/core/styles';

const palette = {
  primary: {
    main: '#cc2128'
  },
  secondary: {
    main: '#3a4d85'
  },
  special: {
    main: '#ffffff'
  }
};

export default createMuiTheme({
  palette,
  shadows: ['none']
});
