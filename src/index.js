import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { addLocaleData } from 'react-intl';
import { MuiThemeProvider } from '@material-ui/core/styles';

import en from 'react-intl/locale-data/en';
import ne from 'react-intl/locale-data/ne';

import Theme from './styles/Theme';
import store from './redux';
import App from './App';
import * as serviceWorker from './serviceWorker';

import 'antd/dist/antd.css';
import './styles/global.scss';

addLocaleData([...en, ...ne]);

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={Theme}>
      <App/>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
