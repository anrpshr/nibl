import {
  SET_MODAL_CLOSE, SET_MODAL_OPEN,
  SET_SPLASH_OPEN, SET_SPLASH_CLOSE, SHOW_SEARCH_BLOCK, HIDE_SEARCH_BLOCK
} from '../constants/action-types';

const initState = {
  modalOpen: false,
  splashOpen: false,
  splashInitialSlide: 0,
  searchBlockOpen: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case SET_MODAL_CLOSE:
      return {
        ...state,
        modalOpen: false
      };
    case SET_MODAL_OPEN:
      return {
        ...state,
        modalOpen: true
      };
    case SET_SPLASH_OPEN:
      return {
        ...state,
        splashOpen: true,
        splashInitialSlide: action.payload
      };
    case SET_SPLASH_CLOSE:
      return {
        ...state,
        splashOpen: false
      };
    case SHOW_SEARCH_BLOCK:
      return {
        ...state,
        searchBlockOpen: true
      };
    case HIDE_SEARCH_BLOCK:
      return {
        ...state,
        searchBlockOpen: false
      };
    default:
      return state;
  }
};
