import { combineReducers } from 'redux';

import localeReducer from './locale.reducer';
import snackReducer from './snack.reducer';
import navigationReducer from './navigation.reducer';
import currentpageReducer from './currentpage.reducer';
import commonReducer from './common.reducer';

const reducers = combineReducers({
  locale: localeReducer,
  snack: snackReducer,
  navigation: navigationReducer,
  currentpage: currentpageReducer,
  common: commonReducer
});

export default reducers;
