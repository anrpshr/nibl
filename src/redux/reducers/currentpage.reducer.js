import { SET_CURRENTPAGE } from '../constants/action-types';

const initState = {
  slug: null
};

export default (state = initState, action) => {
  switch (action.type) {
    case SET_CURRENTPAGE:
      return {
        slug: action.payload
      };
    default:
      return state;
  }
};
