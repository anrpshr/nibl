import { SET_NAVIGATION } from '../constants/action-types';

const initState = null;

export default (state = initState, action) => {
  switch (action.type) {
    case SET_NAVIGATION:
      return {
        ...action.payload
      };
    default:
      return state;
  }
};
