import { SET_LOCALE } from '../constants/action-types';

const initState = {
  lang: 'en'
};

export default (state = initState, action) => {
  switch (action.type) {
    case SET_LOCALE:
      return {
        ...state,
        lang: action.lang
      };
    default:
      return state;
  }
};
