import { SET_NAVIGATION } from '../constants/action-types';

const setNavigation = routes => (
  {
    type: SET_NAVIGATION,
    payload: routes
  }
);

export default setNavigation;
