import {
  SET_MODAL_OPEN,
  SET_MODAL_CLOSE,
  SET_SPLASH_OPEN,
  SET_SPLASH_CLOSE,
  SHOW_SEARCH_BLOCK,
  HIDE_SEARCH_BLOCK
} from '../constants/action-types';

export const setModalOpen = () => ({
  type: SET_MODAL_OPEN
});

export const setModalClose = () => ({
  type: SET_MODAL_CLOSE
});

export const setSplashOpen = slideIndex => ({
  type: SET_SPLASH_OPEN,
  payload: slideIndex
});

export const setSplashClose = () => ({
  type: SET_SPLASH_CLOSE
});

export const showSearchBlock = () => ({
  type: SHOW_SEARCH_BLOCK
});

export const hideSearchBlock = () => ({
  type: HIDE_SEARCH_BLOCK
});
