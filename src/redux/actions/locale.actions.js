import { SET_LOCALE } from '../constants/action-types';
import { GLOBALS } from '../../constants';

export default lang => ({
  type: SET_LOCALE, lang
});

const setLocaleAndRefresh = (lang) => {
  localStorage.setItem(GLOBALS.LANG_KEY, lang);
  window.location.reload();
};

export {
  setLocaleAndRefresh
};
