import { SET_CURRENTPAGE } from '../constants/action-types';

const setCurrentpage = currentPageSlug => (
  {
    type: SET_CURRENTPAGE,
    payload: currentPageSlug
  }
);

export default setCurrentpage;
