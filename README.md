# Nepal Investment Bank Front End

## Pre-requisites
- Node v10
- serve `npm install -g serve`

This guide is for deploying and updating the app in **local environment** 

## Build & Deploy
1. Navigate to folder where app is to be deployed.
2. In the command line, run `git clone https://roshanbodomlol@bitbucket.org/anrpshr/nibl.git`
3. `cd nibl`
4. Run `npm install`
5. Run `npm run build`
6. Run `serve -s build`

## Update
1. Navigate to project folder
2. Run `git pull`
3. Run `npm install` if necessary
4. Run `npm run build`
5. Run `serve -s build`